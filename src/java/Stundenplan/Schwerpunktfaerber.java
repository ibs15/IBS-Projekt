/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Stundenplan;

import java.awt.Color;
import java.util.Hashtable;

/**
 *
 * @author john
 */
class Schwerpunktfaerber {
    Hashtable<String,Color> colors=new Hashtable<String,Color>();
    //Speichert, an welchem index die letzte farbe aus dem future array enfernt wurde.
    int now;
    private Color[] future={Color.red, Color.gray, Color.blue, Color.yellow, Color.pink, Color.CYAN, Color.LIGHT_GRAY};
    public Schwerpunktfaerber() {
    }
    public Color getColor(String schwerpunkt){
        
        Color sw=colors.get(schwerpunkt.trim());
        if(sw==null){
            colors.put(schwerpunkt.trim(), future[now]);
            sw=future[now++];
        }
       return sw;
    }
}
