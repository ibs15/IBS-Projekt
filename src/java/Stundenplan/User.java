/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Stundenplan;

import java.awt.Color;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;

/**
 *
 * @author john
 */
public class User {

    String Debug = "";
    private boolean reverted = false;
    //Anzeigenahmen
    HashSet<Integer> vids = new HashSet();
    private String Name;
    private String Studiengang;
    private String Fachrichtung;
    //SQL "Name"
    private int Uid;
    private Table chosen;
    private int semester;
    int getSemester;
    private boolean changed = false;

    public int getSemester() {
        return semester;
    }

    public void setSemester(int semester) {
        this.semester = semester;
    }

    public User(String Name) {
        System.out.println("user mit name " + Name);
        this.Name = Name;
        this.Uid = searchUID(Name);
        chosen = new Table();
        //to load all subjects
        loadTable();
        //Create SQL Constructor
    }

    public String getName() {
        return Name;
    }

    public int getUid() {
        return Uid;
    }

    /**
     * @author Thomas
     */
    public void loadTable() {
        ArrayList<Integer> VIDS = new ArrayList<Integer>();
        SQLConnector sql = new SQLConnector();
        String base = "select vid from g02_vlteilnahme join g02_user on g02_user.vtid= g02_vlteilnahme.vtid  where g02_user.uid= ?\n";
        try {
            PreparedStatement prep = sql.getPreparedStatement(base);
            prep.setInt(1, Uid);
            ResultSet res = sql.performSQL(prep);
            // res.next();
            System.out.println("No records found");

            while (res.next()) {
                System.out.print("*");
                vids.add(res.getInt(1));

                // res.next();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Table getTable() {
        String s = "";
        Schwerpunktfaerber faerber = new Schwerpunktfaerber();
        SQLConnector sq = new SQLConnector();
        PreparedStatement ps = sq.getPreparedStatement("select kurs, dozent,schwerpunkt, raum, tagnr, zeitblocknr, stg, semester  from g02_vorlesungsplan where vid=?");
        Kurstermin kt1;
        for (int vid : vids) {

            Schwerpunkter schwerePunkte = new Schwerpunkter();
            try {
                ps.setInt(1, vid);
                ResultSet rs = sq.performSQL(ps);
                rs.next();
                //getData;
                String kurs = rs.getString(1);
                String dozent = rs.getString(2);
                String schwerp = rs.getString(3);
                String[] schwp = schwerp.split(",");
                String raum = rs.getString(4);
                int tagnr = rs.getInt(5);
                int zeit = rs.getInt(6);
                String sg = rs.getString(7);
                int sem = rs.getInt(8);
                for (int j = 0; j < schwp.length; j++) {
                    schwerePunkte.addSchwerpunkt(schwp[j], faerber.getColor(schwp[j]));
                }
                //create kurstermin
                kt1 = new Kurstermin(sg, schwerePunkte, sem, dozent, kurs, raum, tagnr, zeit, vid);
                this.addKurstermin(kt1);
                rs.close();
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                for (StackTraceElement r : ex.getStackTrace()) {
                    System.out.println(r.getClassName() + r.getMethodName());

                }

            }
        }
        return this.chosen;
        //return this.chosen;

    }

    public boolean canUndo() {

        return reverted;
    }

    public boolean canRedo() {
        //Do SQL Tasks
        return !reverted;
    }

    public String getStudiengang() {
        return Studiengang;
    }

    public String getFachrichtung() {
        return Fachrichtung;
    }

    public void setStudiengang(String Studiengang) {
        this.Studiengang = Studiengang;
    }

    private void addKurstermin(Kurstermin toAdd) {
        chosen.addTableField(toAdd);
    }

    public void removeKurstermin(int vid) {
        changed = true;
        System.out.println("--------" + vid);

        vids.remove(vid);

    }

    public void addKurstermin(int vid) {
        changed = true;
        System.out.println("++++++++" + vid);
        vids.add(vid);
    }

    void setFachrichtung(String fachrichtung) {
        this.Fachrichtung = fachrichtung;
    }

    void saveSettings() {
        SQLConnector sql = new SQLConnector();
        PreparedStatement prep;
        String base = "update g02_user set  stg = ? , schwerpunkt =?, semester = ? where uid=? ";
        prep = sql.getPreparedStatement(base);
        try {
            prep.setInt(4, this.getUid());
            prep.setString(1, (Studiengang == null ? " " : Studiengang));
            prep.setString(2, (Fachrichtung == null ? " " : Fachrichtung));
            prep.setInt(3, semester);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            ResultSet res = sql.performSQL(prep);
           // res.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void save() {

        saveSettings();
        //get old andNew
        int vtid, vtidr;

        if (changed) {
            try {
                System.out.println("saving");

                //open session
                SQLConnector sql = new SQLConnector();
                PreparedStatement prep;
                String base = "select vtidr, vtid from g02_user where uid=? ";
                prep = sql.getPreparedStatement(base);
                prep.setInt(1, this.getUid());
                ResultSet res = sql.performSQL(prep);
                System.err.println(this.getUid());
                if (res.next()) {
                    vtidr = res.getInt(1);
                    vtid = res.getInt(2);
                    System.err.println("asdf");
                    //beides andersrum speichern
                    base = "update g02_user set reverted=false,vtid = ? , vtidr =?  where uid=? ";
                    prep = sql.getPreparedStatement(base);
                    prep.setInt(3, this.getUid());
                    prep.setInt(1, vtidr);
                    prep.setInt(2, vtid);
                    res = sql.performSQL(prep);

                    //DELETE FROM table_name WHERE id=?
                    base = "DELETE FROM  g02_vlteilnahme  where vtid= ?";
                    prep = sql.getPreparedStatement(base);
                    prep.setInt(1, vtidr);
                    res = sql.performSQL(prep);

                    for (int vid : vids) {
                        System.out.println("saving" + vid);
                        base = "INSERT INTO g02_vlteilnahme (vtid,vid )VALUES (? , ?);";
                        prep = sql.getPreparedStatement(base);
                        prep.setInt(1, vtidr);
                        prep.setInt(2, vid);
                        res =sql.performSQL(prep);
                    }
                  //  res.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            

            //DELETE FROM table_name WHERE id=?; old;
        } else {
            System.out.println("No Changes Detected");
        }
        /* SQLConnector sq = new SQLConnector();
            try{
            PreparedStatement ps = sq.getPreparedStatement("update g02_user set   lastlogin = ? where uid=?");

            ps.setTimestamp(1, new Timestamp(System.currentTimeMillis()));  
            ps.setInt(2,this.getUid() );
            
            ResultSet rs= sq.performSQL(ps);
          //  rs.close();
            
            //   System.out.println("rrrr"+rs.toString());
            }
            catch(Exception e){
               
    System.out.println("AAAAAAAAAAAAAA");
    e.printStackTrace();
}
*/
        
    }

    private int searchUID(String Username) {
        Debug += Username + " Hat ID ";
        System.out.println("Stundenplan.LoginServlet.searchUID()");

        System.out.println("uid for user " + Username);
        SQLConnector sq = new SQLConnector();
        try {
            System.out.println("asdfasdf");
            PreparedStatement ps = sq.getPreparedStatement("select uid , reverted ,stg , schwerpunkt , semester from g02_user where name=?");

            ps.setString(1, Username);

            ResultSet rs = sq.performSQL(ps);
            System.out.println(Username);

            rs.next();
            if (!rs.isLast()) {
                System.out.println("ERR##########" + rs.first());
                return -1;
            }
            Debug += rs.getInt(1);
            Studiengang = rs.getString("stg");
            Fachrichtung = rs.getString("schwerpunkt");
            semester = rs.getInt("semester");
            Studiengang = " ".equals(Studiengang) ? null : Studiengang;
            Fachrichtung = " ".equals(Fachrichtung) ? null : Fachrichtung;

            reverted = rs.getBoolean("reverted");
            int temp=rs.getInt(1);
            rs.close();
            return temp;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return -1;
    }

}
