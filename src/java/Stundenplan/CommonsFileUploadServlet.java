package Stundenplan;

import java.io.PrintWriter;

import java.util.List;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;


import javax.servlet.http.Part;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

 


@MultipartConfig
public class CommonsFileUploadServlet extends HttpServlet {
private User user;
    private File tmpDir;
private static final String TMP_DIR_PATH = System.getProperty("java.io.tmpdir");
//private static final String DESTINATION_DIR_PATH = System.getProperty("java.io.tmpdir");

//    private String destinationDir;
private String working="";
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
           
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UploadServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h3>File Upload:</h3>\n"
                    + "Select a file to upload: <br />\n"
                    + working );
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
}
    
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        tmpDir = new File(TMP_DIR_PATH);
        if(!tmpDir.isDirectory()) {
            throw new ServletException(TMP_DIR_PATH + " is not a directory");
        }
        String realPath = getServletContext().getRealPath(TMP_DIR_PATH);
        File destinationDir = new File(realPath);
        if(!destinationDir.isDirectory()) {
        //    throw new ServletException(DESTINATION_DIR_PATH+" is not a directory");
        }
 
    }
 
    static final String erfolgsseite = "/Stundeplan/StundenPlan";
   static final String misserfolgsseite = "/Stundeplan/file_error.html";


    /* doPost ueberschreiben (doGet ist irrelevant)	*/

    
  public void doPost(HttpServletRequest request, HttpServletResponse response){
        // Feedback an Benutzer vorbereiten
          Cookie[] cookies = request.getCookies();
       User Myuser=LoginManager.login(cookies);
      
        if(Myuser == null) {
              try {
                  response.sendRedirect("/Stundeplan/LogoutServlet");
              } catch (IOException ex) {
                  Logger.getLogger(CommonsFileUploadServlet.class.getName()).log(Level.SEVERE, null, ex);
              }
           
        }
	StringBuffer feedback = new StringBuffer();
	String fileName = "";
	long fileSize = 0;
	String ziel = misserfolgsseite;
        List<String> paths = new ArrayList<String>();
	try {
                        
               

                
		String targetDirName = request.getServletContext().getRealPath("") + 
	                                  File.separator + "WEB-INF" +
	                                  File.separator + "UploadDir";
                ziel = targetDirName;

                
		// Ziel-Verzeichnis anlegen eigentlich unnötig 
		File targetDir = new File(targetDirName);
		if (!targetDir.exists()) targetDir.mkdir();
                feedback.append("Parts");
                
		// Iteration ueber alle Parts

                    
                //Datei upload
		for (Part part : request.getParts()) {
                    
                    
			fileName = getFileName(part);                        
			if (fileName.equals("")) continue;
			fileSize = part.getSize();
			feedback.append("<br>\n"+fileName+" "+((fileSize+512) / 1024)+" kByte");
			part.write(targetDirName + File.separator + fileName);
                        paths.add(targetDirName + File.separator + fileName);
                        
		}
                        
                
		/* Feedback an Benutzer */
		if (feedback.length() == 0) {
			feedback.append("Sie haben keine Datei angegeben.");
		} else {
			ziel = erfolgsseite;
                       //documentbuilder starten
                        DocumentBuilderFactory f = DocumentBuilderFactory.newInstance();
                        
                        f.setIgnoringElementContentWhitespace(true);
                        f.setValidating(true);
                        f.setNamespaceAware(true);
                        f.setAttribute("http://java.sun.com/xml/jaxp/properties/schemaLanguage", "http://www.w3.org/2001/XMLSchema");
                        //Parser anlegen
                        DocumentBuilder b = f.newDocumentBuilder();
                        Document doc = b.parse(new File(paths.get(0)));
                        
                        Node cN = doc.getFirstChild();
                        
                       //überprüfen ob unsere XML geladen wurde
                        if(cN.getNodeName().equals("vorlesungsplan")&&cN.getFirstChild().getNodeName().equals("kurstermin")){
                            Node kurs = cN.getFirstChild();                          
                            
                            while(true){
                                //VID Child holen
                                Node vid = kurs.getFirstChild().getNextSibling().getNextSibling().getNextSibling().getNextSibling().getNextSibling().getNextSibling().getNextSibling().getNextSibling();
                                //Dem User den kurstermin mit der VID zuweisen
                                Myuser.addKurstermin(Integer.parseInt(vid.getTextContent()));
                                //Wenn alle Kurstermine gelesen wurden beende die schleife
                                if(kurs == cN.getLastChild()) {
                                    break;
                                }
                                kurs = kurs.getNextSibling();
                                
                            }
                        }
            }
	}
	catch (IllegalStateException e) {
            e.printStackTrace();
            ziel=misserfolgsseite;
		/* Feedback an Benutzer */
		feedback.append("<br>Upload zu groÃŸ: "+e.getCause());
	}
	catch (Exception e) {
            e.printStackTrace();
                        ziel=misserfolgsseite;

		/* Feedback an Benutzer */
		feedback.append("<br>Unbekannter Grund - Bitte informieren Sie den Systemadministrator: ...");
	}
	finally {
           
		/* Feedback an Benutzer durch JSP-Seite ausgeben */
		try {
		  
                  response.sendRedirect(ziel);		  
		} 
		catch (Exception e) {}
	}
                    Myuser.save();
        
    }
  
  /* Dateiname aus HTTP header "content-disposition" auslesen */
	private String getFileName(Part part) {
	    // Header content-disposition bspw.:
		// form-data; name="files"; filename="IBS-1-XML-Grundlagen.pdf"
		for (String s : part.getHeader("content-disposition").split(";")) {
			if (s.trim().startsWith("filename")) {
				return s.substring(s.indexOf("=") + 2, s.length()-1);
			}
		}
		return ""; // im Falle von Teilen, die keine Dateien sind 
	}
 
}