package Stundenplan;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
    
    String debug = "";
    private static final long serialVersionUID = 1L;
    
    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        try {
            System.out.println("----------------------------------");
            SQLConnector a = new SQLConnector();
            a.databaseCheck();
            
            System.out.println("----------------------------------");
            // get request parameters for userID and password
            String user = request.getParameter("user");
            String pwd = request.getParameter("pwd");
           // System.err.println(user);
           // System.err.println(pwd);
            debug += ".";
            boolean UnreagUser = false;
            if (user.equals("Benutzer")) {
                debug += "n";
                UnreagUser = true;
                //It is an unregisterd user, so we generate a new User
                user = "UnregisterdUser" + (int) (Math.random() * 1000000);
                //if we delete unregisterd users ater an houre, this shuld surfice
                //toDo Generate user in SQL            
                SQLConnector sq = new SQLConnector();
                try {
                    PreparedStatement ps = sq.getPreparedStatement("INSERT INTO g02_user (name, lastlogin,vtid,vtidr) VALUES(?,?,?,?)");
                    ps.setString(1, user);
                    ps.setTimestamp(2, new Timestamp(System.currentTimeMillis()));                    
                    ps.setInt(3, (int) System.currentTimeMillis());                    
                    ps.setInt(4, 2 + (int) System.currentTimeMillis());                    
                    
                    ResultSet rs = sq.performSQL(ps);
                    // rs.close();

                    //       System.out.println("rrrr"+rs.toString());
                } catch (Exception e) {
                    System.out.println("xxxxxxxxxxxxxxxxx");
                    e.printStackTrace();
                }
//Villeicht bekannter user
            }
            int uid = searchUID(user);
            if (UnreagUser || checkPWD(uid, pwd)) {
                System.err.println("Password ok");
                Cookie loginCookie = new Cookie("user", user);
                int sid = (int) (Math.random() * 1000000);
                //SQL:Save sid (with date)
                Cookie SessIDCookie = new Cookie("SID", "" + sid);
                //setting cookie to expiry in 30 mins
                loginCookie.setMaxAge(30 * 60);
                SessIDCookie.setMaxAge(30 * 60);
                response.addCookie(loginCookie);
                response.addCookie(SessIDCookie);
                response.sendRedirect("StundenPlan");
                SQLConnector sq = new SQLConnector();
                try {
                    PreparedStatement ps = sq.getPreparedStatement("update g02_user set  sessid= ? lastlogin = ? where uid=?");
                    ps.setInt(1, sid);
                    ps.setTimestamp(2, new Timestamp(System.currentTimeMillis()));                    
                    ps.setInt(3, uid);
                    
                    ResultSet rs = sq.performSQL(ps);
                    //  rs.close();

                    //       System.out.println("rrrr"+rs.toString());
                } catch (Exception e) {
                    System.out.println("CCCCCCCCCC");
                    e.printStackTrace();
                }
                
            } else {
                debug += "f";
                System.err.println("Not ok");
                RequestDispatcher rd = getServletContext().getRequestDispatcher("/");
                PrintWriter out = response.getWriter();
                response.sendRedirect("/Stundeplan/login_error.html");
                
                rd.include(request, response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
    
    private boolean checkName(String Username) {
        SQLConnector sq = new SQLConnector();
        try {
            PreparedStatement ps = sq.getPreparedStatement("select uid from g02_user where name=?");
            ps.setString(1, Username);            
            ResultSet rs = sq.performSQL(ps);
            if (rs.next()) {
                rs.close();
                return true;
            }
            rs.close();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
        }        
        return false;
    }
    
    private int searchUID(String Username) {
        
        SQLConnector sq = new SQLConnector();
        try {
            PreparedStatement ps = sq.getPreparedStatement("select uid from g02_user where name=?");
            ps.setString(1, Username);            
            ResultSet rs = sq.performSQL(ps);
            if (!rs.next()) {
                System.out.println(rs.toString());
                System.out.println("Stundenplan.LoginServlet.searchUID(): No such User");
                return -1;
            }
            int tmp = rs.getInt(1);
            rs.close();
            return tmp;
        } catch (Exception e) {
            e.printStackTrace();
        }        
        return -1;
    }
    
    private boolean checkPWD(int userid, String PWD) {
        System.out.println("checking Password for uid " + userid);
        
        SQLConnector sq = new SQLConnector();
        
        try {
            PreparedStatement ps = sq.getPreparedStatement("select passwordhash, salz , lastlogin from g02_user where uid=?");
            ps.setInt(1, userid);            
            ResultSet rs = sq.performSQL(ps);
            if (!rs.next()) {
                System.err.println("no Dice");
                return false;
            }
            System.out.println("got hash result");
            
            String dbpwd = rs.getString(1);
            String salts = rs.getString(2);
            // if( rs.getTimestamp("lastlogin").after(DatSystem.currentTimeMillis()))
            System.out.println(dbpwd);
            rs.close();
            byte[] dbHash = DatatypeConverter.parseHexBinary(dbpwd.trim());//.getBytes(StandardCharsets.UTF_8);
            byte[] salt = DatatypeConverter.parseHexBinary(salts.trim());//.getBytes(StandardCharsets.UTF_8);
            
            return Passwords.isExpectedPassword(PWD.toCharArray(), salt, dbHash);
            
        } catch (Exception e) {
            e.printStackTrace();
        }        
        return false;
    }
    
}
