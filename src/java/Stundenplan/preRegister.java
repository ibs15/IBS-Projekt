/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Stundenplan;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author john
 */
@WebServlet("/PreRegister")
public class preRegister extends HttpServlet {

    /**
     * Servlet implementation class LoginServlet //
     */
    String debug = "";
    private static final long serialVersionUID = 1L;
    private final String userID = "a";
    private final String password = "b";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String debug = "";
        // try {

        //out.println("<--! " + Workr + " s-->");
        out.println("<!DOCTYPE html>");
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Stundenplan</title>"
                + "<meta charset=\"utf-8\">"
                + "<link rel=\"stylesheet\" type=\"text/css\" href=\"IBS_Stundenplan_nosidebar.css\">");
        out.println("</head>");
        out.println("<body>"
                       + "<ul class=topbar >\n");
       
        out.print("<li><form action=\"LogoutServlet\" method=\"post\">\n"
                + "<input  type=\"submit\" value=\"Logout\" ></form></li>");

         out.print("<li><form action=\"StundenPlan\" method=\"post\">\n"
                + "<input type=\"submit\" value=\"Abbrechen\" ></form></li>");

      
       

        out.println("</ul> ");

        out.println("<form action=\"RegisterServlet\" method=\"post\">\n"
                + " \n"
                + "new Username: <input type=\"text\" name=\"user\" >\n"
                + "<br>\n"
                + "Password: <input type=\"password\" name=\"pwd\">\n"
                + "<br>\n"
                + "Password nochmal: <input type=\"password\" name=\"pwdT\">\n"
                + "<br>\n"
                + "<input type=\"submit\" value=\"Register\">\n"             
                + "</body>");
                out.close();
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        Cookie[] cookies = request.getCookies();
        User user = LoginManager.login(cookies);
        if (user == null || user.getName() == null || user.getUid() <= 0) {
 try {
                  response.sendRedirect("/Stundeplan/LogoutServlet");
              } catch (IOException ex) {
                  Logger.getLogger(CommonsFileUploadServlet.class.getName()).log(Level.SEVERE, null, ex);
              }
        }
        processRequest(request, response);

    }
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        Cookie[] cookies = request.getCookies();
        User user = LoginManager.login(cookies);
        if (user == null ||user.getName() == null || user.getUid() <= 0) {
       
              try {
                  response.sendRedirect("Stundeplan/LogoutServlet");
              } catch (IOException ex) {
                  Logger.getLogger(CommonsFileUploadServlet.class.getName()).log(Level.SEVERE, null, ex);
              }
           
     
            
        }
        processRequest(request, response);

    }
}
