/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Stundenplan;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import javax.servlet.http.Cookie;

/**
 *
 * @author john
 */
public class LoginManager {
    public static  User login(Cookie[] cookies){
       final long ONE_MINUTE_IN_MILLIS=60000;//millisecs
      
        String  userName=null;
        String SID=null;
        if (cookies != null) {
            for (Cookie cookie : cookies) {
               
                if (cookie.getName().equals("user")) {
                    userName = cookie.getValue();
                
                }
                if (cookie.getName().equals("SID")) {
                    SID = cookie.getValue();
                   
                }
            }
        }
     User a=new User(userName);
     if (cookies != null) {
            for (Cookie cookie : cookies) {
               
                if (cookie.getName().equals("fachrichtung")) {
                    a.setFachrichtung(cookie.getValue());
                
                }
                if (cookie.getName().equals("semester")) {
                    try{
                    a.setSemester(Integer.parseInt(cookie.getValue()));
                    }
                    catch(Exception e){
                        System.out.println("semesterErr:"+cookie.getValue()+e.getMessage());
                    }
                
                }
                
                if (cookie.getName().equals("studiengang")) {
                    a.setStudiengang(cookie.getValue());
                
                }
               
            }
        }

             SQLConnector sql=new SQLConnector();
             PreparedStatement prep=sql.getPreparedStatement("Select lastlogin ,sesid from g02_user where uid= ?");
             try{
             prep.setInt(1, a.getUid());
             ResultSet rs= sql.performSQL(prep);

           Calendar date = Calendar.getInstance();
            long t= date.getTimeInMillis();
            Date afterSubstracting30Mins=new Date(t - (30 * ONE_MINUTE_IN_MILLIS));
                 System.err.println("testing sid");
                 if(rs.next()){
                 int dbsid=-1;
                 int coSID;
                 try{
                     dbsid=rs.getInt("sesid");
                     coSID=Integer.parseInt(SID);
                     dbsid-=coSID;
                     System.out.println("diff sid="+dbsid);
                 }
                 catch(Exception e){
                     e.printStackTrace();
                 }
            if(rs.getTimestamp("lastlogin").after(afterSubstracting30Mins)){
                System.err.println("logintime OK");
                rs.close();
                 
           return a;
           }}
           else{
                                System.out.println("Logintime not ok");

               return null;
           }
             }catch(Exception e){
                 e.printStackTrace();
                 //fail save
                 return null;
             }
    //return a;
    return null;
    }
}
