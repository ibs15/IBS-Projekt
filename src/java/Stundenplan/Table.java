package Stundenplan;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author john
 */
public class Table {
    static int debug=0;
    private String[] times={"08:00 - 09:30","09:45 - 11:15","11:30 - 13:00","14:00 - 15:30", "15:45 - 17:15", "17:30-19:00 ", "19:15-20:45","11","12"};
    Kurstermin[][] data = new Kurstermin[6][9];
    Schwerpunktfaerber sf=new Schwerpunktfaerber();
    public Table() {
        for (int i = 0; i < data[0].length; i++) {
            //die Tage
            for (int j = 0; j < data.length; j++) {
                data[j][i] = null;
            }
            
        }
    }

    /**
     * Overwrites (the null) TableField at a given position to the "toAdd" one
     *
     * @param toAdd The tableField to Be added
     */
    public String addTableField(Kurstermin toAdd) {
       String debug=""+toAdd.zeitblocknr+" "+"toAdd.tagnr";
        if (toAdd.tagnr <= data.length+1 && toAdd.zeitblocknr <= data[0].length+1) {
            debug+="a";
            data[toAdd.tagnr-1][toAdd.zeitblocknr-1] = toAdd;
            debug+="b";

        } else {
            throw new RuntimeException("invallid tableField");
        }
        return debug;
    }

    /**
     * Generates an html source code to the table object
     *
     * @return an html reperentation of the html code.
     */
    public String toHtml() {
         String a = "<form action=\"StundenPlan\" method=\"post\" >";
        a+="<table border=\"1\"><tr class=\"header\" ><th>Stunde</th><th>Montag</th><th>Dienstag</th><th>Mitwoch</th><th>Donnerstag</th><th>Freitag</th><th>Samstag</th>";
        //die Stunden
        for (int i = 0; i < 3; i++) {

            a += "<tr><td class=\"time\">" + this.times[i] + "</td>";//this.times[i]
            //die Tage
            for (int j = 0; j < data.length; j++) {
                if (data[j][i] != null) {
                    a += data[j][i].getTableElement();
                } else {
                    a += "<td></td>";
                }
            }
            a += "</tr>\n";
        }
        a+="<tr class=\"pause\">\n" +
"	<td class=\"time\">13:00 - 14:00"+"</td><td></td><td></td><td></td><td></td><td></td><td></td>\n" +
"</tr>";
        for (int i = 3; i <6; i++) {

            a += "<tr><td class=\"time\">" +this.times[i]  + "</td>";
            //die Tage
            for (int j = 0; j < data.length; j++) {
                if (data[j][i] != null) {
                    a += data[j][i].getTableElement();
                } else {
                    a += "<td></td>";
                }
            }
            a += "</tr>\n";
        }
        a += "</table><input value=\"delete\" type=\"submit\"></form>";
        return a;
    }

    public String toXML() {
        String a = "<?xml version=\"1.0\"?>\n";
        a += "<vorlesungsplan xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"vorlesungsplan.xsd\">\n";
        for (int i = 0; i < data[0].length; i++) {

            //a += "<tag>";
           //die Tage
            for (int j = 0; j < data.length; j++) {
                if (data[j][i] != null) {
              //      a += "<stunde><stundenID>" + j + "</stundenID>";
                    a += data[j][i].getXML();
              //      a += "</stunde>";
                } else {

                }
            }
            //a += "</tag>";
        }
        a += "</vorlesungsplan>";
        return a;
    }
}
