package Stundenplan;
import java.io.BufferedReader;

import java.io.FileReader;
import java.io.PrintStream;

import java.sql.*;

/**
 * 
 * @author Thomas
 *
 */
public class SQLConnector {

	static String treiber  = "org.postgresql.Driver",
			dbserver = "jdbc:postgresql://in-horus.htw-aalen.de:5432/",
                        dburl    = dbserver+"ibs_2015w",
                        user     = "ibsgruppe02",
			passw    = "bxrDTN7z";
	
	/*static String treiber  = "org.postgresql.Driver",
			dbserver = "jdbc:postgresql://franzt.de:5432/",
            dburl    = dbserver+"postgres",
            user     = "postgres",
			passw    = "ibs";*/
	
	static PrintStream o = System.out;
	static Connection con;

    static void free() {
        try{
       con.close();
        }catch(Exception e){
        e.printStackTrace();
        }
    }
	/**
	 * Der Konstruktor baut die verbindung zur DB auf.
	 * @author Thomas
	 */
	public SQLConnector (){
		try{
			Class.forName(treiber);
			con = DriverManager.getConnection(dburl, user, passw);
			//o.println("Verbindung zur Datenbank "+dburl+" als user "+user+" erfolgreich\n");
		}
		catch(Exception ex){
			System.out.println(ex.getMessage());
		}
	}
	/**
	 * Methode um einen PreparedStatement zu bekommen
	 * @input String im SQLFormat mit ? als Parameterwerte (bsp. select * from myTable where x=?)
	 * @return Vorbereitetes SQL statemant
	 * @author Thomas
	 */
	public PreparedStatement getPreparedStatement(String sql){
		try {
			return con.prepareStatement(sql);
		} catch (SQLException e) {
			System.out.println("error bei preparedStatement");
			return null;
		}
	}
	/**
	 * Methode um den PreparedStatement an die Datenbank zu senden und das Ergebnis der DB in ein ResultSet zu schreiben;
	 * @input PreparedStatement
	 * @return ResultSet
	 * @author Thomas
	 */
	public ResultSet performSQL(PreparedStatement ps){		
		try {
//                    System.out.println(ps.toString());
			return ps.executeQuery();
		} catch (SQLException e) {
                     System.out.println(ps.toString());
			System.out.println("error bei preparedStatement"+e.getLocalizedMessage());
			return null;
		}
	}
	/**
	 * Methode um die DB zu ueberpruefen und eventuel fehlende Tabellen einzuf�gen
	 * @author Thomas
	 */
	public void databaseCheck(){
		try {
			boolean [] tablesIO = new boolean[6];
			Statement s = con.createStatement();
			ResultSet rs = s.executeQuery("SELECT table_name FROM information_schema.tables WHERE table_schema = 'public'");
			while(rs.next()){
				System.out.println(rs.getString(1));
				if(rs.getString(1).equals( "g02_fachbereiche"))tablesIO[0] = true;
				if(rs.getString(1).equals("g02_gebaeude"))tablesIO[1] = true;
				if(rs.getString(1).equals("g02_raeume"))tablesIO[2] = true;
				if(rs.getString(1).equals("g02_vorlesungsplan"))tablesIO[3] = true;
				if(rs.getString(1).equals("g02_user"))tablesIO[4] = true;
				if(rs.getString(1).equals("g02_vlteilnahme"))tablesIO[5] = true;
			}
                        /*tablesIO[0]=true;
                        tablesIO[1]=true;
                        tablesIO[2]=true;
                        tablesIO[3]=true;
                        tablesIO[4]=true;
                        tablesIO[5]=true;*/
			if(!tablesIO[0]){
				s.execute("create table g02_fachbereiche (fbid integer primary key,fbk varchar(5),fbname varchar(50))");
				s.execute("GRANT SELECT ON TABLE g02_fachbereiche TO PUBLIC");
				s.execute("insert into g02_fachbereiche (fbid, fbk, fbname) values(11, 'C', 'Chemie'),(21, 'E', 'Elektrotechnik'),(22, 'IN', 'Informatik'),(31, 'M', 'Maschinenbau'),(32, 'WT', 'Werkstofftechnik'),(41, 'A', 'Augenoptik'),(42, 'O', 'Optoelektronik'),(43, 'MC', 'Mechatronik'),(44, 'TR', 'Technische Redaktion'),(51, 'W', 'Wirtschaftswissenschaften'),(52, 'WI', 'Wirtschaftsinformatik'),(53, 'G', 'Gesundheitsmanagement')");
			}
			if(!tablesIO[1]){
				s.execute("Create Table g02_Gebaeude (GebaeudeID varchar (10) primary key,GebBez varchar(30) not null,GebBeschr varchar(100))");
				s.execute("insert into g02_Gebaeude (GebaeudeID, GebBez, GebBeschr) values('hg', 'Hauptgebäude', 'Hauptgebäude, Beethovenstr. 1'),  ('g1', 'G1 Im Burren', 'Gebäude 1 Im Burren'),  ('g2', 'G2 Im Burren', 'Gebäude 2 Im Burren, (Elektronik, Informatik)'),  ('g3', 'G1 Im Burren', 'Gebäude 3 Im Burren'),  ('g4', 'G1 Im Burren', 'Gebäude 4 Im Burren'),  ('bi', 'Bibliothek', 'Bibliothek - Im Burren'),  ('ca', 'Cafeteria', 'Cafeteria - Im Burren'),  ('ga', 'Gartenstraße', 'Gartenstraße'),  ('gr', 'Grundlagenzentrum', 'Grundlagenzentrum'),  ('si', 'Silcherstraße', 'Silcherstraße'),  ('me', 'Mensa', 'Mensa')");
			}
			if(!tablesIO[2]){
				s.execute("Create Table g02_Raeume (RaumId varchar(20) primary key, Kurzbez varchar(20) not null, Ebene int not null, GebaeudeID varchar (10) not null references g02_Gebaeude)");
				s.execute("insert into g02_Raeume (RaumId, GebaeudeID, Kurzbez, Ebene) values('hgo1001','hg','101',1),('hgo1002','hg','102',1),('hgo1003','hg','103',1),('hgo1005','hg','105',1),('hgo1006','hg','106',1),('hgo1011','hg','111',1),('hgo1012','hg','112',1),('hgo1015','hg','115',1),('hgo1021','hg','121',1),('hgo1022','hg','122',1),('hgo2001','hg','201',2),('hgo2002','hg','202',2),('hgo2003','hg','203',2),('hgo2011','hg','211',2),('hgo2012','hg','212',2),('hgo2013','hg','213',2),('hgo2015','hg','215',2),('hgo2016','hg','216',2),('hgo2023','hg','223',2),('hgo2058','hg','258',2),('hgo2073','hg','273',2),('hgo1033','hg','133',1),('g1o0001','g1','0.01',0),('g1o0020','g1','0.20',0),('g2o1017','g2','1.17',1),('g2o0001','g2','0.01',0),('g2o0021','g2','0.21',0),('g2o0023','g2','0.23',0),('g2o0037','g2','0.37',0),('g2o1001','g2','1.01',1),('g2o1028','g2','1.28',1),('g2o1130','g2','1.30',1),('g2o1035','g2','1.35',1),('g2o1044','g2','1.44',1),('g2o2001','g2','2.01',2),('g2o2033','g2','2.33',2),('g2o2041','g2','2.41',2)");
			}
			if(!tablesIO[3]){
				s.execute("CREATE TABLE g02_vorlesungsplan(  vid serial PRIMARY KEY,  stg varchar(6),  schwerpunkt varchar(25),  semester integer,  dozent varchar(250),  kurs varchar(100),  raum varchar(50),  tagnr integer,  zeitblocknr integer)");
				s.execute("GRANT SELECT ON TABLE g02_vorlesungsplan TO public");
				                             
                               for (int i = 0; i < vorlesungstabelle.length; i++) {
                                s.execute(vorlesungstabelle[i]);                                   
                            }
								
			}
			if(!tablesIO[4]){
				s.execute("CREATE TABLE  g02_vlteilnahme (VTID serial not null,vid serial not null references g02_vorlesungsplan);");
			}
			if(!tablesIO[5]){
				s.execute("CREATE TABLE g02_user (UID serial PRIMARY KEY,name varchar(30),stg varchar(6),schwerpunkt varchar(25),semester integer,passwordHash char(64),salz char(64),sesid integer,vtid serial,vtidr serial,lastlogin timestamp, reverted boolean);");				
			}
		} catch (Exception e) {
			                 System.out.println("#''''#########################################" + e.getMessage());
			e.printStackTrace();
		}
		
	}

    @Override
    protected void finalize() throws Throwable {
        super.finalize(); //To change body of generated methods, choose Tools | Templates.
        //con.close();
    }
    
    String[] vorlesungstabelle = {"INSERT INTO g02_vorlesungsplan (stg, schwerpunkt, semester, dozent, kurs, raum, tagnr,  zeitblocknr) VALUES\n" +
"('F', NULL, 1, 'Eichinger', 'Technisches Zeichnen', 'G1 1.32', 3, 2),\n" +
"('FR', NULL, 2, 'Eichinger', 'Technisches Zeichnen', 'G1 1.32', 3, 2),\n" +
"('F', NULL, 2, 'Schmidt Ho', 'Mathematik 2', 'G1 0.22', 1, 1),\n" +
"('F', NULL, 2, 'Schmidt Ho', 'Mathematik 2', 'G1 0.20', 2, 4),\n" +
"('F', NULL, 2, 'Schmidt Ho', 'Mathematik 2', 'G1 0.20', 3, 2),\n" +
"('GE', NULL, 2, 'Schmidt Ho', 'Mathematik 2', 'G1 0.22', 1, 1),\n" +
"('GE', NULL, 2, 'Schmidt Ho', 'Mathematik 2', 'G1 0.20', 2, 4),\n" +
"('GE', NULL, 2, 'Schmidt Ho', 'Mathematik 2', 'G1 0.20', 3, 2),\n" +
"('FR', NULL, 1, 'Richter C.', 'Professionelle Textverarbeitung', 'G1 1.28', 1, 1),\n" +
"('FR', NULL, 1, 'Richter C.', 'Professionelle Textverarbeitung', 'G1 1.28', 2, 3),\n" +
"('FR', NULL, 1, 'Wendland', 'Einführung in die Medienwissenschaften', 'G1 1.01', 1, 4),\n" +
"('FR', NULL, 3, 'Reznicek', '2D-Visualisierungstechnik', 'G1 1.29', 3, 2),\n" +
"('FR', NULL, 3, 'Reznicek', '2D-Visualisierungstechnik', 'G1 1.29', 4, 4),\n" +
"('FR', NULL, 3, 'Reznicek', '2D-Visualisierungstechnik', 'G1 1.29', 4, 5),\n" +
"('FR', NULL, 3, 'Weissgerber', 'Dokumentationsprojekt', 'G1 0.01', 1, 3),\n" +
"('FR', NULL, 6, 'Bauer M.', 'Videoproduktion', '0013', 4, 2),\n" +
"('FR', NULL, 6, 'Bauer M.', 'Videoproduktion', '0013', 4, 3),\n" +
"('FR', NULL, 7, 'Bauer M.', 'Autorensysteme', '0013', 1, 2),\n" +
"('FR', NULL, 7, 'Bauer M.', 'Autorensysteme', '0013', 1, 3);"
,"INSERT INTO g02_vorlesungsplan (stg, schwerpunkt, semester, dozent, kurs, raum, tagnr,  zeitblocknr) VALUES\n" + 
"('ET', NULL, 3, 'Schulz (E)', 'Elektrotechnik 3', 'G2 1.44', 1, 1),\n" +
"('ET', NULL, 3, 'Schulz (E)', 'Elektrotechnik 3', 'G2 1.44', 4, 2),\n" +
"('ET', NULL, 1, 'Kleppmann', 'Mathematik 1', 'G2 0.01', 1, 1),\n" +
"('ET', NULL, 1, 'Kleppmann', 'Mathematik 1', 'G2 2.01', 2, 2),\n" +
"('ET', NULL, 1, 'Kleppmann', 'Mathematik 1', 'G2 0.01', 5, 2),\n" +
"('ET', NULL, 1, 'Werner', 'Physik 1', 'G2 1.44', 1, 3),\n" +
"('ET', NULL, 1, 'Werner', 'Physik 1', 'G2 0.37', 2, 1),\n" +
"('ET', NULL, 1, 'Werner', 'Physik 1', 'G2 0.01', 4, 1),\n" +
"('ET', NULL, 3, 'Werner', 'Elektronische Bauelemente', 'G2 0.37', 3, 4),\n" +
"('ET', NULL, 3, 'Werner', 'Elektronische Bauelemente', 'G2 0.37', 3, 5),\n" +
"('ET', NULL, 2, 'Kleppmann', 'Mathematik 2', 'G2 0.37', 1, 3),\n" +
"('ET', NULL, 2, 'Kleppmann', 'Mathematik 2', 'G2 2.41', 3, 3),\n" +
"('ET', NULL, 2, 'Kleppmann', 'Mathematik 2', 'G2 2.01', 5, 1),\n" +
"('O', NULL, 2, 'Kleppmann', 'Mathematik 2', 'G2 0.37', 1, 3),\n" +
"('O', NULL, 2, 'Kleppmann', 'Mathematik 2', 'G2 2.41', 3, 3),\n" +
"('O', NULL, 2, 'Kleppmann', 'Mathematik 2', 'G2 2.01', 5, 1),\n" +
"('O', NULL, 2, 'Kulisch-Huep', 'Mathematik 2 Tutorium', 'G1 0.02', 5, 4),\n" +
"('ET', NULL, 2, 'Bürkle', 'Elektrotechnik 2', 'G2 0.37', 4, 3),\n" +
"('ET', NULL, 2, 'Werner', 'Physik 2 mit Labor', 'G2 0.01', 2, 3),\n" +
"('ET', NULL, 2, 'Werner', 'Physik 2 mit Labor', 'G2 2.01', 3, 2),\n" +
"('ET', NULL, 3, 'Kleppmann', 'Mathematik 3', 'G2 1.01', 1, 2),\n" +
"('ET', NULL, 3, 'Kleppmann', 'Mathematik 3', 'G2 1.44', 2, 1),\n" +
"('ET', 'IK', 4, 'Seelmann', 'Audiotechnik', 'G2 2.39', 4, 3),\n" +
"('ET', 'IK', 4, 'Seelmann', 'Audiotechnik', 'G2 2.39', 5, 1),\n" +
"('ET', NULL, 4, 'Steinhart', 'Elektrische Antriebe', 'G2 0.01', 1, 2),\n" +
"('ET', NULL, 4, 'Steinhart', 'Elektrische Antriebe', 'G2 0.01', 1, 3),\n" +
"('E', 'EEE, EIT', 3, 'Schulz (E)', 'Angewandte Elektrotechnik', 'G2 1.44', 1, 1),\n" +
"('E', 'EEE, EIT', 3, 'Schulz (E)', 'Angewandte Elektrotechnik', 'G2 1.44', 4, 2),\n" +
"('E', 'EEE, EIT', 3, 'Kleppmann', 'Angewandte Mathematik', 'G2 1.01', 1, 2),\n" +
"('E', 'EEE, EIT', 3, 'Kleppmann', 'Angewandte Mathematik', 'G2 1.44', 2, 1),\n" +
"('E', 'EEE, EIT', 3, 'Bartel', 'Steuerungstechnik 1', 'G2 2.33', 1, 3),\n" +
"('E', 'EEE, EIT', 3, 'Bartel', 'Steuerungstechnik 1', 'G2 2.33', 1, 4),\n" +
"('E', 'EEE, IFE', 4, 'Steinhart', 'Elektrische Antriebe', 'G2 0.01', 1, 2),\n" +
"('E', 'EEE, IFE', 4, 'Steinhart', 'Elektrische Antriebe', 'G2 0.01', 1, 3);"
,"INSERT INTO g02_vorlesungsplan (stg, schwerpunkt, semester, dozent, kurs, raum, tagnr,  zeitblocknr) VALUES\n" +
"('IN', 'IN, MI, SE, IS', 1, 'Hellmann', 'Rechnerarchitektur', 'G2 0.23', 1, 3),\n" +
"('IN', 'IN, MI, SE, IS', 1, 'Hellmann', 'Rechnerarchitektur', 'G2 0.23', 5, 2),\n" +
"('IN', 'IS, MI, SE', 2, 'Karg', 'Automatenth. u. form. Sprachen', 'G2 0.01', 1, 5),\n" +
"('IN', 'IS, MI, SE', 2, 'Karg', 'Automatenth. u. form. Sprachen', 'G2 0.23', 4, 1),\n" +
"('IN', 'IS, MI, SE', 3, 'Heinlein', 'Algorithmen und Datenstrukturen 2', 'G2 1.44', 1, 2),\n" +
"('IN', 'IS, MI, SE', 3, 'Heinlein', 'Algorithmen und Datenstrukturen 2', 'G2 1.44', 4, 4),\n" +
"('E', 'MK, TI', 4, 'Seelmann', 'Audiotechnik', 'G2 2.39', 4, 3),\n" +
"('E', 'MK, TI', 4, 'Seelmann', 'Audiotechnik', 'G2 2.39', 5, 1),\n" +
"('GE', NULL, 4, 'Steinhart', 'Elektrische Antriebstechnik', 'G2 0.01', 1, 2),\n" +
"('GE', NULL, 4, 'Steinhart', 'Elektrische Antriebstechnik', 'G2 0.01', 1, 3),\n" +
"('F', NULL, 6, 'Steinhart', 'Elektrische Antriebstechnik', 'G2 0.01', 1, 2),\n" +
"('F', NULL, 6, 'Steinhart', 'Elektrische Antriebstechnik', 'G2 0.01', 1, 3),\n" +
"('ET', NULL, 4, 'Müller', 'Datenk. / Rechnernetze', 'G2 0.01', 2, 2),\n" +
"('ET', NULL, 4, 'Müller', 'Datenk. / Rechnernetze', 'G2 1.03', 5, 4),\n" +
"('ET', 'IE_W', 6, 'Müller', 'Datenk. / Rechnernetze', 'G2 0.01', 2, 2),\n" +
"('ET', 'IE_W', 6, 'Müller', 'Datenk. / Rechnernetze', 'G2 1.03', 5, 4),\n" +
"('F', NULL, 4, 'Baur J.', 'Regelungstechnik 1', 'G2 1.44', 3, 1),\n" +
"('F', NULL, 4, 'Baur J.', 'Regelungstechnik 1', 'G2 1.44', 3, 2),\n" +
"('GE', NULL, 6, 'Baur J.', 'Regelungstechnik 1', 'G2 1.44', 3, 1),\n" +
"('GE', NULL, 6, 'Baur J.', 'Regelungstechnik 1', 'G2 1.44', 3, 2),\n" +
"('ET', NULL, 3, 'Baur J.', 'Regelungstechnik 1', 'G2 1.44', 3, 1),\n" +
"('ET', NULL, 3, 'Baur J.', 'Regelungstechnik 1', 'G2 1.44', 3, 2),\n" +
"('ET', 'IE_W', 6, 'Baur J.', 'Regelungstechnik 1', 'G2 1.44', 3, 1),\n" +
"('ET', 'IE_W', 6, 'Baur J.', 'Regelungstechnik 1', 'G2 1.44', 3, 2),\n" +
"('ET', NULL, 2, 'Hermann', 'Programmieren 2', 'G2 2.33', 2, 1),\n" +
"('ET', NULL, 2, 'Hermann', 'Programmieren 2', 'G2 2.33', 2, 2),\n" +
"('E', 'TI', 6, 'Hahn-Dambacher', 'Schaltkreisentwurf mit Labor und Projekt', 'G2 0.27', 1, 3),\n" +
"('E', 'TI', 6, 'Hahn-Dambacher', 'Schaltkreisentwurf mit Labor und Projekt', 'G2 0.27', 4, 3),\n" +
"('E', 'TI', 7, 'Hahn-Dambacher', 'Schaltkreisentwurf mit Labor und Projekt', 'G2 0.27', 1, 3),\n" +
"('E', 'TI', 7, 'Hahn-Dambacher', 'Schaltkreisentwurf mit Labor und Projekt', 'G2 0.27', 4, 3),\n" +
"('E', 'MK', 6, 'Müller', 'Internet-Technologien', 'G2 2.01', 1, 1),\n" +
"('E', 'MK', 6, 'Müller', 'Internet-Technologien', 'G2 2.01', 1, 2),\n" +
"('E', 'MK', 7, 'Müller', 'Internet-Technologien', 'G2 2.01', 1, 1),\n" +
"('E', 'MK', 7, 'Müller', 'Internet-Technologien', 'G2 2.01', 1, 2),\n" +
"('ET', 'IK', 6, 'Müller', 'Internet-Technologien', 'G2 2.01', 1, 1),\n" +
"('ET', 'IK', 6, 'Müller', 'Internet-Technologien', 'G2 2.01', 1, 2),\n" +
"('E', 'MK', 6, 'Müller', 'Netzpraktikum', 'G2 2.34', 5, 1),\n" +
"('E', 'MK', 6, 'Müller', 'Netzpraktikum', 'G2 2.34', 5, 2),\n" +
"('E', 'MK', 7, 'Müller', 'Netzpraktikum', 'G2 2.34', 5, 1),\n" +
"('E', 'MK', 7, 'Müller', 'Netzpraktikum', 'G2 2.34', 5, 2),\n" +
"('ET', 'IK', 7, 'Müller', 'Netzpraktikum', 'G2 2.34', 5, 1),\n" +
"('ET', 'IK', 7, 'Müller', 'Netzpraktikum', 'G2 2.34', 5, 2),\n" +
"('ET', 'IK', 6, 'Seelmann', 'Informationsth. / Datenkompress.', 'G2 2.39', 5, 3),\n" +
"('A', NULL, 1, 'Nolting', 'Geometrische Optik 1', '0124', 3, 4),\n" +
"('A', NULL, 1, 'Buschle', 'Prakt. Geometr. Optik', 'G4 0.02', 2, 6),\n" +
"('A', NULL, 1, 'Buschle', 'Prakt. Geometr. Optik', 'G4 0.02', 2, 7),\n" +
"('A', NULL, 1, 'Kirschkamp', 'Physiologische Optik', '0124', 1, 3),\n" +
"('A', NULL, 1, 'Kirschkamp', 'Humanphysiologie', '0124', 2, 3),\n" +
"('A', NULL, 2, 'Schiefer', 'Refraktion', 'G4 0.02', 2, 4),\n" +
"('AH', NULL, 2, 'Schiefer', 'Refraktion', 'G4 0.02', 2, 4),\n" +
"('A', NULL, 2, 'Paffrath', 'Geometrische Optik 2', 'G4 0.02', 3, 2),\n" +
"('AH', NULL, 2, 'Paffrath', 'Geometrische Optik 2', 'G4 0.02', 3, 2),\n" +
"('A', NULL, 2, 'Paffrath', 'Physical Optics', 'G4 -1.01', 3, 1),\n" +
"('A', NULL, 1, 'Buser', 'Statistik', '0203', 1, 4),\n" +
"('A', NULL, 2, 'Buser', 'Statistik', '0203', 1, 4),\n" +
"('AH', NULL, 2, 'Buser', 'Statistik', '0203', 1, 4),\n" +
"('A', NULL, 3, 'Mühlberger', 'W-Mitarbeiterführung', 'G4 2.10', 4, 6),\n" +
"('A', NULL, 4, 'Kirschkamp', 'Sehfunktionen 2', 'G4 -1.01', 4, 1),\n" +
"('AH', NULL, 4, 'Kirschkamp', 'Sehfunktionen 2', 'G4 -1.01', 4, 1),\n" +
"('A', NULL, 6, 'Dressler', 'Sicherheitstechnik', 'G4 -1.01', 6, 4),\n" +
"('A', NULL, 6, 'Dressler', 'Sicherheitstechnik', 'G4 -1.01', 6, 5),\n" +
"('AH', NULL, 6, 'Dressler', 'Sicherheitstechnik', 'G4 -1.01', 6, 4),\n" +
"('AH', NULL, 6, 'Dressler', 'Sicherheitstechnik', 'G4 -1.01', 6, 5),\n" +
"('ET', 'EE, IE', 4, 'Steinhart', 'Elektrische Antriebe', 'G2 0.01', 1, 3);"
,"INSERT INTO g02_vorlesungsplan (stg, schwerpunkt, semester, dozent, kurs, raum, tagnr,  zeitblocknr) VALUES\n" +
"('ET', 'EE, IK, IE', 4, 'Müller', 'Datenk. / Rechnernetze', 'G2 0.01', 2, 2),\n" +
"('ET', 'EE, IK, IE', 4, 'Müller', 'Datenk. / Rechnernetze', 'G2 1.03', 5, 4),\n" +
"('E', 'EEE, EIT', 3, 'Müller', 'Datenk. / Rechnernetze', 'G2 0.01', 2, 2),\n" +
"('E', 'EEE, EIT', 3, 'Müller', 'Datenk. / Rechnernetze', 'G2 1.03', 5, 4),\n" +
"('E', 'EEE, MK, TI, IFE', 4, 'Baur J.', 'Regelungstechnik 1', 'G2 1.44', 3, 1),\n" +
"('E', 'EEE, MK, TI, IFE', 4, 'Baur J.', 'Regelungstechnik 1', 'G2 1.44', 3, 2),\n" +
"('E', 'EEE, IFE', 4, 'Hermann', 'Softwareentwicklung', 'G2 2.33', 2, 1),\n" +
"('E', 'EEE, IFE', 4, 'Hermann', 'Softwareentwicklung', 'G2 2.33', 2, 2),\n" +
"('E', 'MK, TI', 6, 'Hermann', 'Objektorient. Programmierung', 'G2 2.33', 2, 1),\n" +
"('E', 'MK, TI', 6, 'Hermann', 'Objektorient. Programmierung', 'G2 2.33', 2, 2),\n" +
"('E', 'MK, TI', 7, 'Seelmann', 'Informationsth. / Datenkompress.', 'G2 2.39', 5, 3),\n" +
"('A', NULL, 4, 'Kirschkamp', 'Praktikum Kontaktlinse 2', 'G4 1.02', 1, 5),\n" +
"('A', NULL, 4, 'Kirschkamp', 'Praktikum Kontaktlinse 2', 'G4 1.02', 1, 6),\n" +
"('AH', NULL, 4, 'Kirschkamp', 'Praktikum Kontaktlinse 2', 'G4 1.02', 1, 5),\n" +
"('AH', NULL, 4, 'Kirschkamp', 'Praktikum Kontaktlinse 2', 'G4 1.02', 1, 6),\n" +
"('AH', NULL, 6, 'Buschle', 'Projekt Hörakustik', NULL, 3, 2),\n" +
"('I', NULL, 1, 'Peter', 'Einführung BWL', '0701', 3, 1),\n" +
"('I', NULL, 1, 'Peter', 'Einführung BWL', '0701', 3, 2),\n" +
"('I', NULL, 1, 'Bayer', 'Wirtschaftsmathematik', '0701', 5, 1),\n" +
"('I', NULL, 1, 'Bayer', 'Wirtschaftsmathematik', '0701', 5, 2),\n" +
"('I', NULL, 1, 'Bayer', 'Statistik', '0701', 5, 4),\n" +
"('I', NULL, 1, 'Bayer', 'Statistik', '0701', 5, 5),\n" +
"('I', NULL, 1, 'Strauß', 'Einführung in das Recht  Gruppe', '0701', 2, 4),\n" +
"('I', NULL, 1, 'Schlunsky', 'Buchführung', '0129', 3, 3),\n" +
"('I', NULL, 1, 'Schlunsky', 'Buchführung', 'AH 1.01', 4, 4),\n" +
"('I', NULL, 1, 'Francis-Binder', 'Wirtschaftsenglisch C1.1', '0007', 1, 2),\n" +
"('I', NULL, 1, 'Francis-Binder', 'Wirtschaftsenglisch C1.1', '0007', 1, 1),\n" +
"('I', NULL, 1, 'Francis-Binder', 'Wirtschaftsenglisch C1.1', '0007', 1, 3),\n" +
"('I', NULL, 1, 'Francis-Binder', 'Culture & Communication', '0701', 2, 3),\n" +
"('I', NULL, 1, 'Kolbert', 'Wirtschaftsspanisch A2.1', '0114', 2, 1),\n" +
"('I', NULL, 1, 'Kolbert', 'Wirtschaftsspanisch A2.1', '0106', 4, 2),\n" +
"('I', NULL, 1, 'Kolbert', 'Wirtschaftsspanisch A2.1', '0114', 2, 2),\n" +
"('I', NULL, 1, 'Kolbert', 'Wirtschaftsspanisch A2.1', '0103', 4, 1),\n" +
"('I', NULL, 1, 'Kessler', 'Wirtschaftsfranzösisch B2.1', '0007', 2, 2),\n" +
"('I', NULL, 1, 'Kessler', 'Wirtschaftsfranzösisch B2.1', '0007', 4, 3),\n" +
"('B', NULL, 2, 'Freimuth', 'Wirtschaftsrecht', '0258', 5, 1),\n" +
"('B', NULL, 2, 'Freimuth', 'Wirtschaftsrecht', '0258', 5, 2),\n" +
"('B', NULL, 2, 'Amann-Schindler', 'Arbeitsrecht', '0101', 3, 5),\n" +
"('B', NULL, 2, 'Morgado', 'Wirtschaftsenglisch 2', '0213', 1, 2),\n" +
"('B', NULL, 2, 'Morgado', 'Wirtschaftsenglisch 2', '0213', 1, 3),\n" +
"('B', NULL, 2, 'Stiefl', 'Wirtschaftsstatistik', '0106', 4, 4),\n" +
"('B', NULL, 2, 'Stiefl', 'Wirtschaftsstatistik', '0106', 4, 5),\n" +
"('B', NULL, 2, 'Bischof', 'Kosten- und Erlösrechnung', '0122', 4, 2),\n" +
"('B', NULL, 2, 'Bischof', 'Kosten- und Erlösrechnung', '0122', 4, 3),\n" +
"('B', NULL, 2, 'May', 'Grundl. Jahresab. und Bilanz.', '0258', 3, 2),\n" +
"('B', NULL, 2, 'May', 'Grundl. Jahresab. und Bilanz.', '0258', 3, 3),\n" +
"('MWI', NULL, 1, 'Knecktys', 'Quantitative Methoden und Statistik', '0258', 1, 4),\n" +
"('MWI', NULL, 1, 'Knecktys', 'Quantitative Methoden und Statistik', '0258', 1, 5),\n" +
"('MWI', NULL, 1, 'Knecktys', 'Quantitative Methoden und Statistik', '0211', 5, 4),\n" +
"('MWI', NULL, 1, 'Knecktys', 'Quantitative Methoden und Statistik', '0211', 5, 5),\n" +
"('I', NULL, 2, 'Bayer', 'Statistische & empirische Methoden', '0211', 4, 5),\n" +
"('I', NULL, 2, 'Bayer', 'Statistische & empirische Methoden', '0211', 4, 6),\n" +
"('I', NULL, 2, 'Strauß', 'Wirtschaftsrecht', 'AH 1.02', 2, 5),\n" +
"('I', NULL, 2, 'Strauß', 'Wirtschaftsrecht', '0273', 4, 3),\n" +
"('I', NULL, 2, 'Heyd', 'Bilanzierung u. Jahresabschluss nach HGB', 'AH 1.01', 5, 1),\n" +
"('I', NULL, 2, 'Heyd', 'Bilanzierung u. Jahresabschluss nach HGB', 'AH 1.01', 5, 2),\n" +
"('B', NULL, 3, 'Zarzuela', 'Spanisch A1', '0233', 4, 4),\n" +
"('B', NULL, 3, 'Zarzuela', 'Spanisch A1', '0233', 4, 5),\n" +
"('B', NULL, 3, 'Stiefl', 'Finanzierung  /  Kapitalbeschaffung', '0223', 2, 4),\n" +
"('B', NULL, 3, 'Stiefl', 'Finanzierung  /  Kapitalbeschaffung', '0223', 2, 5),\n" +
"('B', NULL, 3, 'Möhring', 'Wirtschaftsinformatik', '0133', 3, 5),\n" +
"('B', NULL, 3, 'Möhring', 'Wirtschaftsinformatik', '0133', 3, 6),\n" +
"('I', NULL, 2, 'Möhring', 'Wirtschaftsinformatik', '0133', 3, 5),\n" +
"('I', NULL, 2, 'Möhring', 'Wirtschaftsinformatik', '0133', 3, 6),\n" +
"('I', NULL, 2, 'Vogel Chr.', 'Wirtschaftsinformatik', '0273', 6, 2),\n" +
"('I', NULL, 2, 'Vogel Chr.', 'Wirtschaftsinformatik', '0273', 6, 3);" 
,"INSERT INTO g02_vorlesungsplan (stg, schwerpunkt, semester, dozent, kurs, raum, tagnr,  zeitblocknr) VALUES\n" +
"('B', NULL, 3, 'Vogel Chr.', 'Wirtschaftsinformatik', '0273', 6, 2),\n" +
"('B', NULL, 3, 'Vogel Chr.', 'Wirtschaftsinformatik', '0273', 6, 3),\n" +
"('B', NULL, 3, 'Bischof', 'Grundlagen des Controlling + Übung', '0213', 1, 4),\n" +
"('B', NULL, 3, 'Bischof', 'Grundlagen des Controlling + Übung', '0213', 1, 5),\n" +
"('B', NULL, 3, 'Schlipf', 'Grundlagen des Personalmanagement', '0223', 2, 6),\n" +
"('B', NULL, 3, 'Battista', 'Wirtschaftsenglisch C1.1', '0226', 4, 2),\n" +
"('B', NULL, 3, 'Battista', 'Wirtschaftsenglisch C1.1', '0226', 4, 3),\n" +
"('B', NULL, 3, 'May', 'Investitionsrechnung', 'AH 0.01', 5, 2),\n" +
"('B', NULL, 3, 'May', 'Investitionsrechnung', 'AH 1.01', 5, 3),\n" +
"('B', NULL, 3, 'Morlock', 'Projektmanagement', '0213', 3, 2),\n" +
"('B', NULL, 3, 'Morlock', 'Projektmanagement', '0213', 3, 3),\n" +
"('B', NULL, 3, 'Morlock', 'Projektmanagement', '0121', 5, 4),\n" +
"('I', NULL, 2, 'Villagrasa', 'Wirtschaftsspanisch A2.2', '0222', 2, 1),\n" +
"('I', NULL, 2, 'Villagrasa', 'Wirtschaftsspanisch A2.2', '0225', 4, 2),\n" +
"('I', NULL, 2, 'Villagrasa', 'Wirtschaftsspanisch A2.2', '0222', 2, 2),\n" +
"('I', NULL, 2, 'Villagrasa', 'Wirtschaftsspanisch A2.2', '0223', 4, 1),\n" +
"('I', NULL, 2, 'Kessler', 'Wirtschaftsfranzösisch B2.2', '0007', 2, 3),\n" +
"('I', NULL, 2, 'Kessler', 'Wirtschaftsfranzösisch B2.2', '0007', 4, 2),\n" +
"('I', NULL, 3, 'Heyd', 'Kosten- u. Leistungsrechnung', 'AH 0.01', 5, 3),\n" +
"('I', NULL, 3, 'Heyd', 'Kosten- u. Leistungsrechnung', 'AH 0.01', 5, 4),\n" +
"('I', NULL, 3, 'Bayer', 'Investition und Finanzierung', 'AH -1.01', 4, 3),\n" +
"('I', NULL, 3, 'Bayer', 'Investition und Finanzierung', 'AH -1.01', 4, 4),\n" +
"('I', NULL, 3, 'Frick G.', 'Organizational Structure', 'AH 0.01', 4, 5),\n" +
"('I', NULL, 3, 'Frick G.', 'Organizational Structure', 'AH 0.01', 4, 6),\n" +
"('I', NULL, 3, 'Frick G.', 'Organizational Behavior', 'AH 0.01', 3, 2),\n" +
"('I', NULL, 3, 'Frick G.', 'Organizational Behavior', 'AH 0.01', 3, 3),\n" +
"('I', NULL, 3, 'Rosas-Landa', 'Wirtschaftsspanisch B1.1', '0222', 1, 1),\n" +
"('I', NULL, 3, 'Rosas-Landa', 'Wirtschaftsspanisch B1.1', '0211', 5, 2),\n" +
"('I', NULL, 3, 'Rosas-Landa', 'Wirtschaftsspanisch B1.1', '0105', 1, 2),\n" +
"('I', NULL, 3, 'Rosas-Landa', 'Wirtschaftsspanisch B1.1', '0211', 5, 1),\n" +
"('I', NULL, 3, 'Breining', 'Wirtschaftsfranzösisch C1.1', '0112', 4, 1),\n" +
"('I', NULL, 3, 'Breining', 'Wirtschaftsfranzösisch C1.1', '0112', 4, 2),\n" +
"('I', NULL, 3, 'Halder', 'Betriebswirtschaftliche  Anwendungssoftware + Übung', '0267', 1, 3),\n" +
"('I', NULL, 3, 'Halder', 'Betriebswirtschaftliche  Anwendungssoftware + Übung', '0267', 1, 4),\n" +
"('I', NULL, 3, 'Halder', 'Betriebswirtschaftliche  Anwendungssoftware + Übung', '0267', 1, 5),\n" +
"('I', NULL, 3, 'Halder', 'Betriebswirtschaftliche  Anwendungssoftware + Übung', '0267', 1, 6),\n" +
"('I', NULL, 3, 'Halder', 'Betriebswirtschaftliche  Anwendungssoftware + Übung', '0267', 1, 7),\n" +
"('MWI', NULL, 1, 'Baumann T.', 'Big Data - Grundlagen', '0233', 2, 2),\n" +
"('MWI', NULL, 1, 'Baumann T.', 'Big Data - Grundlagen', '0233', 2, 3),\n" +
"('MWI', NULL, 1, 'Vladyshevska', 'International Project Management', '0130', 3, 2),\n" +
"('MWI', NULL, 1, 'Vladyshevska', 'International Project Management', '0130', 3, 3),\n" +
"('MWI', NULL, 1, 'Gunsenheimer', 'Customer Relationsship Management', '0105', 4, 5),\n" +
"('MWI', NULL, 1, 'Gunsenheimer', 'Customer Relationsship Management', '0105', 4, 6),\n" +
"('I', NULL, 4, 'Ravens', 'Grundlagen des Marketing', '0222', 5, 1),\n" +
"('I', NULL, 4, 'Ravens', 'Grundlagen des Marketing', '0222', 5, 2),\n" +
"('B', NULL, 6, 'Peter', 'Unternehmenssteuern', '0133', 1, 3),\n" +
"('B', NULL, 6, 'Peter', 'Unternehmenssteuern', 'AH -1.01', 2, 1),\n" +
"('I', NULL, 4, 'Peter', 'Unternehmenssteuern', '0133', 1, 3),\n" +
"('I', NULL, 4, 'Peter', 'Unternehmenssteuern', 'AH -1.01', 2, 1),\n" +
"('I', NULL, 4, 'Scheuermann', 'Kapitalmärkte', 'AH 1.01', 3, 2),\n" +
"('I', NULL, 4, 'Scheuermann', 'Kapitalmärkte', 'AH 1.01', 3, 3),\n" +
"('I', NULL, 4, 'Voss', 'Logistik', '0112', 5, 4),\n" +
"('I', NULL, 4, 'Voss', 'Logistik', '0112', 5, 5),\n" +
"('I', NULL, 4, 'Voss', 'Logistik', '0112', 5, 6),\n" +
"('I', NULL, 4, 'Frick G.', 'Studienarbeit Projekt', '0234', 1, 5),\n" +
"('I', NULL, 4, 'Frick G.', 'Studienarbeit Projekt', '0234', 1, 6),\n" +
"('I', NULL, 4, 'Pocovi', 'Wirtschaftsspanisch B1.2', '0014', 2, 3),\n" +
"('I', NULL, 4, 'Pocovi', 'Wirtschaftsspanisch B1.2', '0014', 4, 2),\n" +
"('I', NULL, 4, 'Pocovi', 'Wirtschaftsspanisch B1.2', '0014', 2, 2),\n" +
"('I', NULL, 4, 'Pocovi', 'Wirtschaftsspanisch B1.2', '0014', 4, 3),\n" +
"('I', NULL, 4, 'Breining', 'Wirtschaftsfranzösisch C1.2', '0112', 4, 3),\n" +
"('I', NULL, 4, 'Breining', 'Wirtschaftsfranzösisch C1.2', '0112', 4, 4);" 
,"INSERT INTO g02_vorlesungsplan (stg, schwerpunkt, semester, dozent, kurs, raum, tagnr,  zeitblocknr) VALUES\n" +
            "('B', NULL, 6, 'Peter K.', 'Tutorium Steuern', '0211', 1, 6),\n" +
"('B', NULL, 6, 'Motte', 'Strategische Unternehmensplanung', '0105', 2, 2),\n" +
"('B', NULL, 6, 'Motte', 'Businessplan-Wettbewerb', '0130', 1, 4),\n" +
"('B', NULL, 6, 'Motte', 'Businessplan-Wettbewerb', '0105', 1, 5),\n" +
"('B', NULL, 6, 'Motte', 'Grundlagen der Existenzgründung', '0122', 2, 3),\n" +
"('B', NULL, 6, 'Härting', 'Controlling u. Inf.Systeme 1', '0215', 4, 4),\n" +
"('B', NULL, 6, 'Härting', 'Controlling u. Inf.Systeme 1', '0215', 4, 5),\n" +
"('B', NULL, 6, 'Morlock', 'Logistikprojekt', '0223', 5, 2),\n" +
"('B', NULL, 6, 'Morlock', 'Logistikprojekt', '0223', 5, 3),\n" +
"('B', NULL, 6, 'Stiefl', 'Corporate Finance', '0216', 3, 2),\n" +
"('B', NULL, 6, 'Stiefl', 'Corporate Finance', '0215', 3, 3),\n" +
"('B', NULL, 6, 'Renz', 'Personal- und Organisationsmanagement 1', '0212', 2, 4),\n" +
"('B', NULL, 6, 'Renz', 'Personal- und Organisationsmanagement 1', '0212', 2, 5),\n" +
"('I', 'M', 6, 'Ravens', 'Internationales Marketing', '0125', 5, 3),\n" +
"('I', 'M', 6, 'Ravens', 'Internationales Marketing', '0125', 5, 4),\n" +
"('I', 'M', 6, 'Gentsch', 'CRM Basic', '0122', 5, 1),\n" +
"('I', 'M', 6, 'Gentsch', 'CRM Basic', '0122', 5, 2),\n" +
"('B', NULL, 1, 'Niethammer', 'Wirtschaftsmathematik', '0231', 1, 4),\n" +
"('B', NULL, 1, 'Niethammer', 'Wirtschaftsmathematik', '0231', 1, 5),\n" +
"('B', NULL, 1, 'Niethammer', 'Tutorium zur Mathematik Gruppe 2', '0203', 2, 5),\n" +
"('B', NULL, 1, 'Reichstein', 'Tutorium Mathematik Gruppe 1', '0203', 3, 3),\n" +
"('B', NULL, 1, 'Egetenmeier', 'Tutorium Mathematik Gruppe 3', '0111', 3, 3),\n" +
"('B', NULL, 1, 'Nissen', 'Mikroökonomik + Makroökonomik', '0112', 2, 1),\n" +
"('B', NULL, 1, 'Nissen', 'Mikroökonomik + Makroökonomik', '0112', 2, 2),\n" +
"('B', NULL, 1, 'Radtke', 'Allgemeine BWL  /  Betriebsorganisation', '0130', 1, 2),\n" +
"('B', NULL, 1, 'Radtke', 'Allgemeine BWL  /  Betriebsorganisation', '0130', 1, 3),\n" +
"('B', NULL, 1, 'May', 'Grundlagen Buchführung', '0203', 5, 1),\n" +
"('B', NULL, 1, 'Jakele', 'Übung Rechnungswesen / EDV', '0223', 2, 3),\n" +
"('B', NULL, 1, 'Jakele', 'Übung Rechnungswesen / EDV', '0203', 2, 4),\n" +
"('B', NULL, 1, 'Freimuth', 'Einführung in das Recht', '0111', 4, 4),\n" +
"('B', NULL, 1, 'Freimuth', 'Wirtschaftsrecht', '0112', 4, 5),\n" +
"('B', NULL, 1, 'Niethammer', 'Lern- u. Arbeitstechniken', '0120', 4, 2),\n" +
"('B', NULL, 1, 'Morgado', 'Englisch 1', '0121', 3, 1),\n" +
"('B', NULL, 1, 'Morgado', 'Englisch 1', '0121', 3, 2),\n" +
"('B', NULL, 4, 'Battista', 'Wirtschaftsenglisch C1.2', '0223', 4, 4),\n" +
"('B', NULL, 4, 'Battista', 'Wirtschaftsenglisch C1.2', '0223', 4, 5),\n" +
"('I', 'F, M', 6, 'Güida', 'Makroökonomie', '0233', 3, 3),\n" +
"('I', 'F, M', 6, 'Strauß', 'Intern. Wirtschaftsrecht', '0222', 1, 3),\n" +
"('I', 'F, M', 6, 'Strauß', 'Intern. Wirtschaftsrecht', '0222', 1, 4),\n" +
"('I', 'F, M', 6, 'Chung', 'Interkult. Management', '0222', 4, 4),\n" +
"('I', 'F, M', 6, 'Chung', 'Interkult. Management', '0222', 4, 5),\n" +
"('B', NULL, 4, 'Bischof, Wiedenmann', 'Nachhaltige Entwicklung / Unternehmensplanspiel', '0112', 2, 4),\n" +
"('B', NULL, 4, 'Bischof, Wiedenmann', 'Nachhaltige Entwicklung / Unternehmensplanspiel', '0112', 2, 5),\n" +
"('B', NULL, 4, 'Morlock', 'Logistik', '0234', 1, 2),\n" +
"('B', NULL, 4, 'Morlock', 'Logistik', '0234', 1, 3),\n" +
"('B', NULL, 4, 'Härting', 'E-Business  /  Neue Medien  /  Business Softw.', '0267', 2, 2),\n" +
"('B', NULL, 4, 'Härting', 'E-Business  /  Neue Medien  /  Business Softw.', '0275', 4, 2),\n" +
"('B', NULL, 4, 'Härting', 'E-Business  /  Neue Medien  /  Business Softw.', '0275', 4, 3),\n" +
"('I', 'M', 6, 'Härting', 'Electronic Business Strategy & Systems', '0267', 2, 2),\n" +
"('I', 'M', 6, 'Härting', 'Electronic Business Strategy & Systems', '0275', 4, 2),\n" +
"('I', 'M', 6, 'Härting', 'Electronic Business Strategy & Systems', '0275', 4, 3),\n" +
"('C', NULL, 1, 'Triebel', 'Allgemeine Chemie', '0132', 1, 4),\n" +
"('C', NULL, 1, 'Triebel', 'Allgemeine Chemie', '0231', 2, 4),\n" +
"('C', NULL, 1, 'Triebel', 'Allgemeine Chemie', '0132', 3, 5),\n" +
"('C', NULL, 1, 'Stein', 'Stöchiometrie', '0131', 4, 2),\n" +
"('C', NULL, 1, 'Flottmann', 'Laborkunde', '0132', 3, 1),\n" +
"('C', NULL, 2, 'Schäfer R.', 'Anorganische Chemie 1', '0114', 5, 2),\n" +
"('VI', NULL, 2, 'Matena', 'Mathematik - Vertiefung', '0105', 3, 1),\n" +
"('VI', NULL, 2, 'Schulz (CV)', 'Statistik', 'AH 1.02', 1, 4),\n" +
"('V', NULL, 3, 'Wegmann', 'Festigkeitslehre', 'AH 1.02', 5, 1),\n" +
"('VMM', NULL, 3, 'Wegmann', 'Festigkeitslehre', 'AH 1.02', 5, 1),\n" +
"('VMG', NULL, 3, 'Wegmann', 'Festigkeitslehre', 'AH 1.02', 5, 1),\n" +
"('VI', NULL, 2, 'Wegmann', 'Festigkeitslehre', 'AH 1.02', 5, 1),\n" +
"('C', NULL, 2, 'Triebel', 'Analytische Chemie 1', '0121', 5, 3),\n" +
"('C', NULL, 3, 'Schäfer R.', 'Anorganische Chemie 2', '0132', 3, 2),\n" +
"('C', NULL, 3, 'Junker', 'Organische Chemie 1', '0215', 3, 4),\n" +
"('C', NULL, 3, 'Schäfer R.', 'Anorganische Chemie 2', '0115', 4, 3),\n" +
"('C', NULL, 3, 'Wagner (C)', 'Physikalische Chemie 2', '0203', 2, 2),\n" +
"('C', NULL, 3, 'Stein', 'Physikalische Chemie 3', '0122', 3, 3),\n" +
"('C', NULL, 3, 'Schäfer R.', 'Technische Chemie 1', '0212', 3, 1),\n" +
"('C', NULL, 4, 'Beck (C)', 'Polymerchemie', '0125', 3, 2),\n" +
"('C', NULL, 4, 'Beck (C)', 'Polymerchemie', '0226', 5, 2),\n" +
"('C', NULL, 4, 'Schaschke', 'Organische Chemie 2', '0216', 3, 3),\n" +
"('C', NULL, 4, 'Schaschke', 'Organische Chemie 2', '0216', 3, 4),\n" +
"('C', NULL, 4, 'Wagner (C)', 'Physikalische Chemie 4', '0114', 5, 1),\n" +
"('C', NULL, 4, 'Wagner (C)', 'Physikalische Chemie 4', '0125', 3, 1),\n" +
"('M', NULL, 1, 'Alpers', 'Mathematik 1', 'AH 1.02', 1, 1),\n" +
"('M', NULL, 1, 'Alpers', 'Mathematik 1', '0273', 3, 1),\n" +
"('M', NULL, 1, 'Alpers', 'Mathematik 1', '0273', 4, 1),\n" +
"('P', NULL, 1, 'Rissner', 'Experimentalphysik', '0201', 1, 1),\n" +
"('P', NULL, 1, 'Rissner', 'Experimentalphysik', 'AH 0.01', 2, 6),\n" +
"('P', NULL, 1, 'Rissner', 'Experimentalphysik', 'AH 0.01', 3, 6),\n" +
"('M', NULL, 1, 'Günter', 'Technische Mechanik 1', 'AH 1.01', 2, 3),\n" +
"('M', NULL, 1, 'Günter', 'Technische Mechanik 1', '0133', 3, 2),\n" +
"('M', NULL, 1, 'Günter', 'Technische Mechanik 1', '0133', 3, 3),\n" +
"('P', NULL, 1, 'Weber (P)', 'Mathematik 1', '0130', 2, 4),\n" +
"('P', NULL, 1, 'Weber (P)', 'Mathematik 1', '0111', 3, 1),\n" +
"('P', NULL, 1, 'Weber (P)', 'Mathematik 1', '0121', 3, 3),\n" +
"('MP', NULL, 1, 'Plotzitza', 'Technische Mechanik', '0114', 4, 2),\n" +
"('MP', NULL, 1, 'Plotzitza', 'Technische Mechanik', '0114', 4, 3),\n" +
"('M', NULL, 1, 'Brenner', 'Technisches Zeichnen:CAD', '0290', 2, 4),\n" +
"('M', NULL, 1, 'Brenner', 'Technisches Zeichnen:CAD', '0290', 2, 5);" 
,"INSERT INTO g02_vorlesungsplan (stg, schwerpunkt, semester, dozent, kurs, raum, tagnr,  zeitblocknr) VALUES\n" +
            "('MP', NULL, 1, 'Schon', 'Mathematik 1', '0105', 1, 1),\n" +
"('MP', NULL, 1, 'Schon', 'Mathematik 1', '0212', 4, 1),\n" +
"('M', NULL, 1, 'Schmidt (M)', 'Technisches Zeichnen:CAD', '0290', 1, 4),\n" +
"('M', NULL, 1, 'Schmidt (M)', 'Technisches Zeichnen:CAD', '0290', 1, 5),\n" +
"('M', NULL, 1, 'Pannert', 'Experimentalphysik', '0273', 1, 3),\n" +
"('M', NULL, 1, 'Pannert', 'Experimentalphysik', '0258', 2, 1),\n" +
"('M', NULL, 1, 'Pannert', 'Experimentalphysik', '0202', 4, 3),\n" +
"('M', NULL, 3, 'Alpers', 'Mathematik 3', 'AH 0.01', 1, 4),\n" +
"('P', NULL, 2, 'Alpers', 'Mathematik 2', '0258', 1, 3),\n" +
"('C', NULL, 1, 'Faber, Wagner (C), Pfundstein', 'Praktikum Physik', 'GS 1.15', 2, 1),\n" +
"('C', NULL, 1, 'Faber, Wagner (C), Pfundstein', 'Praktikum Physik', 'GS 1.15', 2, 2),\n" +
"('C', NULL, 1, 'Faber, Wagner (C), Pfundstein', 'Praktikum Physik', 'GS 1.15', 2, 3),\n" +
"('C', NULL, 2, 'Pfundstein, Wagner (C)', 'Prakt. Phys. Chemie 1', '0153', 4, 1),\n" +
"('C', NULL, 2, 'Pfundstein, Wagner (C)', 'Prakt. Phys. Chemie 1', '0153', 4, 4),\n" +
"('C', NULL, 2, 'Pfundstein, Wagner (C)', 'Prakt. Phys. Chemie 1', '0153', 4, 5),\n" +
"('C', NULL, 2, 'Pfundstein, Wagner (C)', 'Prakt. Phys. Chemie 1', '0153', 4, 6),\n" +
"('C', NULL, 3, 'Pfundstein, Wagner (C)', 'Prakt. Phys. Chemie 2', '0153', 2, 4),\n" +
"('C', NULL, 3, 'Pfundstein, Wagner (C)', 'Prakt. Phys. Chemie 2', '0153', 2, 5),\n" +
"('C', NULL, 3, 'Pfundstein, Wagner (C)', 'Prakt. Phys. Chemie 2', '0153', 2, 6),\n" +
"('P', NULL, 2, 'Alpers', 'Mathematik 2', '0121', 2, 3),\n" +
"('P', NULL, 2, 'Alpers', 'Mathematik 2', '0102', 3, 3),\n" +
"('M', NULL, 2, 'Alpers', 'Mathematik 2', '0258', 1, 3),\n" +
"('M', NULL, 2, 'Alpers', 'Mathematik 2', '0121', 2, 3),\n" +
"('M', NULL, 2, 'Alpers', 'Mathematik 2', '0102', 3, 3),\n" +
"('P', NULL, 2, 'Weidner', 'Simulationswerkzeuge zu den Maschinenelementen', '0290', 3, 1),\n" +
"('M', NULL, 2, 'Otto K. -P', 'Technische Mechanik 2', '0133', 2, 1),\n" +
"('M', NULL, 2, 'Otto K. -P', 'Technische Mechanik 2', '0133', 2, 2),\n" +
"('M', NULL, 2, 'Otto K. -P', 'Technische Mechanik 2', '0133', 4, 1),\n" +
"('P', NULL, 2, 'Otto K. -P', 'Dynamik', '0133', 2, 1),\n" +
"('P', NULL, 2, 'Otto K. -P', 'Dynamik', '0133', 2, 2),\n" +
"('P', NULL, 2, 'Otto K. -P', 'Dynamik', '0133', 4, 1),\n" +
"('P', NULL, 3, 'Pittius', 'Maschinenelemente 2', 'AH -1.01', 5, 1),\n" +
"('P', NULL, 3, 'Pittius', 'Maschinenelemente 2', 'AH -1.01', 5, 2),\n" +
"('K', NULL, 3, 'Pittius', 'Maschinenelemente 2', 'AH -1.01', 5, 1),\n" +
"('K', NULL, 3, 'Pittius', 'Maschinenelemente 2', 'AH -1.01', 5, 2),\n" +
"('P', NULL, 3, 'Weidner', 'Simulationswerkzeuge zu den Maschinenelementen', '0290', 2, 1),\n" +
"('M', NULL, 3, 'Pannert', 'Elektrotechnik', 'AH 0.01', 2, 3),\n" +
"('M', NULL, 3, 'Pannert', 'Elektrotechnik', 'AH 1.02', 4, 4),\n" +
"('P', NULL, 3, 'Weber (P)', 'Informatik', '0111', 1, 1),\n" +
"('P', NULL, 3, 'Weber (P)', NULL, '0290', 1, 1),\n" +
"('M', NULL, 4, 'Pannert', 'Elektr. Antriebe', 'AH 0.01', 2, 2),\n" +
"('P', NULL, 3, 'Weber (P)', 'Informatik', '0115', 2, 3),\n" +
"('M', NULL, 3, 'Körner', 'Maschinenelemente 2', 'AH 1.01', 2, 4),\n" +
"('M', NULL, 3, 'Körner', 'Maschinenelemente 2', 'AH 1.02', 3, 3),\n" +
"('M', NULL, 3, 'Körner', 'Maschinenelemente 2', 'AH 1.02', 5, 2),\n" +
"('M', NULL, 3, 'Körner', 'Maschinenelemente 2', 'AH 1.02', 5, 3),\n" +
"('P', NULL, 3, 'Weber (P)', 'Informatik', '0290', 2, 2),\n" +
"('P', NULL, 3, 'Salvasohn', 'Grundlagen der Elektrotechnik', '0133', 2, 4),\n" +
"('P', NULL, 3, 'Salvasohn', 'Grundlagen der Elektrotechnik', '0133', 2, 5),\n" +
"('K', NULL, 3, 'Salvasohn', 'Elektrotechnik', '0133', 2, 4),\n" +
"('K', NULL, 3, 'Salvasohn', 'Elektrotechnik', '0133', 2, 5),\n" +
"('P', NULL, 4, 'Knecktys', 'Elektrische Antriebe', '0103', 5, 1),\n" +
"('VI', NULL, 6, 'Knecktys', 'Antriebstechnik 1', '0103', 5, 1),\n" +
"('VMM', NULL, 7, 'Knecktys', 'Antriebstechnik 1', '0103', 5, 1),\n" +
"('M', 'E', 6, 'Ruf', 'Messdatenverarb. u. Sensortechn.', '0212', 5, 1),\n" +
"('P', NULL, 6, 'Wagner', 'Steuern / Regeln II', '0106', 3, 1),\n" +
"('P', NULL, 6, 'Wagner', 'Steuern / Regeln II', '0106', 3, 2),\n" +
"('P', NULL, 6, 'Pannert', 'Sound Design und Akustik', '0101', 3, 3),\n" +
"('M', 'E, R, K', 6, 'Kley', 'Konstruktionslehre 2', '0203', 1, 2),\n" +
"('M', 'E, R, K', 6, 'Merkel', 'Finite Elemente Methoden  /  FEM', '0289', 5, 2),\n" +
"('M', 'E, R, K', 6, 'Wagner', 'Steuern und Regeln 2', '0106', 3, 1),\n" +
"('M', 'E, R, K', 6, 'Wagner', 'Steuern und Regeln 2', '0106', 3, 2),\n" +
"('M', 'E, R, K', 6, 'Pannert', 'Technische Akustik', '0101', 3, 3),\n" +
"('M', 'E, K, R', 7, 'Pannert', 'Technische Akustik', '0101', 3, 3),\n" +
"('M', 'E', 6, 'Hubel, Ruf', 'Messdatenverarb. u. Sensortechn.', '0142, 0122', 1, 1),\n" +
"('M', 'E, R, K', 6, 'Hubel, Wagner, Zorniger', 'Steuerungstechnik u. Labor', '0105, 0187', 2, 4),\n" +
"('M', 'E, R, K', 6, 'Hubel, Wagner, Zorniger', 'Steuerungstechnik u. Labor', '0105, 0187', 2, 5),\n" +
"('M', NULL, 4, 'Pannert', 'Elektr. Antriebe - Labor', 'AH 1.02, G2 2.28', 3, 2),\n" +
"('M', NULL, 4, 'Pannert', 'Elektr. Antriebe - Labor', 'AH 1.02, G2 2.28', 3, 1),\n" +
"('P', NULL, 4, 'Eisenmenger, Hartjes', 'Messtechnik', '0133', 5, 2),\n" +
"('P', NULL, 4, 'Eisenmenger, Hartjes', 'Messtechnik', '0133', 5, 3),\n" +
"('M', NULL, 4, 'Eisenmenger, Hartjes', 'Messtechnik', '0133', 5, 2),\n" +
"('M', NULL, 4, 'Eisenmenger, Hartjes', 'Messtechnik', '0133', 5, 3),\n" +
"('M', NULL, 4, 'Kley, Thomisch', 'Konstruktionslehre 1', '0105', 2, 3),\n" +
"('M', 'K', 7, 'Gretzschel', 'Fahrzeugantrieb', '0120', 1, 5),\n" +
"('P', NULL, 7, 'Gretzschel', 'Fahrzeugantrieb', '0120', 1, 5),\n" +
"('M', 'K', 6, 'Gulotta', 'Fahrzeuglenkung', '0103', 3, 6),\n" +
"('P', NULL, 6, 'Gulotta', 'Fahrzeuglenkung', '0103', 3, 6),\n" +
"('P', NULL, 6, 'Class', 'Produktentwicklung mit Kunststoffen', '0121', 1, 1),\n" +
"('P', NULL, 6, 'Weidner', 'Rapid Prototyping', '0212', 2, 3),\n" +
"('P', NULL, 6, 'Schulz (CV)', 'Techn. Statistik', '0111', 1, 3),\n" +
"('K', NULL, 7, 'Ruf', 'Prüfmethoden', '0103', 1, 3),\n" +
"('M', 'E', 7, 'Berger (F)', 'Rapid Prototyping', '0120', 1, 1),\n" +
"('M', 'E', 7, 'Merkel', 'Konstruktionslehre 3  /  Leichtb.', 'AH 0.01', 5, 1),\n" +
"('O', NULL, 2, 'Heinrich', 'Physik 2', 'G1 1.01', 2, 3),\n" +
"('O', NULL, 2, 'Heinrich', 'Physik 2', 'G1 0.05', 4, 1),\n" +
"('O', NULL, 3, 'Schneider (O)', 'Mathematik für Optik und Elektronik', 'G1 1.23', 2, 4),\n" +
"('O', NULL, 3, 'Schneider (O)', 'Mathematik für Optik und Elektronik', 'G1 1.23', 2, 5),\n" +
"('O', NULL, 3, 'Schneckenburger', 'Festkörperphysik', 'G1 1.01', 1, 3),\n" +
"('O', NULL, 3, 'Kettler', 'Digitale Elektronik - Labor', 'G1 2.36', 3, 1),\n" +
"('O', NULL, 3, 'Kettler', 'Digitale Elektronik - Labor', 'G1 2.36', 3, 2),\n" +
"('O', NULL, 3, 'Fritz Dietmar', NULL, 'G1 2.36', 3, 1),\n" +
"('O', NULL, 3, 'Fritz Dietmar', NULL, 'G1 2.36', 3, 2),\n" +
"('O', NULL, 6, 'Krapp', 'Optische Kommunikationstechnik', 'G1 0.01', 3, 1),\n" +
"('O', NULL, 6, 'Krapp', 'Optische Kommunikationstechnik', 'G1 0.01', 4, 2),\n" +
"('O', NULL, 6, 'Krapp', 'Optische Kommunikationstechnik', 'G1 0.21', 4, 3),\n" +
"('O', NULL, 4, 'Hellmuth', 'Einführung Optik Design', 'G1 2.36', 4, 2),\n" +
"('O', NULL, 4, 'Hellmuth', 'Einführung Optik Design', 'G1 2.36', 4, 3),\n" +
"('VI', NULL, 1, 'Rist', 'Mathematische Grundlagen', 'AH 1.01', 2, 6),\n" +
"('VI', NULL, 1, 'Rist', 'Mathematische Grundlagen', 'AH 1.01', 3, 6),\n" +
"('V', NULL, 3, 'Wegmann', 'Kinematik und Kinetik', '0273', 2, 3),\n" +
"('VMM', NULL, 3, 'Wegmann', 'Kinematik und Kinetik', '0273', 2, 3),\n" +
"('VMG', NULL, 3, 'Wegmann', 'Kinematik und Kinetik', '0273', 2, 3),\n" +
"('VI', NULL, 2, 'Wegmann', 'Kinematik und Kinetik', '0273', 2, 3),\n" +
"('V', NULL, 1, 'Heine', 'Einführung in die Metallkunde', 'AH -1.01', 1, 2),\n" +
"('V', NULL, 1, 'Heine', 'Einführung in die Metallkunde', 'AH -1.01', 2, 2),\n" +
"('VMG', NULL, 1, 'Heine', 'Einführung in die Metallkunde', 'AH -1.01', 1, 2),\n" +
"('VMG', NULL, 1, 'Heine', 'Einführung in die Metallkunde', 'AH -1.01', 2, 2),\n" +
"('VMM', NULL, 1, 'Heine', 'Einführung in die Metallkunde', 'AH -1.01', 1, 2),\n" +
"('VMM', NULL, 1, 'Heine', 'Einführung in die Metallkunde', 'AH -1.01', 2, 2),\n" +
"('M', NULL, 1, 'Heine', 'Werkstoffkunde 1', 'AH -1.01', 1, 2),\n" +
"('M', NULL, 1, 'Heine', 'Werkstoffkunde 1', 'AH -1.01', 2, 2),\n" +
"('VI', NULL, 3, 'Schrader', 'Strategische Unternehmensführung', '0103', 5, 2),\n" +
"('VI', NULL, 3, 'Schrader', 'Strategische Unternehmensführung', '0103', 5, 3),\n" +
"('VI', NULL, 4, 'Grohmann', 'Industrial Service Engineering', '0215', 4, 2),\n" +
"('VI', NULL, 4, 'Grohmann', 'Industrial Service Engineering', '0215', 4, 3),\n" +
"('VI', NULL, 4, 'Grohmann', 'Industrial Service Engineering', '0122', 4, 4),\n" +
"('VI', NULL, 4, 'Grohmann', 'Industrial Service Engineering', '0122', 4, 5),\n" +
"('VI', NULL, 2, 'Kallien', 'Grundlagen der Werkstoffkunde', '0112', 3, 2),\n" +
"('VI', NULL, 2, 'Kallien', 'Grundlagen der Werkstoffkunde', '0112', 3, 3),\n" +
"('VI', NULL, 2, 'Benz', 'Wirtschaftsinformatik', 'G2 1.30~AH -1.01', 5, 4),\n" +
"('VI', NULL, 2, 'Benz', 'Wirtschaftsinformatik', 'G2 1.30~AH -1.01', 5, 5),\n" +
"('VI', NULL, 2, 'Möckel', 'Chemische Grundlagen', '0115', 1, 3),\n" +
"('VI', NULL, 2, 'Stegmaier', 'Grundlagen Vertragsrecht', '0273', 3, 4),\n" +
"('VI', NULL, 2, 'Stegmaier', 'Grundlagen Vertragsrecht', '0273', 3, 5),\n" +
"('V', NULL, 4, 'Möckel', 'Lackiertechnik I', '0211', 1, 4),\n" +
"('V', NULL, 4, 'Bingel', 'Galvanotechnische Verfahren', '0129', 5, 1),\n" +
"('M', 'E, R, K', 6, 'Schulz (CV)', 'Techn. Statistik', '0111', 1, 3),\n" +
"('M', 'E, K, R', 7, 'Schulz (CV)', 'Techn. Statistik', '0111', 1, 3),\n" +
"('VI', NULL, 3, 'Albrecht, Faber', 'Physik - Labor', 'GS 1.15', 1, 4),\n" +
"('VI', NULL, 3, 'Albrecht, Faber', 'Physik - Labor', 'GS 1.15', 1, 5),\n" +
"('O', NULL, 3, 'Dittmar, Fritz Dietmar, Wagner (O)', 'Optoelektronische Bauelemente', 'G1 0.01', 2, 2),\n" +
"('O', NULL, 3, 'Dittmar, Fritz Dietmar, Wagner (O)', 'Optoelektronische Bauelemente', 'G1 0.01', 2, 3),\n" +
"('O', NULL, 2, 'Faber, Wagner (O)', 'Physik 2 - Labor', 'GS 1.15', 2, 4),\n" +
"('O', NULL, 2, 'Faber, Wagner (O)', 'Physik 2 - Labor', 'GS 1.15', 2, 5),\n" +
"('O', NULL, 3, 'Schneckenburger, Wagner (O)', 'Optik 1 Labor', 'G1 2.22', 4, 2),\n" +
"('O', NULL, 3, 'Schneckenburger, Wagner (O)', 'Optik 1 Labor', 'G1 2.22', 4, 3),\n" +
"('VI', NULL, 2, 'Stegmaier, Titze', 'Internationales Vertragsrecht', '0273', 3, 4),\n" +
"('VI', NULL, 2, 'Stegmaier, Titze', 'Internationales Vertragsrecht', '0273', 3, 5),\n" +
"('V', NULL, 3, 'Heine', 'Metallkundelabor', '0190', 4, 1),\n" +
"('V', NULL, 3, 'Heine', 'Metallkundelabor', '0190', 4, 2),\n" +
"('V', NULL, 3, 'Heine', 'Metallkundelabor', '0190', 4, 3),\n" +
"('VMM', NULL, 3, 'Heine', 'Metallkundelabor', '0190', 4, 1),\n" +
"('VMM', NULL, 3, 'Heine', 'Metallkundelabor', '0190', 4, 2),\n" +
"('VMM', NULL, 3, 'Heine', 'Metallkundelabor', '0190', 4, 3),\n" +
"('V', NULL, 3, 'Ladwein', 'Korrosion', '0212', 2, 2),\n" +
"('VMG', NULL, 3, 'Ladwein', 'Korrosion', '0212', 2, 2),\n" +
"('V', NULL, 3, 'Bingel', 'Galvanotechnik 1', '0234', 5, 4),\n" +
"('VMG', NULL, 3, 'Bingel', 'Galvanotechnik 1', '0234', 5, 4),\n" +
"('V', NULL, 6, 'Schuhmacher', 'Zerstörungsfreie Bauteilprüfung m. Labor', '0119', 1, 3),\n" +
"('V', NULL, 6, 'Schuhmacher', 'Zerstörungsfreie Bauteilprüfung m. Labor', '0119', 1, 4),\n" +
"('V', NULL, 6, 'Schuhmacher', 'Zerstörungsfreie Bauteilprüfung m. Labor', '0119', 1, 5),\n" +
"('VMM', NULL, 6, 'Schuhmacher', 'Zerstörungsfreie Bauteilprüfung m. Labor', '0119', 1, 3),\n" +
"('VMM', NULL, 6, 'Schuhmacher', 'Zerstörungsfreie Bauteilprüfung m. Labor', '0119', 1, 4),\n" +
"('VMM', NULL, 6, 'Schuhmacher', 'Zerstörungsfreie Bauteilprüfung m. Labor', '0119', 1, 5),\n" +
"('V', NULL, 6, 'Tilscher', 'Schadenskunde', '0119', 1, 1),\n" +
"('V', NULL, 6, 'Tilscher', 'Schadenskunde', '0119', 1, 2),\n" +
"('VMM', NULL, 6, 'Tilscher', 'Schadenskunde', '0119', 1, 1),\n" +
"('VMM', NULL, 6, 'Tilscher', 'Schadenskunde', '0119', 1, 2),\n" +
"('VI', NULL, 3, 'Görne', 'Grundlagen Maschinenelemente', '0111', 4, 1),\n" +
"('VI', NULL, 3, 'Görne', 'Grundlagen Maschinenelemente', '0111', 4, 2),\n" +
"('VI', NULL, 3, 'Görne', 'Grundlagen Maschinenelemente', '0111', 4, 3),\n" +
"('VI', NULL, 1, 'Düwel', 'Technical English', '0201', 2, 1),\n" +
"('VI', NULL, 1, 'Düwel', 'Technical English', '0201', 2, 2),\n" +
"('VI', NULL, 1, 'Dobler', 'Business Comm. in Englisch', '0101', 2, 1),\n" +
"('VI', NULL, 1, 'Dobler', 'Business Comm. in Englisch', '0101', 2, 2),\n" +
"('VI', NULL, 1, 'Albrecht', 'Grundlagen der Physik', '0112', 2, 3),\n" +
"('VI', NULL, 1, 'Albrecht', 'Grundlagen der Physik', '0203', 5, 2),\n" +
"('VI', NULL, 3, 'Goll', 'Elektrizitätslehre', 'AH 1.02', 1, 2),\n" +
"('VI', NULL, 6, 'Görne', 'Labor für technische Anwendungen', '0122', 1, 4),\n" +
"('VI', NULL, 6, 'Görne', 'Labor für technische Anwendungen', '0122', 1, 5),\n" +
"('VI', NULL, 6, 'Görne', 'Labor für technische Anwendungen', '0122', 1, 6),\n" +
"('VI', NULL, 3, 'Häger', 'Werkstoffkunde Labor', '0190', 2, 3),\n" +
"('VI', NULL, 3, 'Häger', 'Werkstoffkunde Labor', '0190', 2, 4),\n" +
"('VI', NULL, 3, 'Häger', 'Technische Strukturwerkstoffe', '0119', 2, 3),\n" +
"('VI', NULL, 3, 'Häger', 'Technische Strukturwerkstoffe', '0119', 2, 4),\n" +
"('VI', NULL, 6, '?', 'Labor für technische Anwendungen', '0215', 1, 1),\n" +
"('VI', NULL, 6, '?', 'Labor für technische Anwendungen', '0215', 1, 2),\n" +
"('VI', NULL, 6, '?', 'Labor für technische Anwendungen', '0215', 1, 3),\n" +
"('VI', NULL, 6, 'Görne', 'Operativer / Strategischer Vertrieb', '0106', 5, 4),\n" +
"('VI', NULL, 6, 'Görne', 'Operativer / Strategischer Vertrieb', '0106', 5, 5),\n" +
"('VI', NULL, 6, 'Fleißner', 'Verhandllungsführung und Präsentationstechnik', '0115', 3, 5),\n" +
"('VI', NULL, 6, 'Fleißner', 'Verhandllungsführung und Präsentationstechnik', '0115', 3, 6);" 
,"INSERT INTO g02_vorlesungsplan (stg, schwerpunkt, semester, dozent, kurs, raum, tagnr,  zeitblocknr) VALUES\n" +
            "('W', NULL, 1, 'Erhardt', 'Mathematik 1 a (SPO 31)', '0111', 2, 4),\n" +
"('W', NULL, 1, 'Erhardt', 'Mathematik 1 a (SPO 31)', '0701', 3, 4),\n" +
"('W', NULL, 1, 'Kunz', 'Mathematik 1 b (SPO 31)', 'AH 0.01', 1, 5),\n" +
"('W', NULL, 1, 'Kunz', 'Mathematik 1 b (SPO 31)', '0273', 2, 5),\n" +
"('W', NULL, 1, 'Holzbaur', 'Grundl. Projektmanagement mit Projekt (SPO 31)', '0273', 3, 6),\n" +
"('W', NULL, 1, 'Holzbaur', 'Grundl. Projektmanagement mit Projekt (SPO 31)', '0273', 3, 7),\n" +
"('W', NULL, 1, 'Stein C.', 'Technische Mechanik (SPO 31)', '0273', 2, 6),\n" +
"('W', NULL, 1, 'Stein C.', 'Technische Mechanik (SPO 31)', '0273', 2, 7),\n" +
"('W', NULL, 1, 'Mathy', 'Werkstoffkunde Labor (SPO 31)', '0258', 3, 1),\n" +
"('W', NULL, 1, 'Joos', 'Buchführung (SPO 31)', '0701', 3, 5),\n" +
"('W', NULL, 1, 'Knörzer', 'Einführung VWL / BWL (SPO 31)', '0273', 5, 4),\n" +
"('W', NULL, 1, 'Knörzer', 'Einführung VWL / BWL (SPO 31)', '0273', 5, 5),\n" +
"('W', NULL, 2, 'Hachtel (W)', 'Angewandten Technische  Mechanik (SPO 31)', '0103', 2, 2),\n" +
"('W', NULL, 2, 'Hachtel (W)', 'Angewandten Technische  Mechanik (SPO 31)', '0103', 2, 3),\n" +
"('W', NULL, 2, 'Hachtel (W)', 'Angewandten Technische  Mechanik (SPO 31)', '0211', 4, 2),\n" +
"('W', NULL, 2, 'Hachtel (W)', 'Angewandten Technische  Mechanik (SPO 31)', '0211', 4, 3),\n" +
"('W', NULL, 2, 'Saniter', 'Englisch Grundlagen (SPO 31)', '0103', 3, 4),\n" +
"('W', NULL, 2, 'Saniter', 'Englisch Grundlagen (SPO 31)', '0103', 3, 5),\n" +
"('W', NULL, 2, 'Löffler', 'Mathematik 2', '0223', 3, 2),\n" +
"('W', NULL, 2, 'Löffler', 'Mathematik 2', '0223', 3, 3),\n" +
"('W', NULL, 2, 'Löffler', 'Mathematik 2', '0101', 5, 1),\n" +
"('W', NULL, 2, 'Beck Y.', 'Einführung im Matlab / Simulink (SPO 31)', '0275', 2, 4),\n" +
"('W', NULL, 2, 'Beck Y.', 'Einführung im Matlab / Simulink (SPO 31)', '0275', 2, 5),\n" +
"('W', NULL, 3, 'Löffler', 'Physik 2 (Modellbildung)', '0101', 5, 2),\n" +
"('W', NULL, 3, 'Löffler', 'Physik 2 (Modellbildung) SPO 31', '0275', 5, 2),\n" +
"('W', NULL, 3, 'Beck Y.', 'Physik Praktikum (SPO 31)', 'GS 1.15', 3, 3),\n" +
"('W', NULL, 3, 'Beck Y.', 'Physik Praktikum (SPO 31)', 'GS 1.15', 3, 4),\n" +
"('W', NULL, 3, 'Beck Y.', 'Physik Praktikum (SPO 31)', 'GS 1.15', 3, 5),\n" +
"('W', NULL, 3, 'Stein C.', 'Konstruktion 1 (SPO 31)', '0111', 1, 4),\n" +
"('W', NULL, 2, 'Beck Y.', 'Physik 1 (SPO 31)', '0102, 0275', 2, 1),\n" +
"('W', NULL, 2, 'Beck Y.', 'Physik 1 (SPO 31)', '0213, 0275', 3, 1),\n" +
"('V', NULL, 3, 'Heine, Salzwedel', 'Werkstoffprüfung I und II', '0190, 0106', 2, 1),\n" +
"('VMM', NULL, 3, 'Heine, Salzwedel', 'Werkstoffprüfung I und II', '0190, 0106', 2, 1),\n" +
"('W', NULL, 3, 'Stein C.', 'Konstruktion 1 (SPO 31)', '0111', 1, 5),\n" +
"('W', NULL, 3, 'Nespeta', 'Materialwirtschaft + Übung (SPO 31)', '0258', 4, 4),\n" +
"('W', NULL, 3, 'Nespeta', 'Materialwirtschaft + Übung (SPO 31)', '0258', 4, 5),\n" +
"('W', NULL, 3, 'Zimmermann Axel', 'Elektrotechnik (SPO 31)', '0701', 1, 2),\n" +
"('W', NULL, 3, 'Zimmermann Axel', 'Elektrotechnik (SPO 31)', '0701', 1, 3),\n" +
"('W', NULL, 3, 'Depner', 'Einführung Wirtschaftsinformatik (SPO 31)', '0275', 2, 3),\n" +
"('W', NULL, 3, 'Depner', 'Softwaretechnologie (SPO 31)', '0275', 2, 2),\n" +
"('W', NULL, 4, 'Stein C.', 'Fertigungstechnik mit Lab or', '0101', 4, 2),\n" +
"('W', NULL, 4, 'Stein C.', 'Fertigungstechnik mit Lab or', '0101', 4, 3),\n" +
"('W', NULL, 4, 'Holzbaur', 'Qualitätsmanagement / NE', '0131', 2, 2),\n" +
"('W', NULL, 4, 'Holzbaur', 'Qualitätsmanagement / NE', '0131', 2, 3),\n" +
"('W', NULL, 4, 'Nespeta', 'Kostenrechnung', '0101', 1, 2),\n" +
"('W', NULL, 4, 'Nespeta', 'Kostenrechnung', '0101', 1, 3),\n" +
"('W', NULL, 4, 'Kleppmann Rita', 'Technisches Englisch B2', '0112', 5, 1),\n" +
"('W', NULL, 4, 'Kleppmann Rita', 'Technisches Englisch B2', '0112', 5, 2),\n" +
"('W', NULL, 4, 'Dobler', 'Wirtschaftsenglisch (Level B2)', '0213', 5, 1),\n" +
"('W', NULL, 4, 'Dobler', 'Wirtschaftsenglisch (Level B2)', '0213', 5, 2),\n" +
"('W', NULL, 7, 'Zimmermann Axel', 'Regelungstechnik', '0203', 1, 5),\n" +
"('W', NULL, 7, 'Zimmermann Axel', 'Regelungstechnik', '0203', 1, 6),\n" +
"('W', NULL, 7, 'Depner', 'Informatik-Projekte', '0216', 5, 2),\n" +
"('W', NULL, 7, 'Depner', 'Informatik-Projekte', '0216', 5, 3),\n" +
"('W', NULL, 4, 'Stein C.', 'Konstruktion 2 mit CAD', '0216', 2, 4),\n" +
"('W', NULL, 4, 'Stein C.', 'Konstruktion 2 mit CAD', '0216', 2, 5),\n" +
"('W', NULL, 7, 'Stein C.', 'Projekt CAD', '0216', 2, 4),\n" +
"('W', NULL, 7, 'Stein C.', 'Projekt CAD', '0216', 2, 5),\n" +
"('W', NULL, 4, 'Zimmermann Axel', 'Produktionsautomatisierung', '0111', 5, 4),\n" +
"('W', NULL, 4, 'Zimmermann Axel', 'Produktionsautomatisierung', '0111', 5, 5),\n" +
"('W', NULL, 4, 'Preikschas', 'Marketing Grundlagen', 'AH 0.01', 4, 4),\n" +
"('W', NULL, 4, 'Preikschas', 'Marketing Grundlagen', 'AH -1.01', 4, 5),\n" +
"('VI', NULL, 3, 'Preikschas', 'Industriegütermarketing', 'AH 0.01', 4, 4),\n" +
"('VI', NULL, 3, 'Preikschas', 'Industriegütermarketing', 'AH -1.01', 4, 5),\n" +
"('W', NULL, 4, 'Beck (W)', 'Operations Research - Übung', '0105', 1, 6),\n" +
"('W', NULL, 4, 'Beck (W)', 'Operations Research - Übung', '0103', 2, 6),\n" +
"('W', NULL, 6, 'Huptych', 'Systemdynamik', '0202', 5, 2),\n" +
"('W', NULL, 6, 'Huptych', 'Systemdynamik', '0202', 5, 3),\n" +
"('W', NULL, 6, 'Becker A.', 'Finanzwirtschaft  /  Fallstudien Projekt', '0234', 4, 2),\n" +
"('W', NULL, 6, 'Becker A.', 'Finanzwirtschaft  /  Fallstudien Projekt', '0234', 4, 3),\n" +
"('W', NULL, 6, 'Gramlich', 'Finanzwirtschaft  /  Fallstudien Projekt', '0202', 1, 3),\n" +
"('W', NULL, 6, 'Nespeta', 'Controlling', '0211', 2, 2),\n" +
"('W', NULL, 6, 'Nespeta', 'Controlling', '0211', 2, 3),\n" +
"('W', NULL, 6, 'Beck (W)', 'Personalführung', '0211', 3, 2),\n" +
"('W', NULL, 6, 'Beck (W)', 'Personalführung', '0211', 3, 3),\n" +
"('W', NULL, 4, 'Beck (W)', 'Operations Research', '0131', 1, 4),\n" +
"('W', NULL, 4, 'Beck (W)', 'Operations Research', '0131', 1, 5),\n" +
"('W', NULL, 4, 'Hachtel (W)', 'Betr. Info-Systeme', '0275', 3, 2),\n" +
"('W', NULL, 4, 'Hachtel (W)', 'Betr. Info-Systeme', '0275', 3, 3),\n" +
"('W', NULL, 6, 'Beck (W)', 'Personalführung - Übung', '0202', 1, 2),\n" +
"('W', NULL, 4, 'Beck (W)', 'Unternehmensorganisation', 'AH 0.01', 1, 1),\n" +
"('W', NULL, 4, 'Beck (W)', 'Unternehmensorganisation', 'AH 0.01', 3, 1),\n" +
"('W', NULL, 6, 'Beck (W)', 'Unternehmensorganisation', 'AH 0.01', 1, 1),\n" +
"('W', NULL, 6, 'Beck (W)', 'Unternehmensorganisation', 'AH 0.01', 3, 1),\n" +
"('W', NULL, 4, 'Beck (W)', 'Unternehmensorganisation + Referate', '0103', 2, 1),\n" +
"('W', NULL, 6, 'Beck (W)', 'Unternehmensorganisation + Referate', '0103', 2, 1),\n" +
"('ZSO', NULL, NULL, 'Vazquez', 'Spanisch A1', '0007', 3, 5),\n" +
"('ZSO', NULL, NULL, 'Vazquez', 'Spanisch A1', '0007', 2, 4),\n" +
"('A', NULL, 4, 'Nagl', 'Marketing & Produktmanagement', 'G4 0.02', 5, 1),\n" +
"('AH', NULL, 4, 'Nagl', 'Marketing & Produktmanagement', 'G4 0.02', 5, 1),\n" +
"('A', NULL, 4, 'Montague, Rath Heinrich', 'Fallstudie Beraten & Verkaufen', 'G4 0.02', 5, 2),\n" +
"('A', NULL, 4, 'Montague, Rath Heinrich', 'Fallstudie Beraten & Verkaufen', 'G4 0.02', 5, 3),\n" +
"('AH', NULL, 4, 'Montague, Rath Heinrich', 'Fallstudie Beraten & Verkaufen', 'G4 0.02', 5, 2),\n" +
"('AH', NULL, 4, 'Montague, Rath Heinrich', 'Fallstudie Beraten & Verkaufen', 'G4 0.02', 5, 3),\n" +
"('W', NULL, 3, 'Preikschas, Rabausch', 'Statistik (SPO 31)', '0258', 4, 2),\n" +
"('W', NULL, 3, 'Preikschas, Rabausch', 'Statistik (SPO 31)', '0258', 4, 3),\n" +
"('W', NULL, 6, 'Preikschas, Rabausch', 'Marketing Fallbeispiele', '0211', 2, 4),\n" +
"('W', NULL, 6, 'Preikschas, Rabausch', 'Marketing Fallbeispiele', '0211', 2, 5),\n" +
"('A', NULL, 4, 'Montague, Rath Heinrich', 'Fallstudie Beraten & Verkaufen', 'null, null', 5, 4),\n" +
"('A', NULL, 4, 'Montague, Rath Heinrich', 'Fallstudie Beraten & Verkaufen', 'null, null', 5, 5),\n" +
"('AH', NULL, 4, 'Montague, Rath Heinrich', 'Fallstudie Beraten & Verkaufen', 'null, null', 5, 5),\n" +
"('AH', NULL, 4, 'Montague, Rath Heinrich', 'Fallstudie Beraten & Verkaufen', 'null, null', 5, 4),\n" +
"('B', NULL, 7, 'Bischof', 'Controlling u. Inf.Systeme 2', '0226', 2, 2),\n" +
"('B', NULL, 7, 'Bischof', 'Controlling u. Inf.Systeme 2', '0226', 2, 3),\n" +
"('B', NULL, 7, 'Möhring', 'Controlling und Informationssystem 2', '0226', 2, 2),\n" +
"('B', NULL, 7, 'Möhring', 'Controlling und Informationssystem 2', '0226', 2, 3),\n" +
"('C', NULL, 2, 'Schäfer R.', 'Praktikum Anorganische Chemie', '0147', 2, 4),\n" +
"('C', NULL, 2, 'Schäfer R.', 'Praktikum Anorganische Chemie', '0147', 2, 5),\n" +
"('C', NULL, 2, 'Schäfer R.', 'Praktikum Anorganische Chemie', '0147', 2, 6),\n" +
"('C', NULL, 2, 'Schäfer R.', 'Praktikum Anorganische Chemie', '0147', 3, 3),\n" +
"('C', NULL, 2, 'Schäfer R.', 'Praktikum Anorganische Chemie', '0147', 3, 4),\n" +
"('C', NULL, 2, 'Schäfer R.', 'Praktikum Anorganische Chemie', '0147', 3, 5),\n" +
"('C', NULL, 2, 'Schäfer R.', 'Praktikum Anorganische Chemie', '0147', 3, 6),\n" +
"('C', NULL, 1, 'Hader', 'Physik 1', '0273', 1, 2),\n" +
"('C', NULL, 2, 'Hader', 'Physik 2 mit Labor', '0234', 3, 1),\n" +
"('V', NULL, 1, 'Hader', 'Grundlagen der Physik', '0234', 3, 1),\n" +
"('VMG', NULL, 1, 'Hader', 'Grundlagen der Physik', '0234', 3, 1),\n" +
"('VMM', NULL, 1, 'Hader', 'Grundlagen der Physik', '0234', 3, 1),\n" +
"('C', NULL, 4, 'Beck (C)', 'Praktikum Organische Chemie', NULL, 1, 1),\n" +
"('C', NULL, 4, 'Beck (C)', 'Praktikum Organische Chemie', '0148', 1, 2),\n" +
"('C', NULL, 4, 'Beck (C)', 'Praktikum Organische Chemie', '0148', 1, 3),\n" +
"('C', NULL, 4, 'Beck (C)', 'Praktikum Organische Chemie', '0148', 1, 4),\n" +
"('C', NULL, 4, 'Beck (C)', 'Praktikum Organische Chemie', '0148', 1, 5),\n" +
"('C', NULL, 4, 'Beck (C)', 'Praktikum Organische Chemie', '0148', 1, 6),\n" +
"('C', NULL, 4, 'Schnell', NULL, NULL, 1, 1),\n" +
"('C', NULL, 4, 'Schnell', NULL, '0148', 1, 2),\n" +
"('C', NULL, 4, 'Schnell', NULL, '0148', 1, 3),\n" +
"('C', NULL, 4, 'Schnell', NULL, '0148', 1, 4),\n" +
"('C', NULL, 4, 'Schnell', NULL, '0148', 1, 5),\n" +
"('C', NULL, 4, 'Schnell', NULL, '0148', 1, 6),\n" +
"('C', NULL, 4, 'Junker', NULL, NULL, 1, 1),\n" +
"('C', NULL, 4, 'Junker', NULL, '0148', 1, 2),\n" +
"('C', NULL, 4, 'Junker', NULL, '0148', 1, 3),\n" +
"('C', NULL, 4, 'Junker', NULL, '0148', 1, 4),\n" +
"('C', NULL, 4, 'Junker', NULL, '0148', 1, 5),\n" +
"('C', NULL, 4, 'Junker', NULL, '0148', 1, 6),\n" +
"('C', NULL, 6, 'Flottmann', 'Elementanalytik', '0112', 3, 4),\n" +
"('C', NULL, 6, 'Flottmann', 'Elementanalytik', '0112', 3, 5),\n" +
"('C', NULL, 6, 'Flottmann', 'Elementanalytik', '0112', 3, 6),\n" +
"('C', NULL, 6, 'Stein', 'Mikrobiologie Praktikum', '0060', 3, 4),\n" +
"('C', NULL, 6, 'Stein', 'Mikrobiologie Praktikum', '0060', 3, 5),\n" +
"('C', NULL, 6, 'Stein', 'Mikrobiologie Praktikum', '0060', 3, 6),\n" +
"('C', NULL, 6, 'Schnell', 'Biochemie', '0223', 5, 1),\n" +
"('C', NULL, 6, 'Schnell', 'Mikrobiologie Praktikum', '0060', 4, 4),\n" +
"('C', NULL, 6, 'Schnell', 'Mikrobiologie Praktikum', '0060', 4, 5),\n" +
"('C', NULL, 6, 'Schnell', 'Mikrobiologie Praktikum', '0060', 4, 6),\n" +
"('C', NULL, 4, 'Beck (C), Junker, Neusüß, Schaschke', 'Praktikum Organische Chemie', '0148', 4, 4),\n" +
"('C', NULL, 4, 'Beck (C), Junker, Neusüß, Schaschke', 'Praktikum Organische Chemie', '0148', 4, 5),\n" +
"('C', NULL, 4, 'Beck (C), Junker, Neusüß, Schaschke', 'Praktikum Organische Chemie', '0148', 4, 6),\n" +
"('C', NULL, 6, 'Beck (C), Triebel', 'Praktikum Polymerchemie / Polymeranalytik', '0157', 4, 4),\n" +
"('C', NULL, 6, 'Beck (C), Triebel', 'Praktikum Polymerchemie / Polymeranalytik', '0157', 4, 5),\n" +
"('C', NULL, 6, 'Beck (C), Triebel', 'Praktikum Polymerchemie / Polymeranalytik', '0157', 4, 6),\n" +
"('C', NULL, 2, 'Flottmann, Schäfer R.', 'Praktikum Anorganische Chemie', '0147', 1, 3),\n" +
"('C', NULL, 2, 'Flottmann, Schäfer R.', 'Praktikum Anorganische Chemie', '0147', 1, 4),\n" +
"('C', NULL, 2, 'Flottmann, Schäfer R.', 'Praktikum Anorganische Chemie', '0147', 1, 5),\n" +
"('C', NULL, 2, 'Flottmann, Schäfer R.', 'Praktikum Anorganische Chemie', '0147', 1, 6),\n" +
"('C', NULL, 2, 'Flottmann, Schäfer R.', 'Praktikum Anorganische Chemie', '0147', 2, 3),\n" +
"('C', NULL, 6, 'Flottmann, Neusüß', 'Analytische Chemie F-Praktikum', '0173, 0131', 3, 6),\n" +
"('C', NULL, 6, 'Flottmann, Neusüß', 'Analytische Chemie F-Praktikum', '0173, 0131', 3, 4),\n" +
"('C', NULL, 6, 'Flottmann, Neusüß', 'Analytische Chemie F-Praktikum', '0173, 0131', 3, 5),\n" +
"('ET', NULL, 4, 'Horn', 'Digitale Signalverarbeitung mit Labor', 'G2 0.01', 3, 3),\n" +
"('ET', NULL, 4, 'Horn', 'Digitale Signalverarbeitung mit Labor', 'G2 0.01', 3, 4),\n" +
"('C', NULL, 6, 'Stein', 'Mikrobiologie', '0222', 4, 3),\n" +
"('FR', NULL, 4, 'Bauer M.', '3D-Animation', '0013', 2, 4),\n" +
"('FR', NULL, 4, 'Bauer M.', '3D-Animation', '0013', 2, 5),\n" +
"('MIM', NULL, 1, 'Güida', 'Scientific Methodology', '0225', 2, 2),\n" +
"('MIM', NULL, 1, 'Güida', 'Scientific Methodology', '0225', 2, 3),\n" +
"('MIM', NULL, 1, 'Gentsch', 'Market Research in Marketing & Sales', '0231', 4, 2),\n" +
"('MIM', NULL, 1, 'Gentsch', 'Market Research in Marketing & Sales', '0231', 4, 3),\n" +
"('MIM', NULL, 1, 'Ravens', 'Marketing & Sales I', '0212', 4, 4),\n" +
"('MIM', NULL, 1, 'Ravens', 'Marketing & Sales I', '0212', 4, 5),\n" +
"('MIM', NULL, 1, 'Güida', 'Managerial  Economics', '0225', 5, 2),\n" +
"('MIM', NULL, 1, 'Güida', 'Managerial  Economics', '0225', 5, 3),\n" +
"('MIM', NULL, 1, 'Chung', 'Complex Organizational Structures', '0225', 3, 2),\n" +
"('MIM', NULL, 1, 'Chung', 'Complex Organizational Structures', '0225', 3, 3),\n" +
"('MIM', NULL, 1, 'Chung', 'Persuation & Negotiation', '0120', 2, 4),\n" +
"('MIM', NULL, 1, 'Chung', 'Persuation & Negotiation', '0120', 2, 5),\n" +
"('K', NULL, 7, 'Walcher', 'Werkzeugbau 2', '0222', 4, 1),\n" +
"('K', NULL, 7, 'Walcher', 'Werkzeugbau 2', '0222', 4, 2),\n" +
"('K', NULL, 7, 'Leyrer', 'Polymerverarbeitung 3', '0106', 3, 3),\n" +
"('K', NULL, 7, 'Hachtel Steffen', 'Polymerverarbeitung 3', '0114', 4, 4),\n" +
"('K', NULL, 7, 'Frick', 'Kunststoffe in der Anwendung 2', '0212', 1, 2),\n" +
"('K', NULL, 7, 'Seitz', 'Technisches Englisch', '0122', 3, 1),\n" +
"('O', NULL, 6, 'Bauer (O)', 'Projekte', 'G1 0.20', 5, 4),\n" +
"('P', NULL, 7, 'Prinz', 'Projektmanagement', '0105', 4, 2),\n" +
"('P', NULL, 7, 'Prinz', 'Projektmanagement', '0105', 4, 3),\n" +
"('P', NULL, 6, 'Prinz', 'Projektmanagement', '0105', 4, 2),\n" +
"('P', NULL, 6, 'Prinz', 'Projektmanagement', '0105', 4, 3),\n" +
"('P', NULL, 6, 'Olasz', 'Präsentationstechnik', '0105', 4, 2),\n" +
"('M', 'E, R, K', 6, 'Prinz', 'Projektmanagement', '0105', 4, 2),\n" +
"('M', 'E, R, K', 6, 'Prinz', 'Projektmanagement', '0105', 4, 3),\n" +
"('M', 'E, K, R', 7, 'Prinz', 'Projektmanagement', '0105', 4, 2),\n" +
"('M', 'E, K, R', 7, 'Prinz', 'Projektmanagement', '0105', 4, 3),\n" +
"('M', 'E, R, K', 6, 'Olasz', 'Präsentationstechnik', '0105', 4, 2),\n" +
"('M', 'E, R, K', 6, 'Olasz', 'Präsentationstechnik', '0105', 4, 3),\n" +
"('M', 'E, K, R', 7, 'Olasz', 'Präsentationstechnik', '0105', 4, 2),\n" +
"('M', 'E, K, R', 7, 'Olasz', 'Präsentationstechnik', '0105', 4, 3),\n" +
"('E', 'EEE, TI, IFE, MK', 4, '?', 'Kolloquium', 'G2 1.01', 2, 5),\n" +
"('ET', 'IE, IK', 4, 'Horn', 'Digitale Signalverarbeitung mit Labor', 'G2 0.01', 3, 3),\n" +
"('ET', 'IE, IK', 4, 'Horn', 'Digitale Signalverarbeitung mit Labor', 'G2 0.01', 3, 4),\n" +
"('E', 'IFE, MK, TI', 4, 'Horn', 'Digitale Signalverarbeitung mit Labor', 'G2 0.01', 3, 3),\n" +
"('E', 'IFE, MK, TI', 4, 'Horn', 'Digitale Signalverarbeitung mit Labor', 'G2 0.01', 3, 4),\n" +
"('E', 'IFE, MK, TI', 6, '?', 'Kolloquium', 'G2 1.01', 2, 5),\n" +
"('E', 'IFE, MK, TI', 7, '?', 'Kolloquium', 'G2 1.01', 2, 5),\n" +
"('IN', 'IN, MI, SE, IS', 1, 'Straub', 'Grundlagen der Mathematik', 'G2 0.23', 2, 1),\n" +
"('IN', 'IN, MI, SE, IS', 1, 'Straub', 'Grundlagen der Mathematik', 'G2 0.23', 2, 2),\n" +
"('IN', 'IS, MI, SE', 3, 'Karg', 'Berechenbarkeits.- Komplex.Th.', 'G2 1.44', 2, 3),\n" +
"('IN', 'IS, MI, SE', 3, 'Karg', 'Berechenbarkeits.- Komplex.Th.', 'G2 0.01', 4, 3),\n" +
"('K', NULL, 7, 'Kaiser', 'CAD / Werkzeugkonstruktion', '0101, 0288', 3, 2),\n" +
"('P', NULL, 6, 'Olasz', 'Präsentationstechnik', '0105', 4, 3),\n" +
"('M', NULL, 4, 'Strobel (M)', 'Kostenrechnung', '0111', 2, 5),\n" +
"('VMM', NULL, 6, 'Wegmann', 'Labor Regelungstechnik', '0126', 3, 4),\n" +
"('VMM', NULL, 6, 'Wegmann', 'Labor Regelungstechnik', '0126', 3, 5),\n" +
"('PH', NULL, NULL, 'Krapp', 'Optical Fiber Communication', 'G1 1.01', 1, 2),\n" +
"('PH', NULL, NULL, 'Krapp', 'Optical Fiber Communication', 'G1 0.22', 1, 3),\n" +
"('PH', NULL, NULL, 'Krapp', 'Optical Fiber Communication', 'G1 1.20', 3, 2),\n" +
"('O', NULL, 2, 'Schneider (O)', 'Informatik für Optik und Elektronik', 'G1 0.22', 3, 1),\n" +
"('IDM', NULL, 1, 'Nespeta', 'Produktionsmanagemen', '0267', 4, 2),\n" +
"('IDM', NULL, 1, 'Nespeta', 'Produktionsmanagemen', '0267', 4, 3),\n" +
"('IDM', NULL, 1, 'Holzbaur', 'Mathematische Modellbildung', '0258', 2, 7),\n" +
"('IDM', NULL, 1, 'Holzbaur', 'Mathematische Modellbildung', '0103', 3, 7),\n" +
"('IDM', NULL, 1, 'Preikschas', 'Investitionsgütermarketing', '0111', 3, 4),\n" +
"('IDM', NULL, 1, 'Preikschas', 'Investitionsgütermarketing', '0111', 3, 5),\n" +
"('M', NULL, 2, 'Baumann', 'Werkstoffkunde 2', '0133', 4, 4),\n" +
"('M', NULL, 2, 'Baumann', 'Werkstoffkunde 2', '0133', 4, 5),\n" +
"('P', NULL, 2, 'Baumann', 'Werkstoffkunde 2', '0133', 4, 4),\n" +
"('P', NULL, 2, 'Baumann', 'Werkstoffkunde 2', '0133', 4, 5),\n" +
"('V', NULL, 3, 'Baumann', 'Strukturwerkstoffe', '0133', 4, 4),\n" +
"('V', NULL, 3, 'Baumann', 'Strukturwerkstoffe', '0133', 4, 5),\n" +
"('VMM', NULL, 3, 'Baumann', 'Strukturwerkstoffe', '0133', 4, 4),\n" +
"('VMM', NULL, 3, 'Baumann', 'Strukturwerkstoffe', '0133', 4, 5),\n" +
"('VMG', NULL, 3, 'Baumann', 'Strukturwerkstoffe', '0133', 4, 4),\n" +
"('VMG', NULL, 3, 'Baumann', 'Strukturwerkstoffe', '0133', 4, 5),\n" +
"('IDM', NULL, 1, 'Beck (W)', 'Leadership / Nachhalt.Unt.Führ.', '0106', 2, 2),\n" +
"('IDM', NULL, 1, 'Beck (W)', 'Leadership / Nachhalt.Unt.Führ.', '0106', 2, 3),\n" +
"('M', 'E', 6, 'Wagner', 'Regelungstechnik', '0120', 1, 4),\n" +
"('M', 'E', 6, 'Wagner', 'Regelungstechnik', '0105', 2, 1),\n" +
"('IDM', NULL, 1, 'Knecktys', 'Supply Chain Management', '0121', 5, 2),\n" +
"('IDM', NULL, 1, 'Knecktys', 'Supply Chain Management', '0258', 5, 3),\n" +
"('IST', NULL, NULL, 'Dekker', 'Strategic Sales Management', '0111', 5, 1),\n" +
"('IST', NULL, NULL, 'Dekker', 'Strategic Sales Management', '0111', 5, 2),\n" +
"('IST', NULL, NULL, 'Dekker', 'Strategic Sales Management', '0111', 5, 3),\n" +
"('IST', NULL, NULL, 'Dekker', 'Strategic Sales Management', '0105', 5, 4),\n" +
"('IST', NULL, NULL, 'Dekker', 'Strategic Sales Management', '0105', 5, 5),\n" +
"('IST', NULL, NULL, 'Dekker', 'Strategic Sales Management', '0105', 5, 6),\n" +
"('IST', NULL, NULL, 'Dekker', 'Strategic Sales Management', '0101', 6, 1),\n" +
"('IST', NULL, NULL, 'Dekker', 'Strategic Sales Management', '0101', 6, 2),\n" +
"('IST', NULL, NULL, 'Dekker', 'Strategic Sales Management', '0101', 6, 3),\n" +
"('IDM', NULL, 1, 'Schrader', 'Innovationsmanagement', '0111', 3, 6),\n" +
"('IDM', NULL, 1, 'Schrader', 'Innovationsmanagement', '0105', 4, 1),\n" +
"('ABC', NULL, 2, 'Diverse', 'Projektarbeit', NULL, 1, 1),\n" +
"('ABC', NULL, 2, 'Diverse', 'Projektarbeit', NULL, 1, 2),\n" +
"('ABC', NULL, 2, 'Diverse', 'Projektarbeit', NULL, 1, 3),\n" +
"('ABC', NULL, 2, 'Diverse', 'Projektarbeit', NULL, 1, 4),\n" +
"('ABC', NULL, 2, 'Diverse', 'Projektarbeit', NULL, 1, 5),\n" +
"('ABC', NULL, 2, 'Diverse', 'Projektarbeit', NULL, 1, 6),\n" +
"('ABC', NULL, 2, 'Diverse', 'Projektarbeit', NULL, 2, 5),\n" +
"('ABC', NULL, 2, 'Diverse', 'Projektarbeit', NULL, 2, 6),\n" +
"('ABC', NULL, 2, 'Diverse', 'Projektarbeit', NULL, 4, 4),\n" +
"('ABC', NULL, 2, 'Diverse', 'Projektarbeit', NULL, 4, 5),\n" +
"('ABC', NULL, 2, 'Diverse', 'Projektarbeit', NULL, 4, 6),\n" +
"('ABC', NULL, 2, 'Diverse', 'Projektarbeit', NULL, 5, 6),\n" +
"('ABC', NULL, 2, 'Wagner (C)', 'Physikalische Chemie 1', '0103', 4, 3),\n" +
"('ABC', NULL, 2, 'Neusüß', 'Brückenkurs Analytische Chemie / Biochemie', '0215', 4, 1),\n" +
"('ABC', NULL, 2, 'Schnell', 'Biochemische Reaktionsmechanismen', '0212', 3, 4),\n" +
"('ABC', NULL, 2, 'Holzbaur', 'Projektmanagement', '0120', 5, 3);" 
,"INSERT INTO g02_vorlesungsplan (stg, schwerpunkt, semester, dozent, kurs, raum, tagnr,  zeitblocknr) VALUES\n" +
            "('P', NULL, 1, 'Pietzsch', 'Freihandzeichnen 1', '0111', 4, 5),\n" +
"('P', NULL, 1, 'Pietzsch', 'Freihandzeichnen 1', '0111', 4, 6),\n" +
"('VI', NULL, 4, 'Zehnder', 'Konstruktion', '0121', 1, 4),\n" +
"('VI', NULL, 4, 'Zehnder', 'Konstruktion', '0121', 1, 5),\n" +
"('VMM', NULL, 3, 'Zehnder', 'Konstruktionsmethodik / Anwendungen Konstruktion', '0121', 1, 4),\n" +
"('VMM', NULL, 3, 'Zehnder', 'Konstruktionsmethodik / Anwendungen Konstruktion', '0121', 1, 5),\n" +
"('VMM', NULL, 3, 'Maier C.(V)', 'Grundlagen der Mechatronik', '0231', 5, 2),\n" +
"('VMM', NULL, 3, 'Maier C.(V)', 'Grundlagen der Mechatronik', '0231', 5, 3),\n" +
"('VI', NULL, 4, 'Maier C.(V)', 'Grundlagen der Mechatronik', '0231', 5, 2),\n" +
"('VI', NULL, 4, 'Maier C.(V)', 'Grundlagen der Mechatronik', '0231', 5, 3),\n" +
"('VMM', NULL, 3, 'Maier C.(V)', 'Automatisierungstechnik', '0231', 5, 2),\n" +
"('VMM', NULL, 3, 'Maier C.(V)', 'Automatisierungstechnik', '0231', 5, 3),\n" +
"('M', 'E, R, K', 6, 'Jourdan', 'Konstruktionslehre 2 - Übungen', '0106', 3, 4),\n" +
"('M', 'E, R, K', 6, 'Jourdan', 'Konstruktionslehre 2 - Übungen', '0106', 3, 5),\n" +
"('M', NULL, 4, '?, Zorniger, Wagner', 'Labor Regelungstechnik', '0126, 0187', 3, 4),\n" +
"('M', NULL, 2, 'Faber, Schäffer', 'Physiklabor', 'GS 1.15', 3, 1),\n" +
"('M', NULL, 2, 'Faber, Schäffer', 'Physiklabor', 'GS 1.15', 3, 2),\n" +
"('M', NULL, 4, '?, Zorniger, Wagner', 'Labor Regelungstechnik', '0126, 0187', 3, 5),\n" +
"('VI', NULL, 4, 'Maier C.(V)', 'Automatisierungstechnik', '0231', 5, 2),\n" +
"('VI', NULL, 4, 'Maier C.(V)', 'Automatisierungstechnik', '0231', 5, 3),\n" +
"('VI', NULL, 4, 'Sulic', 'Spanisch A2', '0223', 2, 1),\n" +
"('VI', NULL, 4, 'Sulic', 'Spanisch A2', '0223', 2, 2),\n" +
"('M', 'R', 7, 'Rittmann', 'Regenerative Energien', '0212', 1, 4),\n" +
"('P', NULL, 1, 'Mathy', 'Werkstoffkunde 1', '0203', 1, 3),\n" +
"('P', NULL, 1, 'Mathy', 'Werkstoffkunde 1', '0701', 2, 2),\n" +
"('M', NULL, 3, 'Merkel', 'CAD / CAM', 'AH 1.01', 4, 1),\n" +
"('V', NULL, 1, 'Möckel', 'Allgemeine Chemie', '0213', 2, 1),\n" +
"('V', NULL, 1, 'Möckel', 'Allgemeine Chemie', '0222', 2, 3),\n" +
"('VMG', NULL, 1, 'Möckel', 'Allgemeine Chemie', '0213', 2, 1),\n" +
"('VMG', NULL, 1, 'Möckel', 'Allgemeine Chemie', '0222', 2, 3),\n" +
"('VMM', NULL, 1, 'Möckel', 'Allgemeine Chemie', '0213', 2, 1),\n" +
"('VMM', NULL, 1, 'Möckel', 'Allgemeine Chemie', '0222', 2, 3),\n" +
"('ABC', NULL, 2, 'Stein', 'Brückenkurs Biologische Chemie', '0231', 2, 3),\n" +
"('ABC', NULL, 2, 'Neusüß', 'Proteinanalytik', '0122', 2, 4),\n" +
"('M', 'R', 6, 'Kropp', 'Nachhaltige Entwicklung', '0120', 2, 2),\n" +
"('FR', NULL, 1, 'Richter C.', 'Technische Dokumentation 1', 'G1 0.06', 1, 2),\n" +
"('FR', NULL, 1, 'Richter C.', 'Technische Dokumentation 1', 'G1 1.01', 2, 2),\n" +
"('FR', NULL, 1, 'Richter C.', 'Technische Dokumentation 1', 'G1 1.32', 5, 3),\n" +
"('C', NULL, 6, 'Stein', 'Mikrobiologie Praktikum', '0060', 2, 4),\n" +
"('C', NULL, 6, 'Stein', 'Mikrobiologie Praktikum', '0060', 2, 5),\n" +
"('C', NULL, 6, 'Stein', 'Mikrobiologie Praktikum', '0060', 2, 6),\n" +
"('C', NULL, 6, 'Flottmann', 'Praktikum Elementanalytik', '0173', 2, 4),\n" +
"('C', NULL, 6, 'Flottmann', 'Praktikum Elementanalytik', '0173', 2, 5),\n" +
"('C', NULL, 6, 'Flottmann', 'Praktikum Elementanalytik', '0173', 2, 6),\n" +
"('C', NULL, 6, 'Flottmann', 'Elementanalytik', '0132', 2, 4),\n" +
"('C', NULL, 6, 'Flottmann', 'Elementanalytik', '0132', 2, 5),\n" +
"('C', NULL, 6, 'Flottmann', 'Elementanalytik', '0258', 2, 6),\n" +
"('C', NULL, 3, 'Flottmann', 'Praktikum Analytische Chemie', '0267', 2, 4),\n" +
"('C', NULL, 3, 'Flottmann', 'Praktikum Analytische Chemie', '0267', 2, 5),\n" +
"('C', NULL, 3, 'Flottmann', 'Praktikum Analytische Chemie', '0267', 2, 6),\n" +
"('C', NULL, 4, 'Junker', 'Praktikum Organische Chemie', '0148', 2, 1),\n" +
"('C', NULL, 4, 'Junker', 'Praktikum Organische Chemie', '0148', 2, 2),\n" +
"('C', NULL, 4, 'Junker', 'Praktikum Organische Chemie', '0148', 2, 3),\n" +
"('C', NULL, 4, 'Junker', 'Praktikum Organische Chemie', '0148', 2, 4),\n" +
"('C', NULL, 4, 'Junker', 'Praktikum Organische Chemie', '0148', 2, 5),\n" +
"('C', NULL, 4, 'Junker', 'Praktikum Organische Chemie', '0148', 2, 6),\n" +
"('C', NULL, 4, 'Schnell', 'Biochemie  /  Biotechnologie', '0125', 3, 5),\n" +
"('C', NULL, 4, 'Schnell', 'Biochemie  /  Biotechnologie', '0129', 5, 3),\n" +
"('ET', NULL, 2, 'Tutoren', 'Elektrotechnik 2 Tutorium', 'G2 1.01', 2, 4),\n" +
"('ZCO', NULL, NULL, 'Liu Xiaoning', 'Chines. Sprache & Kultur 1.1', '0103', 1, 6),\n" +
"('ZCO', NULL, NULL, 'Liu Xiaoning', 'Chines. Sprache & Kultur 1.1', '0106', 3, 6),\n" +
"('GF', NULL, 1, 'Schmidt Ho', 'Mathematik 1', 'G1 0.20', 1, 3),\n" +
"('GF', NULL, 1, 'Schmidt Ho', 'Mathematik 1', 'G1 0.20', 3, 3),\n" +
"('GF', NULL, 1, 'Schmidt Ho', 'Mathematik 1', 'G1 0.20', 4, 2),\n" +
"('MekA', NULL, 1, 'Schmidt Ho', 'Mathematik 1', 'G1 0.20', 1, 3),\n" +
"('MekA', NULL, 1, 'Schmidt Ho', 'Mathematik 1', 'G1 0.20', 3, 3),\n" +
"('MekA', NULL, 1, 'Schmidt Ho', 'Mathematik 1', 'G1 0.20', 4, 2),\n" +
"('F', NULL, 1, 'Schmidt Ho', 'Mathematik 1', 'G1 0.20', 1, 3),\n" +
"('F', NULL, 1, 'Schmidt Ho', 'Mathematik 1', 'G1 0.20', 3, 3),\n" +
"('F', NULL, 1, 'Schmidt Ho', 'Mathematik 1', 'G1 0.20', 4, 2),\n" +
"('FR', NULL, 1, 'Schmidt Ho', 'Mathematik 1', 'G1 0.20', 1, 3),\n" +
"('FR', NULL, 1, 'Schmidt Ho', 'Mathematik 1', 'G1 0.20', 3, 3),\n" +
"('FR', NULL, 1, 'Schmidt Ho', 'Mathematik 1', 'G1 0.20', 4, 2),\n" +
"('PH', NULL, NULL, 'Hellmuth', 'Quantum Optics', 'G1 0.20', 2, 3),\n" +
"('PH', NULL, NULL, 'Hellmuth', 'Quantum Optics', 'G1 0.01', 3, 3),\n" +
"('O', NULL, 3, 'Kettler', 'Digitale Elektronik', 'G1 1.01', 4, 1),\n" +
"('O', NULL, 1, 'Dittmar', 'Übung Elektrotechnik', 'G1 0.21', 4, 4),\n" +
"('O', NULL, 1, 'Fritz Dietmar', 'Arbeitstechniken', 'G1 2.36', 5, 1),\n" +
"('O', NULL, 1, 'Dittmar', 'Elektronik Grundlagen', 'G1 1.20', 3, 1),\n" +
"('IN', 'IN, MI, SE, IS', 1, 'Walter', 'Analysis', 'G2 0.23', 1, 1),\n" +
"('IN', 'IN, MI, SE, IS', 1, 'Walter', 'Analysis', 'G2 0.23', 5, 1),\n" +
"('IN', 'IS, MI, SE', 2, 'Werthebach', 'Betriebssysteme', 'G2 0.01', 2, 1),\n" +
"('IN', 'IS, MI, SE', 2, 'Werthebach', 'Betriebssysteme', 'G2 2.01', 4, 3),\n" +
"('IN', 'IS, MI, SE, WI', 4, 'Oberhauser', 'Software-Projectmanagement', 'G2 1.30', 2, 2),\n" +
"('C', NULL, 4, 'Beck (C), Schaschke', 'Praktikum Organische Chemie', '0148', 4, 1),\n" +
"('C', NULL, 4, 'Beck (C), Schaschke', 'Praktikum Organische Chemie', '0148', 4, 2),\n" +
"('C', NULL, 4, 'Beck (C), Schaschke', 'Praktikum Organische Chemie', '0148', 4, 3),\n" +
"('O', NULL, 1, 'Dittmar', 'Elektrotechnik Grundlagen', 'G1 1.23', 1, 2),\n" +
"('O', NULL, 1, 'Dittmar', 'Elektrotechnik Grundlagen', 'G1 1.23', 1, 3),\n" +
"('PH', NULL, NULL, 'Hellmuth', 'Fundamental Optics', 'G1 0.20', 1, 4),\n" +
"('FOI', NULL, NULL, 'Hellmuth', 'Fundamental Optics', 'G1 0.20', 1, 4),\n" +
"('PH', NULL, NULL, 'Schneider (O)', 'Advanced Image Processing', 'G1 0.06', 2, 2),\n" +
"('PH', NULL, NULL, 'Schneider (O)', 'Advanced Image Processing', 'G1 0.06', 4, 1),\n" +
"('A', NULL, 3, 'Holschbach', 'Kontaktlinse 1', 'G4 -1.01', 5, 1),\n" +
"('AH', NULL, 2, 'Nolting', 'Technische Akustik', 'G4 0.02', 4, 4),\n" +
"('O', NULL, 6, 'Schneckenburger', 'Biomedizinische Optik', 'G1 1.23', 2, 3),\n" +
"('O', NULL, 6, 'Schneckenburger', 'Biomedizinische Optik', 'G1 0.22', 4, 4),\n" +
"('AH', NULL, 2, 'Lanzinger', 'Praktikum Technische Akustik', 'G4 -1.05', 1, 5),\n" +
"('F', NULL, 4, 'Berger (F)', 'CAM', 'G1 1.28', 2, 2),\n" +
"('FR', NULL, 7, 'Berger (F)', 'CAM', 'G1 1.28', 2, 2),\n" +
"('AH', NULL, 4, 'Limberger', 'Hörsystemanpassung 1', 'G4 0.02', 4, 2),\n" +
"('AH', NULL, 4, 'Limberger', 'Hörsystemanpassung 1', 'G4 0.02', 3, 1),\n" +
"('AH', NULL, 4, 'Limberger', 'Hörsystemanpassung 1', 'G4 0.02', 3, 2),\n" +
"('AH', NULL, 6, 'Limberger', 'W-Pädaudiologie', NULL, 3, 3),\n" +
"('PEF', NULL, NULL, 'Hader', 'Mathematische Modelle und Verfahren', '0122', 1, 3),\n" +
"('F', NULL, 3, 'Berger (F)', 'SPS-Programmierung', 'G1 1.28', 2, 4),\n" +
"('GE', NULL, 4, 'Berger (F)', 'SPS-Programmierung', 'G1 1.28', 2, 4),\n" +
"('FR', NULL, 6, 'Berger (F)', 'SPS-Programmierung', 'G1 1.28', 2, 4),\n" +
"('O', NULL, 3, 'Schneckenburger', 'Optik 1', 'G1 1.32', 2, 1),\n" +
"('O', NULL, 3, 'Schneckenburger', 'Optik 1', 'G1 1.01', 3, 3),\n" +
"('F', NULL, 6, 'Schneckenburger', 'Optik 1', 'G1 1.32', 2, 1),\n" +
"('F', NULL, 6, 'Schneckenburger', 'Optik 1', 'G1 1.01', 3, 3),\n" +
"('F', NULL, 7, 'Schneckenburger', 'Optik 1', 'G1 1.32', 2, 1),\n" +
"('F', NULL, 7, 'Schneckenburger', 'Optik 1', 'G1 1.01', 3, 3),\n" +
"('MekA', NULL, 5, 'Schneckenburger', 'Optik 1', 'G1 1.32', 2, 1),\n" +
"('MekA', NULL, 5, 'Schneckenburger', 'Optik 1', 'G1 1.01', 3, 3),\n" +
"('PEF', NULL, NULL, 'Hader', 'Mathematische Modelle und Verfahren und Übungen', '0034', 5, 2),\n" +
"('MM', NULL, 1, 'Cappel', 'Führungskompetenz', '0130', 4, 2),\n" +
"('MM', NULL, 1, 'Cappel', 'Führungskompetenz', '0130', 4, 3),\n" +
"('MM', NULL, 1, 'Cappel', 'Führungskompetenz', '0202', 4, 4),\n" +
"('MM', NULL, 1, 'Cappel', 'Führungskompetenz', '0203', 4, 5),\n" +
"('MM', NULL, 1, 'Wengert', 'Wissenschaftliche Methodik', '0132', 5, 2),\n" +
"('MM', NULL, 1, 'Wengert', 'Wissenschaftliche Methodik', '0114', 5, 3),\n" +
"('MM', NULL, 1, 'Niethammer', 'Existenzgründung', '0125', 2, 2),\n" +
"('MM', NULL, 1, 'Niethammer', 'Existenzgründung', '0125', 2, 3),\n" +
"('MM', NULL, 1, 'Niethammer', 'Innovations-Technologie-Wissensmanag.', '0125', 2, 2),\n" +
"('MM', NULL, 1, 'Niethammer', 'Innovations-Technologie-Wissensmanag.', '0125', 2, 3),\n" +
"('MM', NULL, 1, 'Niethammer', 'Unternehmenskooperation  /  Netzwerke', '0125', 2, 2),\n" +
"('MM', NULL, 1, 'Niethammer', 'Unternehmenskooperation  /  Netzwerke', '0125', 2, 3),\n" +
"('MM', NULL, 1, 'Renz', 'KMU Management & Praxistransfer', '0132', 3, 3),\n" +
"('MM', NULL, 1, 'Niethammer', 'Innovations-Tund Kooperationsmanagement', '0125', 1, 2),\n" +
"('MM', NULL, 1, 'Niethammer', 'Innovations-Tund Kooperationsmanagement', '0125', 1, 3),\n" +
"('MM', NULL, 1, 'Radtke', 'Relationship Marketing / Fallstudien', '0226', 2, 4),\n" +
"('MM', NULL, 1, 'Radtke', 'Relationship Marketing / Fallstudien', '0226', 2, 5),\n" +
"('MM', NULL, 1, 'Morlock', 'Supply Chain Management', '0125', 1, 4),\n" +
"('MM', NULL, 1, 'Morlock', 'Supply Chain Management', '0125', 1, 5),\n" +
"('MM', NULL, 1, 'Mietzner M.', 'Alternative Finanzierungsformen', '0122', 3, 5),\n" +
"('MM', NULL, 1, 'Mietzner M.', 'Alternative Finanzierungsformen', '0122', 3, 6),\n" +
"('O', NULL, 3, 'Fiedler', 'Qualitätsmanagement', 'G1 0.01', 1, 1),\n" +
"('O', NULL, 3, 'Fiedler', 'Qualitätsmanagement', 'G1 0.01', 1, 2),\n" +
"('O', NULL, 1, 'Höfig', 'Technisches Zeichnen', 'G1 1.01', 4, 2),\n" +
"('C', NULL, 6, 'Flottmann', 'Elementanalytik', '0114', 1, 1),\n" +
"('C', NULL, 6, 'Beck (C)', 'Spektroskopie', '0203', 4, 1),\n" +
"('C', NULL, 6, 'Beck (C)', 'Spektroskopie', '0203', 4, 2),\n" +
"('C', NULL, 6, 'Schulz (CV)', 'Qualitätsmanagement', '0114', 1, 2),\n" +
"('C', NULL, 3, 'Flottmann', 'Praktikum Analytische Chemie', '0157', 1, 4),\n" +
"('C', NULL, 3, 'Flottmann', 'Praktikum Analytische Chemie', '0157', 1, 5),\n" +
"('C', NULL, 3, 'Wagner (C)', 'Prakt. Phys. Chemie 2', '0153', 1, 4),\n" +
"('C', NULL, 3, 'Wagner (C)', 'Prakt. Phys. Chemie 2', '0153', 1, 5),\n" +
"('C', NULL, 3, 'Flottmann', 'Praktikum Analytische Chemie', '0157', 1, 6),\n" +
"('C', NULL, 6, 'Beck (C)', 'Organische Analytik', '0132', 5, 3),\n" +
"('C', NULL, 6, 'Beck (C)', 'Organische Analytik', '0132', 5, 4),\n" +
"('AH', NULL, 2, 'Hoffmann, Kreikemeier', 'Physiologische Akustik', 'G4 0.02', 3, 1),\n" +
"('C', NULL, 6, 'Neusüß', 'Analytische Chemie 4', '0114', 2, 3),\n" +
"('C', NULL, 6, 'Beck (C)', 'Spektroskopie', '0215', 2, 1),\n" +
"('C', NULL, 1, 'Flottmann', 'Laborkunde', '0231', 5, 1),\n" +
"('C', NULL, 1, 'Flottmann', 'Laborkunde', '0014', 5, 2),\n" +
"('C', NULL, 3, 'Seitz', 'Englisch 2', '0103', 4, 2),\n" +
"('ABC', NULL, 2, 'Stein', 'Proteinanalytik', '0203', 3, 1),\n" +
"('O', NULL, 1, 'Schmidt H.', 'Elektr. Messtechnik Labor', 'G1 0.34', 3, 4),\n" +
"('O', NULL, 1, 'Schmidt H.', 'Elektr. Messtechnik Labor', 'G1 0.34', 3, 5),\n" +
"('O', NULL, 1, 'Hahn', NULL, 'G1 0.34', 3, 4),\n" +
"('O', NULL, 1, 'Hahn', NULL, 'G1 0.34', 3, 5),\n" +
"('ABC', NULL, 2, 'Stein', 'Proteinanalytik', '0120', 2, 1),\n" +
"('ABC', NULL, 2, 'Holzbaur', 'Qualitätsmanagement', '0114', 3, 2),\n" +
"('ABC', NULL, 2, 'Schäfer R.', 'Anorganische Chemie', '0102', 4, 2),\n" +
"('ET', 'IE_W', 6, 'Hermann', 'Software Engineering', 'G2 2.33', 2, 3),\n" +
"('ET', 'IE_W', 6, 'Hermann', 'Software Engineering', 'G2 2.33', 2, 4),\n" +
"('CCS', NULL, 2, 'Oberhauser', 'Software-Qualität', 'G2 1.44', 3, 3),\n" +
"('CCS', NULL, 2, 'Bantel', 'Programmieren eingebetteter Systeme', 'G2 0.24', 1, 1),\n" +
"('CCS', NULL, 2, 'Bantel', 'Programmieren eingebetteter Systeme', 'G2 0.24', 1, 2),\n" +
"('CCS', NULL, 2, 'Müller', 'Kommunikation in verteilten Systemen', 'G2 2.34', 4, 4),\n" +
"('CCS', NULL, 2, 'Müller', 'Kommunikation in verteilten Systemen', 'G2 2.34', 4, 5),\n" +
"('CCS', NULL, 2, 'Klauck', 'Bildverarbeitung', 'G2 1.34', 4, 1),\n" +
"('CCS', NULL, 2, 'Klauck', 'Bildverarbeitung', 'G2 1.34', 4, 2),\n" +
"('CCS', NULL, 2, 'Oberhauser', 'Systems Engineering', 'G2 1.03~G2 1.30~G2 0.30', 5, 2),\n" +
"('CCS', NULL, 2, 'Oberhauser', 'Systems Engineering', 'G2 1.03~G2 1.30~G2 0.30', 5, 3),\n" +
"('CCS', NULL, 2, 'Dietrich', 'Echtzeitsysteme', 'G2 0.01', 3, 2),\n" +
"('ET', NULL, 3, 'Kolb', 'Regelungstechnik 1 - Labor', 'G2 2.26', 1, 4),\n" +
"('ET', NULL, 3, 'Kolb', 'Regelungstechnik 1 - Labor', 'G2 2.26', 1, 5),\n" +
"('ET', 'IE_W', 6, 'Kolb', 'Regelungstechnik 1 - Labor', 'G2 2.26', 1, 4),\n" +
"('ET', 'IE_W', 6, 'Kolb', 'Regelungstechnik 1 - Labor', 'G2 2.26', 1, 5),\n" +
"('K', NULL, 1, 'Möbius', 'Mathematik 1', 'AH 1.02', 3, 4),\n" +
"('K', NULL, 1, 'Möbius', 'Mathematik 1', 'AH 1.02', 3, 5),\n" +
"('K', NULL, 1, 'Walcher', 'Physik 1', '0121', 1, 3),\n" +
"('K', NULL, 1, 'Walcher', 'Physik 1', '0273', 2, 4),\n" +
"('K', NULL, 1, 'Mathy', 'Werkstoffkunde Metalle', '0112', 1, 4),\n" +
"('K', NULL, 1, 'Mathy', 'Werkstoffkunde Metalle', '0201', 3, 3),\n" +
"('K', NULL, 1, 'Dambacher', 'Technisches Zeichnen', '0288', 2, 1),\n" +
"('K', NULL, 1, 'Dambacher', 'Technisches Zeichnen', '0288', 2, 2),\n" +
"('K', NULL, 1, 'Dambacher', 'Technisches Zeichnen', '0288', 2, 3),\n" +
"('K', NULL, 1, '?', 'Einführung CAD', '0112', 1, 5),\n" +
"('K', NULL, 1, '?', 'Einführung CAD', '0112', 1, 6),\n" +
"('K', NULL, 1, 'Ruf', 'Technische Mechanik', '0121', 1, 2),\n" +
"('K', NULL, 1, 'Ruf', 'Technische Mechanik', '0111', 3, 2),\n" +
"('O', NULL, 1, 'Fritz Dietmar', 'Rechner- und Netzwerktechnik', 'G1 1.23', 4, 1),\n" +
"('E', 'EEE, MK, TI, IFE', 4, 'Kolb', 'Regelungstechnik 1 - Labor', 'G2 2.26', 1, 4),\n" +
"('E', 'EEE, MK, TI, IFE', 4, 'Kolb', 'Regelungstechnik 1 - Labor', 'G2 2.26', 1, 5),\n" +
"('ET', 'IE, IK', 4, 'Hermann', 'Software Engineering', 'G2 2.33', 2, 3),\n" +
"('ET', 'IE, IK', 4, 'Hermann', 'Software Engineering', 'G2 2.33', 2, 4),\n" +
"('E', 'IFE, MK, TI', 6, 'Hermann', 'Software Engineering', 'G2 2.33', 2, 3),\n" +
"('E', 'IFE, MK, TI', 6, 'Hermann', 'Software Engineering', 'G2 2.33', 2, 4),\n" +
"('E', 'IFE, MK, TI', 7, 'Hermann', 'Software Engineering', 'G2 2.33', 2, 3),\n" +
"('E', 'IFE, MK, TI', 7, 'Hermann', 'Software Engineering', 'G2 2.33', 2, 4),\n" +
"('IN', 'IS, MI, SE', 3, 'Dietrich', 'Objektorientierte Modellierung', 'G2 0.37', 2, 2),\n" +
"('IN', 'IS, MI, SE', 3, 'Dietrich', 'Objektorientierte Modellierung', 'G2 0.37', 3, 3),\n" +
"('IN', 'IS, MI, SE', 3, 'Dietrich, Küpper', 'Praktikum Anal. / Entw. Inf.Sys.', 'G2 1.30~G2 1.28', 4, 2),\n" +
"('IN', 'IS, MI, SE', 3, 'Dietrich, Küpper', 'Praktikum Anal. / Entw. Inf.Sys.', 'G2 1.30~G2 1.28', 4, 1),\n" +
"('ET', NULL, 2, 'Faber, Werner', 'Physik 2 mit Labor', 'GS 1.15', 1, 1),\n" +
"('ET', NULL, 2, 'Faber, Werner', 'Physik 2 mit Labor', 'GS 1.15', 1, 2),\n" +
"('IN', 'IS', 3, 'Hellmann', 'Sichere Hardware', 'G2 1.37', 5, 3),\n" +
"('IN', 'IS', 3, 'Hellmann', 'Sichere Hardware', 'G2 1.37', 5, 4),\n" +
"('IN', 'MI', 4, 'Küpper', 'Internetbasierte Systeme', 'G2 2.01', 1, 5),\n" +
"('IN', 'MI', 4, 'Küpper', 'Internetbasierte Systeme', 'G2 2.41', 3, 1),\n" +
"('C', NULL, 4, 'Schnell', 'Biochemie  /  Biotechnologie', '0114', 5, 4),\n" +
"('P', NULL, 2, 'Schmid E.', 'Maschinenelemente 1', '0258', 5, 4),\n" +
"('P', NULL, 2, 'Schmid E.', 'Maschinenelemente 1', '0258', 5, 5),\n" +
"('K', NULL, 3, 'Kaiser', 'Informatik', '0288', 1, 3),\n" +
"('K', NULL, 3, 'Kaiser', 'Informatik', '0288', 3, 3),\n" +
"('K', NULL, 1, 'Beck (C)', 'Polymerchemie', '0112', 3, 1),\n" +
"('K', NULL, 3, 'Leyrer', 'Polymerverarbeitung 1', '0111', 2, 3),\n" +
"('PTC', NULL, 1, 'Leyrer', 'Injection Moulding', '0273', 3, 2),\n" +
"('LBM', NULL, NULL, 'Leyrer', 'Injection Moulding', '0273', 3, 2),\n" +
"('PEF', NULL, NULL, 'Leyrer', 'Einsatz innovativer Werkstoffe: Polymere: Polymere', '0223', 3, 1),\n" +
"('M', 'K', 6, 'Weber (P)', 'Fahrerassistenzsysteme', '0105', 1, 4),\n" +
"('P', NULL, 7, 'Weber (P)', 'Fahrerassistenzsysteme', '0105', 1, 4),\n" +
"('P', NULL, 1, 'Seitz', 'Technisches Englisch', '0102', 4, 3),\n" +
"('P', NULL, 2, 'Seitz', 'Technisches Englisch', '0102', 4, 3),\n" +
"('C', NULL, 6, 'Neusüß', 'Analytische Chemie 4', '0114', 1, 3),\n" +
"('C', NULL, 6, 'Neusüß', 'Analytische Chemie 4', '0215', 3, 1),\n" +
"('C', NULL, 6, 'Stein', 'Mikrobiologie Praktikum', '0060', 1, 4),\n" +
"('C', NULL, 6, 'Stein', 'Mikrobiologie Praktikum', '0060', 1, 5),\n" +
"('C', NULL, 6, 'Stein', 'Mikrobiologie Praktikum', '0060', 1, 6),\n" +
"('C', NULL, 3, 'Stein', 'Physikalische Chemie 3', '0211', 4, 1),\n" +
"('C', NULL, 3, 'Junker', 'Organische Chemie 1', '0112', 1, 3),\n" +
"('C', NULL, 3, 'Junker', 'Organische Chemie 1', '0215', 3, 5),\n" +
"('M', 'E, K, R', 7, 'Rittmann', 'Kraft- und Arbeitsmaschinen', '0115', 3, 1),\n" +
"('M', 'E, K, R', 7, 'Rittmann', 'Kraft- und Arbeitsmaschinen', '0115', 3, 2),\n" +
"('C', NULL, 1, 'Junker, Triebel', 'Praktikum Labortechnik', '0147', 3, 1),\n" +
"('C', NULL, 1, 'Junker, Triebel', 'Praktikum Labortechnik', '0147', 3, 2),\n" +
"('P', NULL, 3, 'Bittner', 'Numerische Mathematik', '0034, 0131', 5, 4),\n" +
"('P', NULL, 3, 'Bittner', 'Numerische Mathematik', '0034, 0131', 5, 5),\n" +
"('PTC', NULL, 1, 'Kaiser', 'Mould Design', '0130, 0288', 3, 1),\n" +
"('K', NULL, 3, 'Kaiser', 'Rheologie', '0133, 0288', 1, 2),\n" +
"('K', NULL, 3, 'Kaiser', 'Informatik', '0273, 0288', 5, 3),\n" +
"('IN', 'MI', 3, 'Küpper', 'Internetbasierte Systeme', 'G2 1.30, G2 2.01', 1, 5),\n" +
"('IN', 'MI', 3, 'Küpper', 'Internetbasierte Systeme', 'G2 1.30, G2 2.41', 3, 1),\n" +
"('C', NULL, 6, 'Flottmann, Neusüß', 'Seminar Analytische Chemie', '0215', 1, 4),\n" +
"('C', NULL, 6, 'Flottmann, Neusüß', 'Seminar Analytische Chemie', '0215', 1, 5),\n" +
"('C', NULL, 6, 'Flottmann, Neusüß', 'Seminar Analytische Chemie', '0215', 1, 6),\n" +
"('C', NULL, 1, 'Junker, Triebel', 'Praktikum Labortechnik', '0147', 2, 1),\n" +
"('C', NULL, 1, 'Junker, Triebel', 'Praktikum Labortechnik', '0147', 2, 2),\n" +
"('C', NULL, 1, 'Junker, Triebel', 'Praktikum Labortechnik', '0147', 4, 1),\n" +
"('C', NULL, 1, 'Junker, Triebel', 'Praktikum Labortechnik', '0147', 4, 2),\n" +
"('C', NULL, 1, 'Junker, Triebel', 'Praktikum Labortechnik', '0147', 5, 1),\n" +
"('C', NULL, 1, 'Junker, Triebel', 'Praktikum Labortechnik', '0147', 5, 2),\n" +
"('C', NULL, 1, 'Junker, Triebel', 'Praktikum Labortechnik', '0147', 5, 3),\n" +
"('C', NULL, 1, 'Junker, Triebel', 'Praktikum Labortechnik', '0147', 5, 4),\n" +
"('C', NULL, 1, 'Junker, Triebel', 'Praktikum Labortechnik', '0147', 5, 5),\n" +
"('C', NULL, 1, 'Junker, Triebel', 'Praktikum Labortechnik', 'null, null', 2, 3),\n" +
"('C', NULL, 1, 'Junker, Triebel', 'Praktikum Labortechnik', 'null, null', 3, 3),\n" +
"('C', NULL, 1, 'Junker, Triebel', 'Praktikum Labortechnik', 'null, null', 3, 4),\n" +
"('C', NULL, 3, 'Wagner (C)', 'Physikalische Chemie 2', '0203', 2, 3),\n" +
"('M', 'R', 6, 'Gretzschel', 'Alternative Antriebe', '0115', 1, 4),\n" +
"('M', 'E', 7, 'Gretzschel', 'Alternative Antriebe', '0115', 1, 4),\n" +
"('VMM', NULL, 7, 'Gretzschel', 'Antriebstechnik 2', '0115', 1, 4),\n" +
"('VI', NULL, 6, 'Gretzschel', 'Antriebstechnik 2', '0115', 1, 4),\n" +
"('M', 'R', 7, 'Rittmann', 'Experimentelle Übungen', '0212', 1, 5),\n" +
"('M', 'R', 7, 'Hubmann', 'Windenergie', '0102', 5, 1),\n" +
"('M', 'R', 7, 'Hubmann', 'Windenergie', '0102', 5, 2),\n" +
"('FR', NULL, 6, 'Holl', 'Recht und Normen', 'G1 0.06', 3, 1),\n" +
"('FR', NULL, 6, 'Holl', 'Recht und Normen', 'G1 0.06', 3, 2),\n" +
"('VI', NULL, 1, 'Albrecht', 'Physik Ueb.', '0203', 3, 2),\n" +
"('V', NULL, 7, 'Möckel', 'Analytische Methoden u. Labor', '0174', 5, 1),\n" +
"('V', NULL, 7, 'Möckel', 'Analytische Methoden u. Labor', '0174', 5, 2),\n" +
"('V', NULL, 7, 'Möckel', 'Analytische Methoden u. Labor', '0174', 5, 3),\n" +
"('V', NULL, 6, 'Möckel', 'Analytische Methoden u. Labor', '0174', 5, 1),\n" +
"('V', NULL, 6, 'Möckel', 'Analytische Methoden u. Labor', '0174', 5, 2),\n" +
"('V', NULL, 6, 'Möckel', 'Analytische Methoden u. Labor', '0174', 5, 3),\n" +
"('V', NULL, 7, 'Hass', 'Lackiertechnik Labor', '0124', 5, 1),\n" +
"('V', NULL, 7, 'Hass', 'Lackiertechnik Labor', '0124', 5, 2),\n" +
"('V', NULL, 7, 'Hass', 'Lackiertechnik Labor', '0124', 5, 3),\n" +
"('V', NULL, 6, 'Hass', 'Lackiertechnik Labor', '0124', 5, 1),\n" +
"('V', NULL, 6, 'Hass', 'Lackiertechnik Labor', '0124', 5, 2),\n" +
"('V', NULL, 6, 'Hass', 'Lackiertechnik Labor', '0124', 5, 3),\n" +
"('VMG', NULL, 1, 'Schulz', 'Mikroskopische und analytische Verfahren', '0223', 1, 4),\n" +
"('VMG', NULL, 1, 'Schulz', 'Mikroskopische und analytische Verfahren', '0223', 1, 5),\n" +
"('VMG', NULL, 1, 'Ketzer-Raichle', 'Einführung Materialographische Präparation', '0185', 2, 4),\n" +
"('VMG', NULL, 1, 'Ketzer-Raichle', 'Einführung Materialographische Präparation', '0185', 2, 5),\n" +
"('F', NULL, 3, 'Justen', 'Mathematik 3', 'G1 0.22', 1, 6),\n" +
"('F', NULL, 3, 'Justen', 'Mathematik 3', 'G1 0.22', 4, 6),\n" +
"('MekA', NULL, 3, 'Justen', 'Mathematik 3', 'G1 0.22', 1, 6),\n" +
"('MekA', NULL, 3, 'Justen', 'Mathematik 3', 'G1 0.22', 4, 6),\n" +
"('PEF', NULL, NULL, 'Schuhmacher', 'Zerstörungsfreie Bauteilprüfung', '0114', 2, 5),\n" +
"('IN', 'MI', 1, 'Werthebach', 'Digitale Photographie', 'G2 0.23', 3, 3),\n" +
"('IN', 'MI', 2, 'Werthebach', 'Digitale Photographie', 'G2 0.23', 3, 3),\n" +
"('P', NULL, 1, 'Rozsa', 'Technisches Zeichnen', '0212', 5, 2),\n" +
"('P', NULL, 1, 'Rozsa', 'Technisches Zeichnen', '0212', 5, 3),\n" +
"('P', NULL, 1, 'Rozsa', 'Technisches Zeichnen', '0212', 5, 4),\n" +
"('P', NULL, 1, 'Rozsa', 'Technisches Zeichnen', '0212', 5, 5),\n" +
"('C', NULL, 2, 'Triebel', 'Analytische Chemie 1', '0132', 1, 2),\n" +
"('O', NULL, 1, 'Schneider (O)', 'Strukturiertes Programmieren', 'G1 2.36', 1, 4),\n" +
"('O', NULL, 2, 'Tomas', 'CAD', '0289', 3, 4),\n" +
"('O', NULL, 2, 'Tomas', 'CAD', '0289', 3, 5),\n" +
"('O', NULL, 4, 'Kettler', 'Mikrocontroller', 'G1 0.06', 1, 4),\n" +
"('O', NULL, 3, 'Bälder', 'Betriebswirtschaftslehre', 'G1 0.01', 1, 5),\n" +
"('O', NULL, 3, 'Bälder', 'Unternehmensorganisation', 'G1 0.22', 1, 4),\n" +
"('F', NULL, 1, 'Schmidt H.', 'Übungen Elektrotechnik', 'AH -1.01', 2, 3),\n" +
"('GF', NULL, 3, 'Schmidt H.', 'Übungen Elektrotechnik', 'AH -1.01', 2, 3),\n" +
"('FR', NULL, 2, 'Schmidt H.', 'Übungen Elektrotechnik', 'AH -1.01', 2, 3),\n" +
"('FR', NULL, 3, 'Schmidt H.', 'Übungen Elektrotechnik', 'AH -1.01', 2, 3),\n" +
"('MekA', NULL, 1, 'Schmidt H.', 'Übungen Elektrotechnik', 'AH -1.01', 2, 3),\n" +
"('ET', NULL, 1, 'Schmidt H.', 'Übungen Elektrotechnik', 'AH -1.01', 2, 3),\n" +
"('M', 'E, R, K', 7, 'Rittmann', 'Energietechnik', '0103', 1, 2),\n" +
"('M', 'E, R, K', 7, 'Rittmann', 'Energietechnik', '0114', 4, 1),\n" +
"('IN', 'IS, MI, SE', 4, 'Werthebach', 'Rechnernetze', 'G2 0.21', 1, 2),\n" +
"('IN', 'IS, MI, SE', 4, 'Werthebach', 'Rechnernetze', 'G2 2.01', 4, 1),\n" +
"('IN', 'IS, MI, SE, WI', 4, 'Hellmann', 'IT-Sicherheit', 'G2 2.01', 4, 2),\n" +
"('C', NULL, 3, 'Flottmann', 'Analytische Chemie 2', '0131, 0132', 4, 5),\n" +
"('C', NULL, 3, 'Flottmann', 'Analytische Chemie 2', '0131, 0142', 4, 4),\n" +
"('C', NULL, 3, 'Flottmann', 'Analytische Chemie 2', '0131, 0132', 4, 6),\n" +
"('C', NULL, 3, 'Flottmann', 'Analytische Chemie 2', '0157, 0233', 1, 2),\n" +
"('C', NULL, 3, 'Flottmann', 'Analytische Chemie 2', '0157, 0231', 2, 1),\n" +
"('V', NULL, 7, 'Hass, Sörgel', 'Spezielle Verfahren der Galvanotechnik - Labor', '0168', 4, 2),\n" +
"('V', NULL, 7, 'Hass, Sörgel', 'Spezielle Verfahren der Galvanotechnik - Labor', '0168', 4, 3),\n" +
"('O', NULL, 4, 'Hahn, Kettler', 'Systemtechnik Labor', 'G1 2.33, G1 2.36', 2, 4),\n" +
"('O', NULL, 4, 'Hahn, Kettler', 'Systemtechnik Labor', 'G1 2.33, G1 2.36', 2, 5),\n" +
"('O', NULL, 4, 'Hahn, Kettler', 'Systemtechnik Labor', 'G1 2.33, G1 2.36', 2, 6),\n" +
"('IN', 'IS', 4, 'Rössel', 'IT-Sicherheits- und Servicemanagement', 'G2 0.37', 1, 5),\n" +
"('IN', 'WI', 7, 'Rössel', 'IT-Sicherheits- und Servicemanagement', 'G2 0.37', 1, 5),\n" +
"('WI', NULL, 1, 'Fischer', 'Mathematik für Wirtschaftsinformatiker', 'G2 0.21', 2, 2),\n" +
"('WI', NULL, 1, 'Fischer', 'Mathematik für Wirtschaftsinformatiker', 'G2 0.21', 2, 3),\n" +
"('WI', NULL, 1, 'Burdack', 'Statistik', 'G2 2.41', 1, 2),\n" +
"('WI', NULL, 1, 'Burdack', 'Operations Research', 'G2 2.01', 3, 3),\n" +
"('IN', 'WI', 4, 'Knobelspies', 'Wirtschaftsenglisch', 'G2 1.01', 2, 1),\n" +
"('IN', 'WI', 4, 'Knobelspies', 'Wirtschaftsenglisch', 'G2 2.41', 4, 1),\n" +
"('WI', NULL, 1, 'Knobelspies', 'Wirtschaftsenglisch', 'G2 1.01', 2, 1),\n" +
"('WI', NULL, 1, 'Knobelspies', 'Wirtschaftsenglisch', 'G2 2.41', 4, 1),\n" +
"('F', NULL, 1, 'Höfig', 'Konstruktionselemente 1', 'G1 1.32', 5, 4),\n" +
"('FR', NULL, 2, 'Höfig', 'Konstruktionselemente 1', 'G1 1.32', 5, 4),\n" +
"('F', NULL, 2, 'Schmidt Ho', 'Physik 1', 'AH -1.01', 1, 4),\n" +
"('F', NULL, 2, 'Schmidt Ho', 'Physik 1', 'AH -1.01', 3, 4);" 
,"INSERT INTO g02_vorlesungsplan (stg, schwerpunkt, semester, dozent, kurs, raum, tagnr,  zeitblocknr) VALUES\n" +
            "('MekA', NULL, 1, 'Schmidt Ho', 'Physik 1', 'AH -1.01', 1, 4),\n" +
"('MekA', NULL, 1, 'Schmidt Ho', 'Physik 1', 'AH -1.01', 3, 4),\n" +
"('FR', NULL, 3, 'Schmidt Ho', 'Physik 1', 'AH -1.01', 1, 4),\n" +
"('FR', NULL, 3, 'Schmidt Ho', 'Physik 1', 'AH -1.01', 3, 4),\n" +
"('GF', NULL, 1, 'Schmidt Ho', 'Physik 1', 'AH -1.01', 1, 4),\n" +
"('GF', NULL, 1, 'Schmidt Ho', 'Physik 1', 'AH -1.01', 3, 4),\n" +
"('FR', NULL, 1, 'Finkbeiner', 'Objektorientierte Systementwicklung', 'G1 1.28', 3, 1),\n" +
"('FR', NULL, 1, 'Finkbeiner', 'Objektorientierte Systementwicklung', 'G1 1.28', 3, 2),\n" +
"('IN', 'MI', 6, 'Klauck', 'Computergraphik und Animation', 'G2 0.37', 1, 1),\n" +
"('IN', 'MI', 6, 'Klauck', 'Computergraphik und Animation', 'G2 0.37', 1, 2),\n" +
"('IN', 'MI', 7, 'Klauck', 'Computergraphik und Animation', 'G2 0.37', 1, 1),\n" +
"('IN', 'MI', 7, 'Klauck', 'Computergraphik und Animation', 'G2 0.37', 1, 2),\n" +
"('E', 'TI', 7, 'Klauck', 'Computergraphik und Animation', 'G2 0.37', 1, 1),\n" +
"('E', 'TI', 7, 'Klauck', 'Computergraphik und Animation', 'G2 0.37', 1, 2),\n" +
"('F', NULL, 2, 'Schmidt H.', 'Laborführerschein Elektronik', 'G1 0.34', 5, 1),\n" +
"('GF', NULL, 3, 'Schmidt H.', 'Laborführerschein Elektronik', 'G1 0.34', 5, 1),\n" +
"('IN', 'SE', 7, 'Oberhauser', 'SW-Qualitätsmanagement', 'G2 0.21', 3, 4),\n" +
"('IN', 'SE', 6, 'Oberhauser', 'SW-Qualitätsmanagement', 'G2 0.21', 3, 4),\n" +
"('F', NULL, 1, 'Schmitt', 'Allgemeine Mechanik', 'G1 0.20', 4, 1),\n" +
"('F', NULL, 1, 'Schmitt', 'Allgemeine Mechanik', 'G1 0.20', 4, 4),\n" +
"('F', NULL, 1, 'Schmitt', 'Allgemeine Mechanik', 'G1 0.20', 5, 2),\n" +
"('MekA', NULL, 1, 'Schmitt', 'Allgemeine Mechanik', 'G1 0.20', 4, 1),\n" +
"('MekA', NULL, 1, 'Schmitt', 'Allgemeine Mechanik', 'G1 0.20', 4, 4),\n" +
"('MekA', NULL, 1, 'Schmitt', 'Allgemeine Mechanik', 'G1 0.20', 5, 2),\n" +
"('GE', NULL, 2, 'Schmitt', 'Allgemeine Mechanik', 'G1 0.20', 4, 1),\n" +
"('GE', NULL, 2, 'Schmitt', 'Allgemeine Mechanik', 'G1 0.20', 4, 4),\n" +
"('GE', NULL, 2, 'Schmitt', 'Allgemeine Mechanik', 'G1 0.20', 5, 2),\n" +
"('GF', NULL, 1, 'Schmitt', 'Allgemeine Mechanik', 'G1 0.20', 4, 1),\n" +
"('GF', NULL, 1, 'Schmitt', 'Allgemeine Mechanik', 'G1 0.20', 4, 4),\n" +
"('GF', NULL, 1, 'Schmitt', 'Allgemeine Mechanik', 'G1 0.20', 5, 2),\n" +
"('FR', NULL, 1, 'Schmitt', 'Allgemeine Mechanik', 'G1 0.20', 4, 1),\n" +
"('FR', NULL, 1, 'Schmitt', 'Allgemeine Mechanik', 'G1 0.20', 4, 4),\n" +
"('FR', NULL, 1, 'Schmitt', 'Allgemeine Mechanik', 'G1 0.20', 5, 2),\n" +
"('IN', 'SE', 7, 'Oberhauser', 'SW-Qualitätsmanagement', 'G2 1.30~G2 0.30', 4, 3),\n" +
"('IN', 'SE', 6, 'Oberhauser', 'SW-Qualitätsmanagement', 'G2 1.30~G2 0.30', 4, 3),\n" +
"('F', NULL, 2, 'Glaser', 'Fertigungstechnik 1', 'AH 0.01', 2, 1),\n" +
"('F', NULL, 2, 'Glaser', 'Fertigungstechnik 1', 'G1 1.32', 5, 2),\n" +
"('FR', NULL, 4, 'Glaser', 'Fertigungstechnik 1', 'AH 0.01', 2, 1),\n" +
"('FR', NULL, 4, 'Glaser', 'Fertigungstechnik 1', 'G1 1.32', 5, 2),\n" +
"('FR', NULL, 2, 'Richter C.', 'Visuelle Wahrnehmung und Gestaltung', 'G1 0.05', 2, 1),\n" +
"('FR', NULL, 2, 'Richter C.', 'Visuelle Wahrnehmung und Gestaltung', 'G1 0.06', 5, 1),\n" +
"('FR', NULL, 2, 'Richter C.', 'Visuelle Wahrnehmung und Gestaltung', 'G1 0.06', 5, 2),\n" +
"('FR', NULL, 2, 'Schwan', 'Layoutsysteme 2 - InDesign', 'G1 1.29', 4, 1),\n" +
"('F', NULL, 1, 'Eichinger', 'Werkstoffkunde', 'G1 0.20', 5, 1),\n" +
"('MekA', NULL, 1, 'Eichinger', 'Werkstoffkunde', 'G1 0.20', 5, 1),\n" +
"('GE', NULL, 2, 'Eichinger', 'Werkstoffkunde', 'G1 0.20', 5, 1),\n" +
"('GF', NULL, 1, 'Eichinger', 'Werkstoffkunde', 'G1 0.20', 5, 1),\n" +
"('FR', NULL, 1, 'Eichinger', 'Werkstoffkunde', 'G1 0.20', 5, 1),\n" +
"('FR', NULL, 2, 'Weissgerber', 'Technische Dokumentation 2', 'G2 1.03', 2, 2),\n" +
"('FR', NULL, 2, 'Weissgerber', 'Technische Dokumentation 2', 'G1 1.20', 4, 4),\n" +
"('F', NULL, 2, 'Holzwarth', 'Konstruktionselemente 2', 'G1 0.20', 3, 1),\n" +
"('FR', NULL, 3, 'Holzwarth', 'Konstruktionselemente 2', 'G1 0.20', 3, 1),\n" +
"('GF', NULL, 3, 'Holzwarth', 'Konstruktionselemente 2', 'G1 0.20', 3, 1),\n" +
"('FR', NULL, 2, 'Reznicek', 'Gestaltung Grundlagen', 'G1 1.29', 3, 5),\n" +
"('FR', NULL, 2, 'Reznicek', 'Gestaltung Grundlagen', 'G1 1.29', 3, 6),\n" +
"('IN', 'IS, MI, SE, WI', 4, 'Oberhauser', 'Software-Projectmanagement', 'G2 0.01', 4, 4),\n" +
"('IN', 'IS', 7, 'Karg', 'Kryptographische Algorithmen', 'G2 1.28, G2 2.01', 1, 4),\n" +
"('IN', 'IS', 7, 'Karg', 'Kryptographische Algorithmen', 'G2 1.28, G2 1.44', 2, 2),\n" +
"('IN', 'IS', 6, 'Karg', 'Kryptographische Algorithmen', 'G2 1.28, G2 1.44', 2, 2),\n" +
"('IN', 'IS', 6, 'Karg', 'Kryptographische Algorithmen', 'G2 1.28, G2 2.01', 1, 4),\n" +
"('F', NULL, 2, 'Hörmann', 'Elektronische Bauelemente', 'G1 1.32', 1, 5),\n" +
"('F', NULL, 2, 'Hörmann', 'Elektronische Bauelemente', 'G1 1.32', 2, 2),\n" +
"('FR', NULL, 3, 'Hörmann', 'Elektronische Bauelemente', 'G1 1.32', 1, 5),\n" +
"('FR', NULL, 3, 'Hörmann', 'Elektronische Bauelemente', 'G1 1.32', 2, 2),\n" +
"('F', NULL, 2, 'Bäuerle', 'Angewandte Programmierung', 'G1 1.28', 1, 2),\n" +
"('F', NULL, 2, 'Bäuerle', 'Angewandte Programmierung', 'G1 1.28', 1, 3),\n" +
"('GE', NULL, 6, 'Bäuerle', 'Angewandte Programmierung', 'G1 1.28', 1, 2),\n" +
"('GE', NULL, 6, 'Bäuerle', 'Angewandte Programmierung', 'G1 1.28', 1, 3),\n" +
"('FR', NULL, 4, 'Richter C.', 'Begleitveranstaltungen und Vorträge zum Praxissemester', 'G1 0.20', 3, 6),\n" +
"('F', NULL, 4, 'Richter C.', 'Begleitveranstaltungen und Vorträge zum Praxissemester', 'G1 0.20', 3, 6),\n" +
"('GE', NULL, 4, 'Richter C.', 'Begleitveranstaltungen und Vorträge zum Praxissemester', 'G1 0.20', 3, 6),\n" +
"('FR', NULL, 6, 'Richter C.', 'Begleitveranstaltungen und Vorträge zum Praxissemester', 'G1 0.20', 3, 6),\n" +
"('F', NULL, 6, 'Richter C.', 'Begleitveranstaltungen und Vorträge zum Praxissemester', 'G1 0.20', 3, 6),\n" +
"('GE', NULL, 6, 'Richter C.', 'Begleitveranstaltungen und Vorträge zum Praxissemester', 'G1 0.20', 3, 6),\n" +
"('MekA', NULL, 3, 'Richter C.', 'Begleitveranstaltungen und Vorträge zum Praxissemester', 'G1 0.20', 3, 6),\n" +
"('F', NULL, 2, 'Bäuerle', 'Labor Angewandte Programmierung', 'G1 1.28', 4, 4),\n" +
"('GE', NULL, 6, 'Bäuerle', 'Labor Angewandte Programmierung', 'G1 1.28', 4, 4),\n" +
"('ET', NULL, 1, 'Bantel', 'Strukturierte Programmierung', 'G2 0.23', 1, 4),\n" +
"('ET', NULL, 1, 'Bantel', 'Strukturierte Programmierung', 'G2 0.23', 3, 4),\n" +
"('ET', NULL, 1, 'Bantel', 'Strukturierte Programmierung', 'G2 0.23', 3, 5),\n" +
"('ET', NULL, 1, 'Hommel', 'Mathematik Tutorium', 'G2 1.01', 3, 3),\n" +
"('M', NULL, 4, 'Schuck', 'Strömungslehre', 'AH 1.01', 4, 5),\n" +
"('M', NULL, 4, 'Schuck', 'Strömungslehre', 'AH 1.01', 4, 6),\n" +
"('P', NULL, 4, 'Schuck', 'Strömungslehre', 'AH 1.01', 4, 5),\n" +
"('P', NULL, 4, 'Schuck', 'Strömungslehre', 'AH 1.01', 4, 6),\n" +
"('VMM', NULL, 6, 'Schuck', 'Strömungslehre / Wärmelehre', 'AH 1.01', 4, 5),\n" +
"('VMM', NULL, 6, 'Schuck', 'Strömungslehre / Wärmelehre', 'AH 1.01', 4, 6),\n" +
"('F', NULL, 2, 'Hörmann', 'Elektronische Bauelemente', 'G1 0.22', 5, 3),\n" +
"('FR', NULL, 3, 'Hörmann', 'Elektronische Bauelemente', 'G1 0.22', 5, 3),\n" +
"('M', NULL, 4, 'Kalhöfer', 'Fertigungstechnik', '0201', 2, 4),\n" +
"('F', NULL, 1, 'Eichinger', 'Werkstoffkunde', 'G1 0.20', 1, 5),\n" +
"('MekA', NULL, 1, 'Eichinger', 'Werkstoffkunde', 'G1 0.20', 1, 5),\n" +
"('GE', NULL, 2, 'Eichinger', 'Werkstoffkunde', 'G1 0.20', 1, 5),\n" +
"('GF', NULL, 1, 'Eichinger', 'Werkstoffkunde', 'G1 0.20', 1, 5),\n" +
"('FR', NULL, 1, 'Eichinger', 'Werkstoffkunde', 'G1 0.20', 1, 5),\n" +
"('PTC', NULL, 1, 'Frick', 'Polymer Design', '0203', 4, 3),\n" +
"('M', NULL, 2, 'Athanasiou', 'Festigkeitslehre 2', 'G2 2.41', 2, 5),\n" +
"('M', NULL, 2, 'Athanasiou', 'Festigkeitslehre 2', 'G2 2.41', 2, 6),\n" +
"('P', NULL, 4, 'Schwarzer', 'Produktentwicklung / Konstruktion I', '0129', 3, 5),\n" +
"('P', NULL, 4, 'Schwarzer', 'Produktentwicklung / Konstruktion I', '0129', 3, 6),\n" +
"('PTC', NULL, 1, 'Leyrer', 'Injection Moulding Lab', '0701', 1, 1),\n" +
"('PTC', NULL, 1, 'Leyrer', 'Injection Moulding Lab', '0189', 1, 2),\n" +
"('PTC', NULL, 1, 'Leyrer', 'Injection Moulding Lab', '0189', 1, 3),\n" +
"('PTC', NULL, 1, 'Leyrer', 'Injection Moulding Lab', '0189', 1, 4),\n" +
"('LBM', NULL, NULL, 'Leyrer', 'Injection Moulding Lab', '0701', 1, 1),\n" +
"('LBM', NULL, NULL, 'Leyrer', 'Injection Moulding Lab', '0189', 1, 2),\n" +
"('LBM', NULL, NULL, 'Leyrer', 'Injection Moulding Lab', '0189', 1, 3),\n" +
"('LBM', NULL, NULL, 'Leyrer', 'Injection Moulding Lab', '0189', 1, 4),\n" +
"('M', NULL, 1, 'Otto K. -P', 'Festigkeitslehre 1', '0133', 4, 2),\n" +
"('MW', NULL, 1, 'Kallien', 'Werkstoffkunde', '0213', 4, 1),\n" +
"('MW', NULL, 1, 'Kallien', 'Werkstoffkunde', '0213', 4, 2),\n" +
"('MP', NULL, 1, 'Kallien', 'Werkstoffkunde', '0213', 2, 2),\n" +
"('MP', NULL, 1, 'Kallien', 'Werkstoffkunde', '0213', 2, 3),\n" +
"('M', NULL, 2, 'Zehnder', 'Maschinenelemente 1 Übungen', '0120', 1, 2),\n" +
"('M', NULL, 2, 'Zehnder', 'Maschinenelemente 1 Übungen', '0115', 5, 1),\n" +
"('M', NULL, 2, 'Zehnder', 'Maschinenelemente 1 Übungen', '0115', 5, 2),\n" +
"('MP', NULL, 1, 'Haag', 'Konstruktion 1', '0288', 1, 4),\n" +
"('MP', NULL, 1, 'Haag', 'Konstruktion 1', '0288', 1, 5),\n" +
"('A', NULL, 3, 'Liebhäusser', 'Werkstatt 2', 'G4 0.09', 2, 2),\n" +
"('A', NULL, 3, 'Liebhäusser', 'Werkstatt 2', 'G4 0.09', 2, 3),\n" +
"('IN', 'IN, MI, SE, IS', 1, 'Bantel', 'Strukturierte Programmierung', 'G2 0.23', 1, 4),\n" +
"('IN', 'IN, MI, SE, IS', 1, 'Bantel', 'Strukturierte Programmierung', 'G2 0.23', 3, 4),\n" +
"('IN', 'IN, MI, SE, IS', 1, 'Bantel', 'Strukturierte Programmierung', 'G2 0.23', 3, 5),\n" +
"('PTC', NULL, 1, 'Kaiser', 'Advanced Rheology', '0201, 0288', 4, 4),\n" +
"('MW', NULL, 1, 'Haag, Müllner', 'Konstruktion 1', '0288, 0129', 2, 5),\n" +
"('M', NULL, 3, 'Kaufmann, Schillinger', 'Schweißlabor', '0202, 0192', 3, 2),\n" +
"('M', NULL, 3, 'Kaufmann, Schillinger', 'Schweißlabor', '0202, 0192', 3, 1),\n" +
"('MW', NULL, 1, 'Haag, Müllner', 'Konstruktion 1', '0288, 0129', 2, 4),\n" +
"('A', NULL, 3, 'Limberger', 'Vorbereitung Praxissemester', 'G4 0.02', 4, 3),\n" +
"('A', NULL, 3, 'Nagl', 'Vorbereitung Auslandssemester', 'G4 0.02', 4, 3),\n" +
"('A', NULL, 6, 'Nagl', 'W-Buchführung', 'G4 -1.01', 4, 6),\n" +
"('AH', NULL, 6, 'Nagl', 'W-Buchführung', 'G4 -1.01', 4, 6),\n" +
"('A', NULL, 7, 'Nagl', 'W-Buchführung', 'G4 -1.01', 4, 6),\n" +
"('V', NULL, 1, 'Hader', 'Mathematische Grundlagen', '0133', 1, 1),\n" +
"('V', NULL, 1, 'Hader', 'Mathematische Grundlagen', '0133', 4, 3),\n" +
"('VMG', NULL, 1, 'Hader', 'Mathematische Grundlagen', '0133', 1, 1),\n" +
"('VMG', NULL, 1, 'Hader', 'Mathematische Grundlagen', '0133', 4, 3),\n" +
"('VMM', NULL, 1, 'Hader', 'Mathematische Grundlagen', '0133', 1, 1),\n" +
"('VMM', NULL, 1, 'Hader', 'Mathematische Grundlagen', '0133', 4, 3),\n" +
"('C', NULL, 1, 'Hader', 'Mathematische Grundlagen', '0133', 1, 1),\n" +
"('C', NULL, 1, 'Hader', 'Mathematische Grundlagen', '0133', 4, 3),\n" +
"('C', NULL, 2, 'Hader', 'Mathematische Grundlagen', '0133', 1, 1),\n" +
"('C', NULL, 2, 'Hader', 'Mathematische Grundlagen', '0133', 4, 3),\n" +
"('A', NULL, 3, NULL, 'Objektive / Subjektive Refraktion Übungen', 'G4 1.07', 1, 4),\n" +
"('A', NULL, 3, NULL, 'Objektive / Subjektive Refraktion Übungen', 'G4 1.07', 1, 5),\n" +
"('A', NULL, 3, NULL, 'Objektive / Subjektive Refraktion Übungen', 'G4 1.07', 4, 1),\n" +
"('A', NULL, 3, NULL, 'Objektive / Subjektive Refraktion Übungen', 'G4 1.07', 4, 2),\n" +
"('A', NULL, 2, 'Buschle', 'Physical Optics Laboratory', 'G4 0.02', 5, 4),\n" +
"('A', NULL, 2, 'Buschle', 'Physical Optics Laboratory', 'G4 0.02', 5, 5),\n" +
"('A', NULL, 2, 'Paffrath', 'Geometrische Optik 2', 'G4 0.02', 5, 4),\n" +
"('A', NULL, 2, 'Paffrath', 'Geometrische Optik 2', 'G4 0.02', 5, 5),\n" +
"('AH', NULL, 2, 'Paffrath', 'Geometrische Optik 2', 'G4 0.02', 5, 4),\n" +
"('AH', NULL, 2, 'Paffrath', 'Geometrische Optik 2', 'G4 0.02', 5, 5),\n" +
"('C', NULL, 2, 'Schäfer R.', 'Anorganische Chemie 1', '0132', 2, 1),\n" +
"('C', NULL, 2, 'Schäfer R.', 'Anorganische Chemie 1', '0132', 2, 2),\n" +
"('A', NULL, 4, 'Schiefer', 'Binokularsehen', 'G4 -1.01', 2, 2),\n" +
"('AH', NULL, 4, 'Schiefer', 'Binokularsehen', 'G4 -1.01', 2, 2),\n" +
"('ZDO', NULL, NULL, 'Freller', 'Deutsch als Fremdsprache B2', '0007', 1, 5),\n" +
"('ZDO', NULL, NULL, 'Freller', 'Deutsch als Fremdsprache B2', '0007', 1, 6),\n" +
"('PEF', NULL, NULL, 'Feuchter', 'Computational Fluid Dynamic (CFD) mit Übungen', '0273', 1, 4),\n" +
"('PEF', NULL, NULL, 'Feuchter', 'Computational Fluid Dynamic (CFD) mit Übungen', '0273', 1, 5),\n" +
"('TME', NULL, NULL, 'Feuchter', 'Computational Fluid Dynamic (CFD) mit Übungen', '0273', 1, 4),\n" +
"('TME', NULL, NULL, 'Feuchter', 'Computational Fluid Dynamic (CFD) mit Übungen', '0273', 1, 5),\n" +
"('TMP', NULL, NULL, 'Feuchter', 'Computational Fluid Dynamic (CFD) mit Übungen', '0273', 1, 4),\n" +
"('TMP', NULL, NULL, 'Feuchter', 'Computational Fluid Dynamic (CFD) mit Übungen', '0273', 1, 5),\n" +
"('PEF', NULL, NULL, 'Feuchter', 'Computational Fluid Dynamic (CFD)', '0289', 1, 4),\n" +
"('PEF', NULL, NULL, 'Feuchter', 'Computational Fluid Dynamic (CFD)', '0289', 1, 5),\n" +
"('TME', NULL, NULL, 'Feuchter', 'Computational Fluid Dynamic (CFD)', '0289', 1, 4),\n" +
"('TME', NULL, NULL, 'Feuchter', 'Computational Fluid Dynamic (CFD)', '0289', 1, 5),\n" +
"('TMP', NULL, NULL, 'Feuchter', 'Computational Fluid Dynamic (CFD)', '0289', 1, 4),\n" +
"('TMP', NULL, NULL, 'Feuchter', 'Computational Fluid Dynamic (CFD)', '0289', 1, 5),\n" +
"('P', NULL, 1, 'Otto K. -P', 'Festigkeitslehre', '0133', 2, 3),\n" +
"('C', NULL, 2, 'Wagner (C)', 'Physikalische Chemie 1', '0122', 3, 2),\n" +
"('C', NULL, 2, 'Wagner (C)', 'Physikalische Chemie 1', '0233', 4, 2),\n" +
"('B', NULL, 4, 'Haubrock', 'Personalmanagement', '0101', 1, 1),\n" +
"('B', NULL, 4, 'Haubrock', 'Personalmanagement', '0211', 3, 1),\n" +
"('B', NULL, 4, 'Niethammer', 'Technologiemanagement / Wissenschaftl. Arbeiten', '0105', 3, 2),\n" +
"('B', NULL, 4, 'Niethammer', 'Technologiemanagement / Wissenschaftl. Arbeiten', '0105', 3, 3),\n" +
"('IN', 'IS', 4, 'Rössel', 'IT-Management', 'G2 0.37', 1, 4),\n" +
"('IN', 'WI', 7, 'Rössel', 'IT-Management', 'G2 0.37', 1, 4),\n" +
"('GF', NULL, 3, 'Bartkowiak', '3-D-CAD', '0289', 2, 5),\n" +
"('GF', NULL, 3, 'Bartkowiak', '3-D-CAD', '0289~0034', 2, 6),\n" +
"('F', NULL, 2, 'Bartkowiak', '3-D-CAD', '0289', 2, 5),\n" +
"('F', NULL, 2, 'Bartkowiak', '3-D-CAD', '0289~0034', 2, 6),\n" +
"('C', NULL, 6, 'Schnell', 'Biochemie', '0060', 2, 2),\n" +
"('C', NULL, 6, 'Schnell', 'Biochemie', '0215', 3, 2),\n" +
"('O', NULL, 6, 'Hellmuth', 'Lasertechnik', 'G1 0.22', 1, 2),\n" +
"('O', NULL, 6, 'Hellmuth', 'Lasertechnik', 'G1 0.21', 1, 3),\n" +
"('FOI', NULL, NULL, 'Hellmuth', 'Laser Technology', 'G1 0.22', 1, 2),\n" +
"('A', NULL, 6, 'Nagl, Rath Heinrich', 'Berufspädagogik', 'G4 2.10', 5, 4),\n" +
"('AH', NULL, 6, 'Nagl, Rath Heinrich', 'Berufspädagogik', 'G4 2.10', 5, 4),\n" +
"('A', NULL, 4, 'Lanzinger, Michels', 'Praktikum Binokularsehen', 'G4 2.02, G4 1.07', 3, 1),\n" +
"('A', NULL, 4, 'Lanzinger, Michels', 'Praktikum Binokularsehen', 'G4 2.02, G4 1.07', 3, 2),\n" +
"('AH', NULL, 4, 'Lanzinger, Michels', 'Praktikum Binokularsehen', 'G4 2.02, G4 1.07', 3, 1),\n" +
"('AH', NULL, 4, 'Lanzinger, Michels', 'Praktikum Binokularsehen', 'G4 2.02, G4 1.07', 3, 2),\n" +
"('A', NULL, 3, 'Buschle, Michels', 'Sehfunktionen / Lichttechnik Praktikum', 'G4 2.02, G4 2.01', 1, 4),\n" +
"('A', NULL, 3, 'Buschle, Michels', 'Sehfunktionen / Lichttechnik Praktikum', 'G4 2.02, G4 2.01', 1, 5),\n" +
"('A', NULL, 3, 'Buschle, Michels', 'Sehfunktionen / Lichttechnik Praktikum', 'G4 2.02, G4 2.01', 4, 1),\n" +
"('A', NULL, 3, 'Buschle, Michels', 'Sehfunktionen / Lichttechnik Praktikum', 'G4 2.02, G4 2.01', 4, 2),\n" +
"('FOI', NULL, NULL, 'Hellmuth', 'Laser Technology', 'G1 0.21', 1, 3),\n" +
"('C', NULL, 6, 'Triebel', 'Polymeranalytik', '0115', 3, 3),\n" +
"('F', NULL, 1, 'Eichinger', 'Technisches Zeichnen Übungen', 'G1 1.32', 1, 4),\n" +
"('FR', NULL, 2, 'Eichinger', 'Technisches Zeichnen Übungen', 'G1 1.32', 1, 4),\n" +
"('O', NULL, 4, 'Kettler', 'Systeme und Signale', 'G1 0.05', 2, 2),\n" +
"('C', NULL, 1, 'Flottmann', 'Laborkunde', '0132', 4, 1),\n" +
"('O', NULL, 4, 'Bauer (O)', 'Entwicklungsmanagement', 'G1 1.23', 5, 3),\n" +
"('O', NULL, 6, 'Bauer (O)', 'Entwicklungsmanagement', 'G1 1.23', 5, 3),\n" +
"('O', NULL, 4, 'Bauer (O)', 'Marketing', 'G1 1.23', 3, 2),\n" +
"('O', NULL, 4, 'Bauer (O)', 'Rechtliche Aspekte des PM', 'G1 1.23', 5, 2),\n" +
"('O', NULL, 6, 'Bauer (O)', 'Rechtliche Aspekte des PM', 'G1 1.23', 5, 2),\n" +
"('A', NULL, 1, 'Paffrath', 'Mathematik&Wellenlehre', '0124', 2, 1),\n" +
"('A', NULL, 1, 'Paffrath', 'Mathematik&Wellenlehre', '0124', 2, 2),\n" +
"('A', NULL, 1, 'Nolting', 'Praktische Informatik', 'G4 -1.01', 5, 2),\n" +
"('A', NULL, 1, 'Nolting', 'Praktische Informatik', 'G4 -1.01', 5, 3),\n" +
"('A', NULL, 1, 'Nolting', 'Praktische Informatik - Übungen', 'G4 -1.01', 5, 4),\n" +
"('A', NULL, 1, 'Nolting', 'Praktische Informatik - Übungen', 'G4 -1.01', 5, 5),\n" +
"('A', NULL, 4, 'Wurscher', 'Vergrößernde Sehhilfen', 'G4 0.02', 2, 1),\n" +
"('A', NULL, 6, 'Nagl', 'Unternehmensf. /  strat. Planung', 'G4 2.10', 5, 3),\n" +
"('AH', NULL, 6, 'Nagl', 'Unternehmensf. /  strat. Planung', 'G4 2.10', 5, 3),\n" +
"('F', NULL, 3, 'Holzwarth', 'Konstruktionselemente 3', 'G1 1.32', 4, 4),\n" +
"('FR', NULL, 6, 'Holzwarth', 'Konstruktionselemente 3', 'G1 1.32', 4, 4),\n" +
"('A', NULL, 6, 'Nagl', 'Rechnungswesen', 'G4 2.10', 5, 2),\n" +
"('AH', NULL, 6, 'Nagl', 'Rechnungswesen', 'G4 2.10', 5, 2),\n" +
"('A', NULL, 6, 'Nagl', 'Unternehmensplanspiel', 'G4 2.10', 5, 1),\n" +
"('AH', NULL, 6, 'Nagl', 'Unternehmensplanspiel', 'G4 2.10', 5, 1),\n" +
"('F', NULL, 3, 'Holzwarth', 'Mechatronische Baugruppen / Getriebelehre', 'G1 1.32', 1, 2),\n" +
"('FR', NULL, 6, 'Holzwarth', 'Mechatronische Baugruppen / Getriebelehre', 'G1 1.32', 1, 2),\n" +
"('A', NULL, 6, 'Rath Heinrich', 'Berufspädagogik', 'G4 2.10', 5, 5),\n" +
"('AH', NULL, 6, 'Rath Heinrich', 'Berufspädagogik', 'G4 2.10', 5, 5),\n" +
"('F', NULL, 3, 'Holzwarth', 'Mechatronische Baugruppen / Getriebelehre', 'G1 0.20', 2, 5),\n" +
"('FR', NULL, 6, 'Holzwarth', 'Mechatronische Baugruppen / Getriebelehre', 'G1 0.20', 2, 5),\n" +
"('AH', NULL, 6, 'Kreikemeier', 'Hörgeräte 2', 'G4 0.02', 1, 5),\n" +
"('F', NULL, 3, 'Holzwarth', 'Geometrische Messtechnik 1', 'AH -1.01', 1, 1),\n" +
"('F', NULL, 3, 'Holzwarth', 'Geometrische Messtechnik 1', 'G1 1.32', 4, 5),\n" +
"('FR', NULL, 6, 'Holzwarth', 'Geometrische Messtechnik 1', 'AH -1.01', 1, 1),\n" +
"('FR', NULL, 6, 'Holzwarth', 'Geometrische Messtechnik 1', 'G1 1.32', 4, 5),\n" +
"('F', NULL, 3, 'Schmitt', 'Statik und Elastomechanik', 'G1 0.20', 2, 2),\n" +
"('F', NULL, 3, 'Schmitt', 'Statik und Elastomechanik', 'G1 0.20', 3, 5),\n" +
"('GF', NULL, 3, 'Schmitt', 'Statik und Elastomechanik', 'G1 0.20', 2, 2),\n" +
"('GF', NULL, 3, 'Schmitt', 'Statik und Elastomechanik', 'G1 0.20', 3, 5),\n" +
"('MekA', NULL, 3, 'Schmitt', 'Statik und Elastomechanik', 'G1 0.20', 2, 2),\n" +
"('MekA', NULL, 3, 'Schmitt', 'Statik und Elastomechanik', 'G1 0.20', 3, 5),\n" +
"('F', NULL, 3, 'Schmitt', 'Kinematik / Kinetik', 'G1 0.22', 3, 2),\n" +
"('GF', NULL, 3, 'Schmitt', 'Kinematik / Kinetik', 'G1 0.22', 3, 2),\n" +
"('MekA', NULL, 3, 'Schmitt', 'Kinematik / Kinetik', 'G1 0.22', 3, 2),\n" +
"('F', NULL, 3, 'Holzwarth', 'Geometrische Messtechnik 1 Labor', 'G1 -1.22', 1, 3),\n" +
"('FR', NULL, 6, 'Holzwarth', 'Geometrische Messtechnik 1 Labor', 'G1 -1.22', 1, 3),\n" +
"('F', NULL, 3, 'Kazi', 'Aktorik Grundlagen', 'G1 0.01', 1, 4),\n" +
"('F', NULL, 3, 'Kazi', 'Aktorik Grundlagen', 'G1 0.01', 5, 2),\n" +
"('GE', NULL, 6, 'Kazi', 'Aktorik Grundlagen', 'G1 0.01', 1, 4),\n" +
"('GE', NULL, 6, 'Kazi', 'Aktorik Grundlagen', 'G1 0.01', 5, 2),\n" +
"('MekA', NULL, 3, 'Kazi', 'Aktorik Grundlagen', 'G1 0.01', 1, 4),\n" +
"('MekA', NULL, 3, 'Kazi', 'Aktorik Grundlagen', 'G1 0.01', 5, 2),\n" +
"('F', NULL, 4, 'Berger (F)', 'Rapid Manufacturing', 'G1 0.20', 1, 2),\n" +
"('MekA', NULL, 3, 'Berger (F)', 'Rapid Manufacturing', 'G1 0.20', 1, 2),\n" +
"('F', NULL, 4, 'Höfig', 'Systematisches Konstruieren / Lean Development', 'G1 0.01', 2, 1),\n" +
"('F', NULL, 4, 'Höfig', 'Systematisches Konstruieren / Lean Development', 'G1 0.01', 4, 1),\n" +
"('FR', NULL, 6, 'Höfig', 'Systematisches Konstruieren / Lean Development', 'G1 0.01', 2, 1),\n" +
"('FR', NULL, 6, 'Höfig', 'Systematisches Konstruieren / Lean Development', 'G1 0.01', 4, 1),\n" +
"('MekA', NULL, 3, 'Höfig', 'Systematisches Konstruieren / Lean Development', 'G1 0.01', 2, 1),\n" +
"('MekA', NULL, 3, 'Höfig', 'Systematisches Konstruieren / Lean Development', 'G1 0.01', 4, 1),\n" +
"('F', NULL, 2, 'Glaser', 'Fertigungstechnik 2', 'G1 1.32', 4, 3),\n" +
"('FR', NULL, 4, 'Glaser', 'Fertigungstechnik 2', 'G1 1.32', 4, 3),\n" +
"('F', NULL, 4, 'Höfig', 'Product Lifecycle Management (PLM)', 'G1 1.28', 1, 5),\n" +
"('FR', NULL, 6, 'Höfig', 'Product Lifecycle Management (PLM)', 'G1 1.28', 1, 5),\n" +
"('O', NULL, 6, 'Hahn, Wagner (O), Krapp', 'Messtechnik der Glasfaserübertragung', 'G1 2.27, G1 0.21', 1, 4),\n" +
"('AH', NULL, 6, 'Heller, Kreikemeier', 'Praktikum Hörgeräte 2', 'G4 -1.05', 2, 4),\n" +
"('O', NULL, 4, 'Piper, Wagner (O)', 'Physikalische Optik mit Labor', 'G1 0.21', 1, 2),\n" +
"('O', NULL, 4, 'Piper, Wagner (O)', 'Physikalische Optik mit Labor', 'G1 0.06', 1, 3),\n" +
"('O', NULL, 6, 'Hahn, Wagner (O), Krapp', 'Messtechnik der Glasfaserübertragung', 'G1 2.27, G1 0.21', 1, 5),\n" +
"('MekA', NULL, 3, 'Höfig', 'Product Lifecycle Management (PLM)', 'G1 1.28', 1, 5),\n" +
"('FR', NULL, 3, 'Finkbeiner', 'Strukturieren mit XML', 'G1 1.28', 4, 1),\n" +
"('FR', NULL, 3, 'Finkbeiner', 'Strukturieren mit XML', 'G1 1.28', 4, 2),\n" +
"('F', NULL, 4, 'Berger (F)', 'Labor Präzisions- und Mikrofertigung', 'G1 0.01', 4, 3),\n" +
"('FR', NULL, 7, 'Berger (F)', 'Labor Präzisions- und Mikrofertigung', 'G1 0.01', 4, 3),\n" +
"('FR', NULL, 4, 'Starkmann', 'Single Source Publishing', 'G1 1.01', 5, 3),\n" +
"('F', NULL, 4, 'Kazi', 'Sensorik Grundlagen', 'G1 1.32', 1, 1),\n" +
"('F', NULL, 4, 'Kazi', 'Sensorik Grundlagen', 'G1 1.32', 4, 2),\n" +
"('F', NULL, 4, 'Kazi', 'Sensorik Grundlagen', 'G1 1.32', 5, 1),\n" +
"('FR', NULL, 4, 'Kazi', 'Sensorik Grundlagen', 'G1 1.32', 1, 1),\n" +
"('FR', NULL, 4, 'Kazi', 'Sensorik Grundlagen', 'G1 1.32', 4, 2),\n" +
"('FR', NULL, 4, 'Kazi', 'Sensorik Grundlagen', 'G1 1.32', 5, 1),\n" +
"('GE', NULL, 4, 'Kazi', 'Sensorik Grundlagen', 'G1 1.32', 1, 1),\n" +
"('GE', NULL, 4, 'Kazi', 'Sensorik Grundlagen', 'G1 1.32', 4, 2),\n" +
"('GE', NULL, 4, 'Kazi', 'Sensorik Grundlagen', 'G1 1.32', 5, 1),\n" +
"('MekA', NULL, 3, 'Kazi', 'Sensorik Grundlagen', 'G1 1.32', 1, 1),\n" +
"('MekA', NULL, 3, 'Kazi', 'Sensorik Grundlagen', 'G1 1.32', 4, 2),\n" +
"('MekA', NULL, 3, 'Kazi', 'Sensorik Grundlagen', 'G1 1.32', 5, 1),\n" +
"('FR', NULL, 4, 'Starkmann', 'Redaktionssysteme', 'G1 1.29', 5, 4),\n" +
"('FR', NULL, 4, 'Starkmann', 'Redaktionssysteme', 'G1 1.29', 5, 5),\n" +
"('F', NULL, 4, 'Bälder', 'Betriebswirtschaftliche Grundlagen', 'G1 1.01', 4, 4),\n" +
"('FR', NULL, 4, 'Bälder', 'Betriebswirtschaftliche Grundlagen', 'G1 1.01', 4, 4),\n" +
"('GE', NULL, 2, 'Liebschner', 'Energieerzeugung', 'G1 1.23', 1, 4),\n" +
"('F', NULL, 6, 'Liebschner', 'Energieerzeugung', 'G1 1.23', 1, 4),\n" +
"('F', NULL, 7, 'Liebschner', 'Energieerzeugung', 'G1 1.23', 1, 4),\n" +
"('MekA', NULL, 5, 'Liebschner', 'Energieerzeugung', 'G1 1.23', 1, 4),\n" +
"('A', NULL, 6, 'Liebhäusser', 'W-Projekt-Refraktion', 'G4 0.09', 2, 4),\n" +
"('A', NULL, 6, 'Liebhäusser', 'W-Projekt-Refraktion', 'G4 0.09', 2, 5),\n" +
"('A', NULL, 6, '?', 'W-Sprachkurse Spanisch A1', NULL, 2, 4),\n" +
"('A', NULL, 6, '?', 'W-Sprachkurse Spanisch A1', NULL, 2, 5),\n" +
"('A', NULL, 7, '?', 'W-Sprachkurse Spanisch A1', NULL, 2, 4),\n" +
"('A', NULL, 7, '?', 'W-Sprachkurse Spanisch A1', NULL, 2, 5),\n" +
"('FR', NULL, 3, 'Holzwarth', 'Konstruktionselemente 2 Übung', 'G1 1.32', 3, 3),\n" +
"('GF', NULL, 3, 'Holzwarth', 'Konstruktionselemente 2 Übung', 'G1 1.32', 3, 3),\n" +
"('F', NULL, 2, 'Holzwarth', 'Konstruktionselemente 2 Übung', 'G1 1.32', 3, 3),\n" +
"('M', 'K', 6, 'Gretzschel', 'Elektromobilität', 'AH 1.01', 2, 1),\n" +
"('M', 'K', 6, 'Gretzschel', 'Elektromobilität', 'AH 1.01', 2, 2),\n" +
"('P', NULL, 7, 'Gretzschel', 'Elektromobilität', 'AH 1.01', 2, 1),\n" +
"('P', NULL, 7, 'Gretzschel', 'Elektromobilität', 'AH 1.01', 2, 2),\n" +
"('P', NULL, 6, 'Gretzschel', 'Elektromobilität', 'AH 1.01', 2, 1),\n" +
"('P', NULL, 6, 'Gretzschel', 'Elektromobilität', 'AH 1.01', 2, 2),\n" +
"('O', NULL, 2, 'Schneider (O)', 'Informatik für Optik und Elektronik', 'G1 1.01', 3, 2),\n" +
"('B', NULL, 7, 'Radtke', 'Marketingforschung', '0212', 4, 2),\n" +
"('B', NULL, 7, 'Radtke', 'Marketingforschung', '0212', 4, 3),\n" +
"('GE', NULL, 2, 'Liebschner', 'Energieübertragung', 'G1 0.21', 4, 2),\n" +
"('F', NULL, 6, 'Liebschner', 'Energieübertragung', 'G1 0.21', 4, 2),\n" +
"('F', NULL, 7, 'Liebschner', 'Energieübertragung', 'G1 0.21', 4, 2),\n" +
"('MekA', NULL, 5, 'Liebschner', 'Energieübertragung', 'G1 0.21', 4, 2),\n" +
"('GE', NULL, 4, 'Schmidt H.', 'Installationstechnik und Schutzmaßnahmen', 'G1 1.20', 5, 3),\n" +
"('F', NULL, 6, 'Schmidt H.', 'Installationstechnik und Schutzmaßnahmen', 'G1 1.20', 5, 3),\n" +
"('F', NULL, 7, 'Schmidt H.', 'Installationstechnik und Schutzmaßnahmen', 'G1 1.20', 5, 3),\n" +
"('MekA', NULL, 5, 'Schmidt H.', 'Installationstechnik und Schutzmaßnahmen', 'G1 1.20', 5, 3),\n" +
"('F', NULL, 1, 'Hörmann', 'Programmierübungen Gruppe A', 'G1 1.28', 2, 1),\n" +
"('MekA', NULL, 1, 'Hörmann', 'Programmierübungen Gruppe A', 'G1 1.28', 2, 1),\n" +
"('GE', NULL, 2, 'Glunk', 'Übungen Elektrizität / Magnetismus', 'G1 0.21', 3, 6),\n" +
"('GF', NULL, 3, 'Glunk', 'Übungen Elektrizität / Magnetismus', 'G1 0.21', 3, 6),\n" +
"('F', NULL, 4, 'Baur J.', 'Systemsimulation mit Matlab-Simulink', 'G1 1.28', 3, 3),\n" +
"('GE', NULL, 6, 'Baur J.', 'Systemsimulation mit Matlab-Simulink', 'G1 1.28', 3, 3),\n" +
"('GE', NULL, 4, 'Barth A.', 'Einführung Fachdidaktik Energie- / Automatisierungstechnik', 'G1 1.20', 2, 6),\n" +
"('FR', NULL, 6, 'Barth A.', 'Einführung Fachdidaktik Energie- / Automatisierungstechnik', 'G1 1.20', 2, 6),\n" +
"('FR', NULL, 7, 'Barth A.', 'Einführung Fachdidaktik Energie- / Automatisierungstechnik', 'G1 1.20', 2, 6),\n" +
"('GE', NULL, 4, 'Schmidt H.', 'Installationstechnik und Schutzmaßnahmen', 'G1 1.20', 5, 2),\n" +
"('F', NULL, 6, 'Schmidt H.', 'Installationstechnik und Schutzmaßnahmen', 'G1 1.20', 5, 2),\n" +
"('F', NULL, 7, 'Schmidt H.', 'Installationstechnik und Schutzmaßnahmen', 'G1 1.20', 5, 2),\n" +
"('MekA', NULL, 5, 'Schmidt H.', 'Installationstechnik und Schutzmaßnahmen', 'G1 1.20', 5, 2),\n" +
"('GE', NULL, 4, 'Barth A.', 'Labor Energie- / Automatisierungstechnik', 'G1 1.20', 3, 5),\n" +
"('FR', NULL, 6, 'Barth A.', 'Labor Energie- / Automatisierungstechnik', 'G1 1.20', 3, 5),\n" +
"('FR', NULL, 7, 'Barth A.', 'Labor Energie- / Automatisierungstechnik', 'G1 1.20', 3, 5),\n" +
"('O', NULL, 4, 'Dittmar', 'Praktikum Präsentation', 'G1 1.01', 1, 1),\n" +
"('O', NULL, 6, 'Dittmar', 'Praktikum Präsentation', 'G1 1.01', 1, 1),\n" +
"('GE', NULL, 4, 'Schmidt H.', 'Labor Gebäudetechnik', 'G1 1.20', 4, 3),\n" +
"('F', NULL, 6, 'Schmidt H.', 'Labor Gebäudetechnik', 'G1 1.20', 4, 3),\n" +
"('F', NULL, 7, 'Schmidt H.', 'Labor Gebäudetechnik', 'G1 1.20', 4, 3),\n" +
"('MekA', NULL, 5, 'Schmidt H.', 'Labor Gebäudetechnik', 'G1 1.20', 4, 3),\n" +
"('P', NULL, 7, 'Gärtner, Schupbach', 'Industrialdesign Projekt', '0213', 5, 3),\n" +
"('P', NULL, 7, 'Gärtner, Schupbach', 'Industrialdesign Projekt', '0213', 5, 4),\n" +
"('FR', NULL, 4, 'Höfig', '3D-CAD-Anwendungen', '0289', 2, 2),\n" +
"('I', 'F', 6, 'Peter', 'Projektseminar Steuern I', '0222', 2, 4),\n" +
"('I', 'F', 6, 'Peter', 'Projektseminar Steuern I', '0222', 2, 5),\n" +
"('I', 'F', 6, 'Scheuermann', 'Corporate Finance', '0222', 1, 5),\n" +
"('I', 'F', 6, 'Scheuermann', 'Corporate Finance', '0222', 1, 6),\n" +
"('I', 'F', 6, 'Heyd', 'International Accounting', '0201', 4, 2),\n" +
"('I', 'F', 6, 'Heyd', 'International Accounting', '0201', 4, 3),\n" +
"('I', 'F', 6, 'Kurz J.', 'Übung Buchführung und Bilanzierung', '0701', 4, 1),\n" +
"('GMF', NULL, NULL, 'Berger (F)', 'RPD Labor', 'G1 0.29', 4, 6),\n" +
"('FR', NULL, 4, 'Kazi', 'Mechatronisches Labor - Sensorik', 'G1 1.26', 3, 2),\n" +
"('FR', NULL, 4, 'Kazi', 'Mechatronisches Labor - Sensorik', 'G1 1.26', 3, 3),\n" +
"('FR', NULL, 4, 'Kazi', 'Mechatronisches Labor - Sensorik', 'G1 1.26', 3, 4),\n" +
"('I', NULL, 4, 'Butz', 'Tutorium Steuern', 'AH 1.01', 1, 1),\n" +
"('I', NULL, 4, 'Rieg', 'Controlling', '0258', 2, 4),\n" +
"('I', NULL, 4, 'Rieg', 'Controlling', '0258', 2, 5),\n" +
"('IN', 'WI', 4, 'Obel', 'CRM', 'G2 2.01~G2 1.28', 5, 4),\n" +
"('IN', 'WI', 4, 'Obel', 'CRM', 'G2 2.01~G2 1.28', 5, 5),\n" +
"('IN', 'WI', 4, 'Obel', 'CRM', 'G2 2.01~G2 1.28', 5, 6),\n" +
"('IN', 'WI', 4, 'Obel', 'CRM', 'G2 1.01~G2 1.28', 6, 1),\n" +
"('IN', 'WI', 4, 'Obel', 'CRM', 'G2 1.01~G2 1.28', 6, 2),\n" +
"('IN', 'WI', 4, 'Obel', 'CRM', 'G2 1.01~G2 1.28', 6, 3),\n" +
"('GE', NULL, 2, 'Glunk', 'Grundlagen Elektrizität / Magnetismus', 'G1 1.20', 3, 4),\n" +
"('GF', NULL, 3, 'Glunk', 'Grundlagen Elektrizität / Magnetismus', 'G1 1.20', 3, 4),\n" +
"('PH', NULL, NULL, 'Dörband', 'Interferometry', 'G1 1.23', 1, 1),\n" +
"('PH', NULL, NULL, 'Dörband', 'Interferometry', 'G1 1.23', 2, 1),\n" +
"('ZEO', NULL, NULL, 'Ruf Viola', 'Grammar Refresher B2', '0212', 3, 6),\n" +
"('ZEO', NULL, NULL, 'Düwel', 'Communication in English B2', '0120', 3, 4),\n" +
"('MP', NULL, 3, 'Heilmann', 'Kostenrechnung und Rechnungswesen', 'AH -1.01', 4, 1),\n" +
"('MP', NULL, 3, 'Heilmann', 'Kostenrechnung und Rechnungswesen', 'AH -1.01', 4, 2),\n" +
"('MW', NULL, 3, 'Heilmann', 'Kostenrechnung und Rechnungswesen', 'AH -1.01', 4, 1),\n" +
"('MW', NULL, 3, 'Heilmann', 'Kostenrechnung und Rechnungswesen', 'AH -1.01', 4, 2),\n" +
"('IN', 'IS, MI, SE', 2, 'Werthebach', 'Objektorientierte Programmierung', 'G2 1.01', 1, 4),\n" +
"('IN', 'IS, MI, SE', 2, 'Werthebach', 'Objektorientierte Programmierung', 'G2 2.41', 2, 3),\n" +
"('IN', 'IS, MI, SE', 2, 'Heinlein', 'Analysis und Lineare Algebra', 'G2 2.01', 1, 3),\n" +
"('IN', 'IS, MI, SE', 2, 'Heinlein', 'Analysis und Lineare Algebra', 'G2 0.21', 4, 2),\n" +
"('IN', 'IS, SE, MI', 3, 'Heinlein', 'Programmierprojekt', 'G2 0.01', 1, 4),\n" +
"('IN', 'IS, MI, SE, WI', 4, 'Brandt', 'IT-Recht', 'G2 2.01', 5, 4),\n" +
"('IN', 'IS, MI, SE, WI', 4, 'Brandt', 'IT-Recht', 'G2 2.01', 5, 5),\n" +
"('IN', 'IS, MI, SE, WI', 4, 'Brandt', 'IT-Recht', 'G2 2.01', 5, 6),\n" +
"('IN', 'IS, MI, SE, WI', 4, 'Brandt', 'IT-Recht', 'G2 1.01', 6, 1),\n" +
"('IN', 'IS, MI, SE, WI', 4, 'Brandt', 'IT-Recht', 'G2 1.01', 6, 2),\n" +
"('IN', 'IS, MI, SE, WI', 4, 'Brandt', 'IT-Recht', 'G2 1.01', 6, 3),\n" +
"('IN', 'IS, SE, MI, WI', 4, 'Dietrich', 'Software Engineering', 'G2 0.37', 2, 3),\n" +
"('IN', 'IS, SE, MI, WI', 4, 'Dietrich', 'Software Engineering', 'G2 1.44', 4, 3),\n" +
"('MP', NULL, 3, 'Haag', 'Konstruktion 3', '0103, 0290', 5, 4),\n" +
"('MP', NULL, 3, 'Haag', 'Konstruktion 3', '0103, 0290', 5, 5),\n" +
"('ET', NULL, 1, 'Bartel', 'Lern- u. Arbeitstechniken', 'G2 0.01, G2 1.01', 5, 1),\n" +
"('ET', NULL, 1, 'Bartel', 'Lern- u. Arbeitstechniken', 'G2 0.01, G2 2.33', 4, 2),\n" +
"('IN', 'IS, MI, SE, WI', 4, 'Klauck, Wiese', 'Seminar', 'G2 1.44', 3, 5),\n" +
"('E', 'IFE', 6, 'Hofmann (E)', 'Energieeffizienz', 'G2 0.37', 4, 4),\n" +
"('E', 'IFE', 6, 'Hofmann (E)', 'Energieeffizienz', 'G2 2.41', 5, 3),\n" +
"('E', 'IFE', 7, 'Hofmann (E)', 'Energieeffizienz', 'G2 0.37', 4, 4),\n" +
"('E', 'IFE', 7, 'Hofmann (E)', 'Energieeffizienz', 'G2 2.41', 5, 3),\n" +
"('ET', 'EE', 6, 'Hofmann (E)', 'Energieeffizienz', 'G2 0.37', 4, 4),\n" +
"('ET', 'EE', 6, 'Hofmann (E)', 'Energieeffizienz', 'G2 2.41', 5, 3),\n" +
"('M', 'R', 7, 'Hofmann (E)', 'Energieeffizienz', 'G2 0.37', 4, 4),\n" +
"('M', 'R', 7, 'Hofmann (E)', 'Energieeffizienz', 'G2 2.41', 5, 3),\n" +
"('M', NULL, 4, 'Zehnder', 'Qualitätsmanagement', '0212', 1, 3),\n" +
"('VI', NULL, 1, 'Schrader', 'Grundlagen der Betriebswirtschaft', '0121', 4, 5),\n" +
"('VI', NULL, 1, 'Schrader', 'Grundlagen der Betriebswirtschaft', '0121', 4, 6),\n" +
"('V', NULL, 1, 'Wegmann', 'Statik', 'AH -1.01', 3, 3),\n" +
"('V', NULL, 1, 'Wegmann', 'Statik', 'AH 0.01', 4, 2),\n" +
"('VMG', NULL, 1, 'Wegmann', 'Statik', 'AH -1.01', 3, 3),\n" +
"('VMG', NULL, 1, 'Wegmann', 'Statik', 'AH 0.01', 4, 2),\n" +
"('VMM', NULL, 1, 'Wegmann', 'Statik', 'AH -1.01', 3, 3),\n" +
"('VMM', NULL, 1, 'Wegmann', 'Statik', 'AH 0.01', 4, 2),\n" +
"('VI', NULL, 1, 'Wegmann', 'Statik', 'AH -1.01', 3, 3),\n" +
"('VI', NULL, 1, 'Wegmann', 'Statik', 'AH 0.01', 4, 2),\n" +
"('P', NULL, 3, 'Rimkus', 'FEM', '0290', 3, 4),\n" +
"('P', NULL, 3, 'Rimkus', 'FEM', '0290', 3, 5),\n" +
"('P', NULL, 3, 'Rimkus', 'FEM', '0289', 4, 1),\n" +
"('P', NULL, 3, 'Rimkus', 'FEM', '0289', 4, 2),\n" +
"('CCS', NULL, 2, 'Dietrich', 'Echtzeitsysteme', 'G2 0.30', 1, 4),\n" +
"('A', NULL, 1, 'Nolting', 'Geometrische Optik 1', '0124', 4, 3),\n" +
"('O', NULL, 6, 'Bauer (O)', 'Projekte', 'G1 0.20', 5, 5),\n" +
"('AMM', NULL, 2, 'Knoblauch', 'Werkzeuge des wissenschaftlichen Arbeitens in FuE-Projekten', '0105', 3, 5),\n" +
"('AMM', NULL, 1, 'Knoblauch', 'Werkzeuge des wissenschaftlichen Arbeitens in FuE-Projekten', '0105', 3, 5),\n" +
"('A', NULL, 3, 'Nagl', 'W-Kommunikationstraining', 'G4 -1.01', 4, 4),\n" +
"('A', NULL, 3, 'Nagl', 'W-Kommunikationstraining', 'G4 -1.01', 4, 5),\n" +
"('AMM', NULL, 2, 'Diverse', 'Applied Materials and Manufacturing Technology 1', '0105', 3, 6),\n" +
"('AMM', NULL, 1, 'Diverse', 'Applied Materials and Manufacturing Technology 1', '0105', 3, 6),\n" +
"('F', NULL, 3, 'Berger (F)', 'Steuerungstechnik', 'G1 1.28', 3, 4),\n" +
"('GE', NULL, 4, 'Berger (F)', 'Steuerungstechnik', 'G1 1.28', 3, 4),\n" +
"('FR', NULL, 6, 'Berger (F)', 'Steuerungstechnik', 'G1 1.28', 3, 4),\n" +
"('A', NULL, 2, 'Michels', 'Prakt. Geometr. Optik', 'G4 0.02', 2, 5),\n" +
"('AH', NULL, 2, 'Michels', 'Prakt. Geometr. Optik', 'G4 0.02', 2, 5);" 
,"INSERT INTO g02_vorlesungsplan (stg, schwerpunkt, semester, dozent, kurs, raum, tagnr,  zeitblocknr) VALUES\n" +
            "('A', NULL, 1, 'Paffrath', 'Mathematik&Technologie', '0124', 1, 1),\n" +
"('A', NULL, 1, 'Paffrath', 'Mathematik&Technologie', '0124', 1, 2),\n" +
"('IN', 'SE', 4, 'Dietrich', 'Komponentenbasierte Softwaret.', 'G2 0.30, G2 1.01', 3, 4),\n" +
"('IN', 'SE', 4, 'Dietrich', 'Komponentenbasierte Softwaret.', 'G2 0.30, G2 1.44', 1, 5),\n" +
"('G', 'AH', 1, 'Baumbach, Nolting, Buser, Schiefer, Kirschkamp, Holschbach, Limberger, Kreikemeier, Paffrath', 'Studiengangsitzung', 'G4 0.09', 3, 5),\n" +
"('G', 'AH', 1, 'Baumbach, Nolting, Buser, Schiefer, Kirschkamp, Holschbach, Limberger, Kreikemeier, Paffrath', 'Studiengangsitzung', 'G4 0.09', 3, 6),\n" +
"('V', NULL, 1, 'Bingel, Jonke, Hildebrandt', 'Grundlagen der Vorbehandlung - Verfahrensüberblick', '0215', 5, 2),\n" +
"('V', NULL, 1, 'Bingel, Jonke, Hildebrandt', 'Grundlagen der Vorbehandlung - Verfahrensüberblick', '0215', 5, 3),\n" +
"('ABC', NULL, 2, 'Neusüß', 'Oberflächenanalytik', '0120', 5, 2),\n" +
"('P', NULL, 1, 'Maier U.', 'Mathematik I Tutorium', '0234', 1, 4),\n" +
"('ABC', NULL, 2, 'Engel', 'Anorganische Strukturanalytik', '0231', 5, 4),\n" +
"('ABC', NULL, 2, 'Engel', 'Anorganische Strukturanalytik', '0231', 5, 5),\n" +
"('A', NULL, 2, 'Schiefer', 'Refraktion', 'G4 0.02', 3, 3),\n" +
"('AH', NULL, 2, 'Schiefer', 'Refraktion', 'G4 0.02', 3, 3),\n" +
"('A', NULL, 2, 'Lanzinger', 'Praktikum Refraktion', 'G4 1.07', 1, 2),\n" +
"('A', NULL, 2, 'Lanzinger', 'Praktikum Refraktion', 'G4 1.07', 1, 3),\n" +
"('A', NULL, 2, 'Lanzinger', 'Praktikum Refraktion', 'G4 1.07', 2, 2),\n" +
"('A', NULL, 2, 'Lanzinger', 'Praktikum Refraktion', 'G4 1.07', 2, 3),\n" +
"('AH', NULL, 2, 'Lanzinger', 'Praktikum Refraktion', 'G4 1.07', 1, 2),\n" +
"('AH', NULL, 2, 'Lanzinger', 'Praktikum Refraktion', 'G4 1.07', 1, 3),\n" +
"('AH', NULL, 2, 'Lanzinger', 'Praktikum Refraktion', 'G4 1.07', 2, 2),\n" +
"('AH', NULL, 2, 'Lanzinger', 'Praktikum Refraktion', 'G4 1.07', 2, 3),\n" +
"('A', NULL, 4, 'Baumbach', 'Optik u. Technik der Brille 1', 'G4 -1.01', 2, 3),\n" +
"('AH', NULL, 4, 'Baumbach', 'Optik u. Technik der Brille 1', 'G4 -1.01', 2, 3),\n" +
"('A', NULL, 2, 'Kaier', 'Fallstudie Projekt', 'G4 2.10', 6, 1),\n" +
"('A', NULL, 2, 'Kaier', 'Fallstudie Projekt', 'G4 2.10', 6, 2),\n" +
"('A', NULL, 2, 'Kaier', 'Fallstudie Projekt', 'G4 2.10', 6, 3),\n" +
"('AH', NULL, 2, 'Kaier', 'Fallstudie Projekt', 'G4 2.10', 6, 1),\n" +
"('AH', NULL, 2, 'Kaier', 'Fallstudie Projekt', 'G4 2.10', 6, 2),\n" +
"('AH', NULL, 2, 'Kaier', 'Fallstudie Projekt', 'G4 2.10', 6, 3),\n" +
"('A', NULL, 3, 'Michels', 'Praktikum Kontaktlinse 1', 'G4 1.02', 1, 2),\n" +
"('A', NULL, 3, 'Michels', 'Praktikum Kontaktlinse 1', 'G4 1.02', 1, 3),\n" +
"('A', NULL, 3, 'Michels', 'Praktikum Kontaktlinse 1', 'G4 1.02', 5, 2),\n" +
"('A', NULL, 3, 'Michels', 'Praktikum Kontaktlinse 1', 'G4 1.02', 5, 3),\n" +
"('A', NULL, 3, 'Buser', 'Objektive / Subjektive Refraktion', 'G4 0.02', 3, 4),\n" +
"('A', NULL, 4, 'Baumbach', 'Optik u. Technik der Brille 1', 'G4 0.02', 1, 3),\n" +
"('AH', NULL, 4, 'Baumbach', 'Optik u. Technik der Brille 1', 'G4 0.02', 1, 3),\n" +
"('A', NULL, 4, 'Lanzinger', 'Praktikum Brille', 'G4 1.01', 2, 4),\n" +
"('AH', NULL, 4, 'Lanzinger', 'Praktikum Brille', 'G4 1.01', 2, 4),\n" +
"('A', NULL, 6, 'Nagl', 'W-Augenoptische Systemsoftw.', 'G4 -1.01', 4, 7),\n" +
"('AH', NULL, 6, 'Nagl', 'W-Augenoptische Systemsoftw.', 'G4 -1.01', 4, 7),\n" +
"('A', NULL, 7, 'Nagl', 'W-Augenoptische Systemsoftw.', 'G4 -1.01', 4, 7),\n" +
"('A', NULL, 6, 'Baumbach', 'W-Technische Optik', 'G4 -1.01', 1, 4),\n" +
"('A', NULL, 6, 'Baumbach', 'W-Technische Optik', 'G4 -1.01', 1, 5),\n" +
"('A', NULL, 7, 'Baumbach', 'W-Technische Optik', 'G4 -1.01', 1, 4),\n" +
"('A', NULL, 7, 'Baumbach', 'W-Technische Optik', 'G4 -1.01', 1, 5),\n" +
"('A', NULL, 6, '?', 'Berufspädagogik-Unterw.', 'G4 2.10', 4, 4),\n" +
"('A', NULL, 6, '?', 'Berufspädagogik-Unterw.', 'G4 2.10', 4, 5),\n" +
"('AH', NULL, 6, '?', 'Berufspädagogik-Unterw.', 'G4 2.10', 4, 4),\n" +
"('AH', NULL, 6, '?', 'Berufspädagogik-Unterw.', 'G4 2.10', 4, 5),\n" +
"('A', NULL, 2, 'Brändle', 'Projektmanagement', 'G4 2.10', 5, 6),\n" +
"('AH', NULL, 2, 'Brändle', 'Projektmanagement', 'G4 2.10', 5, 6),\n" +
"('A', NULL, 2, 'Nagl', 'Business Management', 'G4 2.10', 5, 7),\n" +
"('AH', NULL, 2, 'Nagl', 'Business Management', 'G4 2.10', 5, 7),\n" +
"('B', NULL, 7, 'May', 'Management Finanz- und Rechnungswesen', '0213', 4, 4),\n" +
"('B', NULL, 7, 'May', 'Management Finanz- und Rechnungswesen', '0213', 4, 5),\n" +
"('M', NULL, 3, 'Alpers', 'Informatik', '0202', 2, 1),\n" +
"('O', NULL, 2, 'Börret', 'Werkstoffe der Optik und Elektronik', 'G1 0.22', 4, 2),\n" +
"('O', NULL, 2, 'Börret', 'Werkstoffe der Optik und Elektronik', 'G1 0.22', 4, 3),\n" +
"('FOI', NULL, NULL, 'Börret', 'Optical Materials and Manufacturing', 'G1 0.22', 4, 2),\n" +
"('FOI', NULL, NULL, 'Börret', 'Optical Materials and Manufacturing', 'G1 0.22', 4, 3),\n" +
"('I', 'M', 7, 'Gentsch', 'CRM Advanced', '0215', 5, 4),\n" +
"('I', 'M', 7, 'Gentsch', 'CRM Advanced', '0215', 5, 5),\n" +
"('ABC', NULL, 2, 'Flottmann, Neusüß', 'Anorganische Strukturanalytik', '0114', 3, 3),\n" +
"('ABC', NULL, 2, 'Flottmann, Neusüß', 'Anorganische Strukturanalytik', '0121', 5, 1),\n" +
"('B', NULL, 7, 'Ilg, Vogel', 'Personal- und Organisationsma', '0130', 5, 1),\n" +
"('B', NULL, 7, 'Ilg, Vogel', 'Personal- und Organisationsma', '0130', 5, 2),\n" +
"('B', NULL, 7, 'Ilg, Vogel', 'Personal- und Organisationsma', '0130', 5, 3),\n" +
"('B', NULL, 7, 'Ilg, Vogel', 'Personal- und Organisationsma', '0130', 5, 4),\n" +
"('M', NULL, 3, 'Alpers, Zorniger', 'Informatik-Labor', '0273, 0290, 0034', 4, 2),\n" +
"('AH', NULL, 6, 'Buschle, Kreikemeier', 'Praktikum Hörgeräte 2', 'G4 -1.05, G4 -1.20', 1, 6),\n" +
"('I', 'F', 7, 'Peter', 'Internationale Unternehmensbesteuerung', '0126', 1, 4),\n" +
"('I', 'F', 7, 'Peter', 'Internationale Unternehmensbesteuerung', '0126', 1, 5),\n" +
"('I', 'F', 7, 'Rieg', 'Intern. Controlling', '0231', 4, 4),\n" +
"('I', 'F', 7, 'Rieg', 'Intern. Controlling', '0231', 4, 5),\n" +
"('I', 'M', 7, 'Spintig', 'Preismanagement', '0201', 3, 4),\n" +
"('I', 'M', 7, 'Spintig', 'Preismanagement', '0201', 3, 5),\n" +
"('I', 'M', 7, 'Spintig', 'Preismanagement', '0201', 3, 6),\n" +
"('I', 'P', 7, 'Frick G.', 'Personalführung', '0202', 2, 4),\n" +
"('I', 'P', 7, 'Frick G.', 'Personalführung', '0202', 2, 5),\n" +
"('P', NULL, 2, 'Schöppach', 'Festigkeitslehre 2', '0222', 3, 4),\n" +
"('P', NULL, 2, 'Schöppach', 'Festigkeitslehre 2', '0222', 3, 5),\n" +
"('I', 'P', 7, 'Güida', 'Außenwirtschaft', '0203', 4, 4),\n" +
"('I', 'P', 7, 'Güida', 'Außenwirtschaft', '0202', 4, 5),\n" +
"('I', 'P', 7, 'Güida', 'Regionalstudien Lateinamerika', '0203', 4, 4),\n" +
"('I', 'P', 7, 'Güida', 'Regionalstudien Lateinamerika', '0202', 4, 5),\n" +
"('P', NULL, 2, 'Pietzsch', 'Freihandzeichnen 2', '0115', 4, 2),\n" +
"('I', 'P', 7, 'Chung', 'Comparative Business Systems', '0102', 4, 1),\n" +
"('I', 'P', 7, 'Chung', 'Comparative Business Systems', '0132', 4, 2),\n" +
"('P', NULL, 2, 'Weidner', 'Einführung in die virtuelle Produktentwicklung', '0203', 5, 3),\n" +
"('PTC', NULL, 1, 'Frick', 'Polymer Testing', 'AH 1.02', 1, 5),\n" +
"('PTC', NULL, 1, 'Walcher', 'Polymer Physics', '0203', 2, 1),\n" +
"('PTC', NULL, 1, 'Frick', 'Laboratory Polymer Testing', '0121', 2, 4),\n" +
"('PTC', NULL, 1, 'Frick', 'Laboratory Polymer Testing', '0121', 2, 5),\n" +
"('PTC', NULL, 1, 'Seitz', 'Technical English C1', '0202', 3, 3),\n" +
"('PTC', NULL, 1, 'Seitz', 'Technical English C1', '0115', 3, 4),\n" +
"('PTC', NULL, 1, 'Hoffmann J.', 'German A 2', '0202', 2, 2),\n" +
"('PTC', NULL, 1, 'Hoffmann J.', 'German A 2', '0202', 2, 3),\n" +
"('PTC', NULL, 1, 'Hoffmann J.', 'German A 2', '0101', 5, 3),\n" +
"('PTC', NULL, 1, 'Hoffmann J.', 'German A 2', '0101', 5, 4),\n" +
"('PTC', NULL, 1, 'Schlipf M. (M)', 'Polymer Materials', '0273', 4, 5),\n" +
"('PTC', NULL, 1, 'Schlipf M. (M)', 'Polymer Materials', '0273', 4, 6),\n" +
"('PTC', NULL, 1, 'Schlipf M. (M)', 'Polymer Materials', '0273', 5, 1),\n" +
"('PTC', NULL, 1, 'Schlipf M. (M)', 'Polymer Materials', '0273', 5, 2),\n" +
"('LBM', NULL, NULL, 'Schlipf M. (M)', 'Polymer Materials', '0273', 4, 5),\n" +
"('LBM', NULL, NULL, 'Schlipf M. (M)', 'Polymer Materials', '0273', 4, 6),\n" +
"('LBM', NULL, NULL, 'Schlipf M. (M)', 'Polymer Materials', '0273', 5, 1),\n" +
"('LBM', NULL, NULL, 'Schlipf M. (M)', 'Polymer Materials', '0273', 5, 2),\n" +
"('GE', NULL, 6, 'Glunk', 'Mechanik 2 / Wärmelehre 2', 'G1 1.32', 3, 5),\n" +
"('GF', NULL, 7, 'Glunk', 'Mechanik 2 / Wärmelehre 2', 'G1 1.32', 3, 5),\n" +
"('VMG', NULL, 3, 'Goll', 'Digitale Bildverarbeitung in der Mikroskopie', '0185', 1, 4),\n" +
"('O', NULL, 3, 'Schneider (O)', 'Statistik', 'G1 0.01', 1, 1),\n" +
"('O', NULL, 3, 'Schneider (O)', 'Statistik', 'G1 0.01', 1, 2),\n" +
"('FR', NULL, 4, 'Berger (F)', 'Mechatronisches Labor - Fertigung', 'G1 1.32', 4, 1),\n" +
"('VMG', NULL, 7, 'Rimkus', 'FEM-Strukturmechanik', '0289', 1, 2),\n" +
"('VMG', NULL, 7, 'Rimkus', 'FEM-Strukturmechanik', '0289', 1, 3),\n" +
"('VMM', NULL, 7, 'Rimkus', 'FEM-Strukturmechanik', '0289', 1, 2),\n" +
"('VMM', NULL, 7, 'Rimkus', 'FEM-Strukturmechanik', '0289', 1, 3),\n" +
"('F', NULL, 3, 'Kazi', 'Aktorik Grundlagen', 'G1 0.01', 5, 3),\n" +
"('GE', NULL, 6, 'Kazi', 'Aktorik Grundlagen', 'G1 0.01', 5, 3),\n" +
"('MekA', NULL, 3, 'Kazi', 'Aktorik Grundlagen', 'G1 0.01', 5, 3),\n" +
"('F', NULL, 6, 'Baur J.', 'Modellbasierte Softwareentwicklung', 'G1 1.28', 3, 5),\n" +
"('MekA', NULL, 5, 'Baur J.', 'Modellbasierte Softwareentwicklung', 'G1 1.28', 3, 5),\n" +
"('F', NULL, 6, 'Baur J.', 'Embedded Control Systems', 'G1 1.28', 2, 5),\n" +
"('F', NULL, 6, 'Baur J.', 'Embedded Control Systems', 'G1 1.28', 2, 6),\n" +
"('MekA', NULL, 5, 'Baur J.', 'Embedded Control Systems', 'G1 1.28', 2, 5),\n" +
"('MekA', NULL, 5, 'Baur J.', 'Embedded Control Systems', 'G1 1.28', 2, 6),\n" +
"('VMG', NULL, 3, 'Ketzer-Raichle', 'Gefügeinterpretation', '0185', 4, 1),\n" +
"('VMG', NULL, 3, 'Ketzer-Raichle', 'Gefügeinterpretation', '0185', 4, 2),\n" +
"('VMG', NULL, 3, 'Ketzer-Raichle', 'Gefügeinterpretation', '0185', 4, 3),\n" +
"('FR', NULL, 2, 'Ferrano', 'HTML und Web-Technologien', '0013', 3, 4),\n" +
"('F', NULL, 6, 'Berger (F)', 'Ablaufsteuerung', 'G1 0.29', 3, 1),\n" +
"('F', NULL, 6, 'Berger (F)', 'Dezentrale Peripherie', 'G1 0.29', 3, 2),\n" +
"('F', NULL, 4, 'Bälder', 'Kostenrechnung', 'G1 1.01', 4, 5),\n" +
"('FR', NULL, 4, 'Bälder', 'Kostenrechnung', 'G1 1.01', 4, 5),\n" +
"('GE', NULL, 6, 'Glunk', 'Übungen Mechanik 2 / Wärmelehre 2', 'G1 0.06', 4, 3),\n" +
"('GF', NULL, 7, 'Glunk', 'Übungen Mechanik 2 / Wärmelehre 2', 'G1 0.06', 4, 3),\n" +
"('FR', NULL, 6, 'Starkmann', 'Software-Dokumentation und Hilfesysteme', 'G1 1.29', 5, 2),\n" +
"('FR', NULL, 7, 'Starkmann', 'Software-Dokumentation und Hilfesysteme', 'G1 1.29', 5, 2),\n" +
"('FR', NULL, 2, 'Ferrano', 'HTML und Web-Technologien', '0013', 3, 3),\n" +
"('F', NULL, 6, 'Glaser', 'Fehlersichere Systeme', 'G1 0.06', 4, 2),\n" +
"('I', 'F, M, P', 7, 'Ravens', 'Unternehmensstrategie & Fallstudien', '0213', 4, 3),\n" +
"('IN', 'IS, SE, MI', 3, 'Küpper', 'Datenbanksysteme', 'G2 2.41~G2 1.30', 1, 3),\n" +
"('IN', 'IS, SE, MI', 3, 'Küpper', 'Datenbanksysteme', 'G2 0.37~G2 1.30', 3, 2),\n" +
"('IN', 'IS, SE, MI', 2, 'Lecon', 'Algorithmen und Datenstrukturen 1', 'G2 0.37', 5, 2),\n" +
"('P', NULL, 2, 'Reinisch, Weidner', 'PDM-Labor', '0290', 3, 2),\n" +
"('F', NULL, 7, 'Glaser', 'Fehlersichere Systeme', 'G1 0.06', 4, 2),\n" +
"('FR', NULL, 6, 'Gläser', 'Übersetzungsmanagement und Lokalisierung', 'G1 1.29', 5, 3),\n" +
"('FR', NULL, 7, 'Gläser', 'Übersetzungsmanagement und Lokalisierung', 'G1 1.29', 5, 3),\n" +
"('FR', NULL, 6, 'Gläser', 'TMS- / Terminologiesysteme und Textprüfungs-Tools', 'G1 0.21', 5, 4),\n" +
"('FR', NULL, 7, 'Gläser', 'TMS- / Terminologiesysteme und Textprüfungs-Tools', 'G1 0.21', 5, 4),\n" +
"('FR', NULL, 2, 'Wendland', 'Mensch-Computer-Interaktion', 'G1 0.21', 2, 5),\n" +
"('FR', NULL, 2, 'Woisetschläger', 'Satz- und Drucktechnik', '0014', 5, 3),\n" +
"('F', NULL, 6, 'Zellner', 'Arbeitssicherheit', 'G1 1.32', 3, 4),\n" +
"('F', NULL, 7, 'Zellner', 'Arbeitssicherheit', 'G1 1.32', 3, 4),\n" +
"('IST', NULL, NULL, 'Maurer (V)', 'Leading Sales Teams', '0231', 3, 1),\n" +
"('IST', NULL, NULL, 'Maurer (V)', 'Leading Sales Teams', '0231', 3, 2),\n" +
"('IST', NULL, NULL, 'Maurer (V)', 'Leading Sales Teams', '0231', 3, 3),\n" +
"('IST', NULL, NULL, 'Maurer (V)', 'Leading Sales Teams', '0233', 3, 4),\n" +
"('IST', NULL, NULL, 'Maurer (V)', 'Leading Sales Teams', '0233', 3, 5),\n" +
"('IST', NULL, NULL, 'Maurer (V)', 'Leading Sales Teams', '0233', 3, 6),\n" +
"('IST', NULL, NULL, 'Maurer (V)', 'Leading Sales Teams', 'M17', 4, 1),\n" +
"('IST', NULL, NULL, 'Maurer (V)', 'Leading Sales Teams', 'M17', 4, 2),\n" +
"('IST', NULL, NULL, 'Maurer (V)', 'Leading Sales Teams', 'M17', 4, 3),\n" +
"('IST', NULL, NULL, 'Maurer (V)', 'Leading Sales Teams', '0234', 4, 4),\n" +
"('IST', NULL, NULL, 'Maurer (V)', 'Leading Sales Teams', '0234', 4, 5),\n" +
"('IST', NULL, NULL, 'Maurer (V)', 'Leading Sales Teams', '0234', 4, 6),\n" +
"('IST', NULL, NULL, 'Maurer (V)', 'International Sales Structures', '0231', 3, 1),\n" +
"('IST', NULL, NULL, 'Maurer (V)', 'International Sales Structures', '0231', 3, 2),\n" +
"('IST', NULL, NULL, 'Maurer (V)', 'International Sales Structures', '0231', 3, 3),\n" +
"('IST', NULL, NULL, 'Maurer (V)', 'International Sales Structures', '0233', 3, 4),\n" +
"('IST', NULL, NULL, 'Maurer (V)', 'International Sales Structures', '0233', 3, 5),\n" +
"('IST', NULL, NULL, 'Maurer (V)', 'International Sales Structures', '0233', 3, 6),\n" +
"('IST', NULL, NULL, 'Maurer (V)', 'International Sales Structures', 'M17', 4, 1),\n" +
"('IST', NULL, NULL, 'Maurer (V)', 'International Sales Structures', 'M17', 4, 2),\n" +
"('IST', NULL, NULL, 'Maurer (V)', 'International Sales Structures', 'M17', 4, 3),\n" +
"('IST', NULL, NULL, 'Maurer (V)', 'International Sales Structures', '0234', 4, 4),\n" +
"('IST', NULL, NULL, 'Maurer (V)', 'International Sales Structures', '0234', 4, 5),\n" +
"('IST', NULL, NULL, 'Maurer (V)', 'International Sales Structures', '0234', 4, 6),\n" +
"('AM', NULL, 1, 'Buser', 'Wissenschaftl. Methodik', 'G4 -1.01', 1, 2),\n" +
"('F', NULL, 6, 'Hörmann', 'Electronic Circuit Design', 'G1 1.01', 2, 4),\n" +
"('F', NULL, 6, 'Hörmann', 'Electronic Circuit Design', 'G1 1.01', 4, 6),\n" +
"('F', NULL, 7, 'Hörmann', 'Electronic Circuit Design', 'G1 1.01', 2, 4),\n" +
"('F', NULL, 7, 'Hörmann', 'Electronic Circuit Design', 'G1 1.01', 4, 6),\n" +
"('FOI', NULL, NULL, 'Hörmann', 'Electronic Circuit Design', 'G1 1.01', 2, 4),\n" +
"('FOI', NULL, NULL, 'Hörmann', 'Electronic Circuit Design', 'G1 1.01', 4, 6),\n" +
"('IST', NULL, NULL, 'Hofmann (E)', 'Energiekonzepte', '0124', 3, 3),\n" +
"('F', NULL, 6, 'Hörmann', 'Tutorial Electronic Circuit Design', 'G1 1.01', 5, 1),\n" +
"('F', NULL, 7, 'Hörmann', 'Tutorial Electronic Circuit Design', 'G1 1.01', 5, 1),\n" +
"('FOI', NULL, NULL, 'Hörmann', 'Tutorial Electronic Circuit Design', 'G1 1.01', 5, 1),\n" +
"('AM', NULL, 1, 'Buser', 'Wissenschaftl. Methodik', 'G4 -1.01', 1, 3),\n" +
"('AM', NULL, 1, 'Baumbach', 'Projekt Brillenglas', 'G4 1.01', 3, 2),\n" +
"('AM', NULL, 1, 'Baumbach', 'Projekt Brillenglas', 'G4 1.01', 3, 3),\n" +
"('MW', NULL, 1, 'Schon', 'Mathematik 1', '0122', 1, 2),\n" +
"('MW', NULL, 1, 'Schon', 'Mathematik 1', '0103', 3, 1),\n" +
"('M', NULL, 4, 'Kalhöfer', 'Fertigungstechnik', '0102', 1, 2),\n" +
"('MW', NULL, 1, 'Heilmann', 'Grundlagen der VWL', '0103', 3, 2),\n" +
"('MW', NULL, 1, 'Heilmann', 'Grundlagen der VWL', '0103', 3, 3),\n" +
"('VI', NULL, 6, 'Schrader', 'International Marketing Strategy', '0223', 4, 2),\n" +
"('VI', NULL, 6, 'Schrader', 'International Marketing Strategy', '0223', 4, 3),\n" +
"('P', NULL, 4, 'Gretzschel', 'Systemdynamik Übungen', '0034', 3, 1),\n" +
"('MP', NULL, 1, 'Plotzitza', 'Festigkeitslehre 1', '0131', 3, 2),\n" +
"('MP', NULL, 1, 'Plotzitza', 'Festigkeitslehre 1', '0131', 3, 3),\n" +
"('ET', NULL, 3, 'Horn', 'Elektrische Messtechnik', 'G2 2.26', 2, 2),\n" +
"('ET', NULL, 3, 'Horn', 'Elektrische Messtechnik', 'G2 2.26', 2, 3),\n" +
"('ET', NULL, 3, 'Horn', 'Elektrische Messtechnik', 'G2 0.37', 5, 1),\n" +
"('AM', NULL, 1, 'Schiefer', 'W-Projekt-Hören&Sehen', NULL, 4, 1),\n" +
"('AM', NULL, 1, 'Schiefer', 'Augenerkrankungen', NULL, 2, 5),\n" +
"('M', 'E, K, R', 6, 'Gretzschel', 'Maschinendynamik Übungen', '0290', 4, 5),\n" +
"('IN', 'IS, MI, SE', 3, 'Fischer', 'Wahrscheinl.Theorie / Statistik', 'G2 0.37', 2, 4),\n" +
"('IN', 'IS, MI, SE', 3, 'Fischer', 'Wahrscheinl.Theorie / Statistik', 'G2 0.37', 2, 5),\n" +
"('WBA', NULL, 2, '?', 'Raumreservierung', 'G2 0.01, G2 2.41, G2 1.44, G2 0.37, G2 0.21', 5, 5),\n" +
"('WBA', NULL, 2, '?', 'Raumreservierung', 'G2 0.01, G2 2.41, G2 1.44, G2 0.37, G2 0.21', 5, 6),\n" +
"('WBA', NULL, 2, '?', 'Raumreservierung', 'G2 0.01, G2 2.41, G2 1.44, G2 0.37, G2 0.21', 5, 7),\n" +
"('GE', NULL, 4, 'Piper', 'Labor Mechanik', 'GS 1.15', 4, 4),\n" +
"('GF', NULL, 3, 'Piper', 'Labor Mechanik', 'GS 1.15', 4, 4),\n" +
"('VMM', NULL, 4, 'Wisniewski', 'Konstruktionslehre 1 - Übungen', '0102', 1, 4),\n" +
"('VMM', NULL, 4, 'Wisniewski', 'Konstruktionslehre 1 - Übungen', '0102', 1, 5),\n" +
"('GE', NULL, 4, 'Piper', 'Labor Wärmelehre', 'GS 1.15', 4, 5),\n" +
"('GF', NULL, 3, 'Piper', 'Labor Wärmelehre', 'GS 1.15', 4, 5),\n" +
"('PEF', NULL, NULL, 'Riegel', 'Laser Appplication Technologie', 'AH 1.02', 4, 2),\n" +
"('PEF', NULL, NULL, 'Riegel', 'Laser Appplication Technologie', 'AH 1.02', 4, 3),\n" +
"('TMP', NULL, NULL, 'Riegel', 'Laser Appplication Technologie', 'AH 1.02', 4, 2),\n" +
"('TMP', NULL, NULL, 'Riegel', 'Laser Appplication Technologie', 'AH 1.02', 4, 3),\n" +
"('TME', NULL, NULL, 'Riegel', 'Laser Appplication Technologie', 'AH 1.02', 4, 2),\n" +
"('TME', NULL, NULL, 'Riegel', 'Laser Appplication Technologie', 'AH 1.02', 4, 3),\n" +
"('PH', NULL, NULL, 'Riegel', 'Laser application Technologie', 'AH 1.02', 4, 2),\n" +
"('PH', NULL, NULL, 'Riegel', 'Laser application Technologie', 'AH 1.02', 4, 3),\n" +
"('AMM', NULL, 1, 'Riegel', 'Laser Appplication Technologie', 'AH 1.02', 4, 2),\n" +
"('AMM', NULL, 1, 'Riegel', 'Laser Appplication Technologie', 'AH 1.02', 4, 3),\n" +
"('AMM', NULL, 2, 'Riegel', 'Laser Appplication Technologie', 'AH 1.02', 4, 2),\n" +
"('AMM', NULL, 2, 'Riegel', 'Laser Appplication Technologie', 'AH 1.02', 4, 3),\n" +
"('ZDO', NULL, NULL, 'Kreuzer', 'Deutsch als Fremdsprache A2.2', '0124', 2, 4),\n" +
"('ZDO', NULL, NULL, 'Kreuzer', 'Deutsch als Fremdsprache A2.2', '0124', 2, 5),\n" +
"('GE', NULL, 2, 'Liebschner', 'Energieübertragung', 'M17', 1, 3),\n" +
"('F', NULL, 6, 'Liebschner', 'Energieübertragung', 'M17', 1, 3),\n" +
"('F', NULL, 7, 'Liebschner', 'Energieübertragung', 'M17', 1, 3),\n" +
"('MekA', NULL, 5, 'Liebschner', 'Energieübertragung', 'M17', 1, 3),\n" +
"('ZDO', NULL, NULL, 'Baur E.', 'Deutsch als Fremdsprache B1', '0114', 1, 4),\n" +
"('ZDO', NULL, NULL, 'Baur E.', 'Deutsch als Fremdsprache B1', '0114', 3, 4),\n" +
"('ZDO', NULL, NULL, 'Freller', 'German & European Culture', 'AH 1.02', 4, 5),\n" +
"('ZDO', NULL, NULL, 'Freller', 'German & European Culture', 'AH 1.02', 4, 6),\n" +
"('FR', NULL, 1, 'Wendland', 'Kommunikation und Didaktik', 'G1 0.20', 1, 5),\n" +
"('ZSO', NULL, NULL, 'Vazquez', 'Spanisch A1', '0007', 2, 5),\n" +
"('ZSO', NULL, NULL, 'Vazquez', 'Spanisch A1', '0007', 3, 4),\n" +
"('VI', NULL, 2, 'Hommel', 'Mathematik - Vertiefung Übungen', '0129', 2, 2),\n" +
"('B', NULL, 2, 'Radtke', 'Grundlagen Marketing / Intern. Mark.', '0258', 2, 2),\n" +
"('B', NULL, 2, 'Radtke', 'Grundlagen Marketing / Intern. Mark.', '0258', 2, 3),\n" +
"('IDM', NULL, 1, 'Ulsamer', 'Bauteilentw.-u.Konstruktionstechnik', '0106', 3, 7),\n" +
"('IDM', NULL, 1, 'Class', 'Bauteilentwicklung u. Konstruktionstechnik', '0233', 5, 1),\n" +
"('GE', NULL, 4, 'Barth A.', 'Labor Energie- / Automatisierungstechnik', 'G1 0.21', 3, 3),\n" +
"('FR', NULL, 6, 'Barth A.', 'Labor Energie- / Automatisierungstechnik', 'G1 0.21', 3, 3),\n" +
"('C', NULL, 1, 'Stein', 'Stöchiometrie', '0102', 3, 2),\n" +
"('FR', NULL, 3, 'Schmidt H.', 'Laborführerschein Elektronik', 'G1 0.34', 2, 1),\n" +
"('VI', NULL, 3, 'Zarzuela', 'Spanisch A1', '0111', 2, 1),\n" +
"('VI', NULL, 3, 'Zarzuela', 'Spanisch A1', '0111', 2, 2),\n" +
"('C', NULL, 2, 'Hader', 'Physik 2 mit Labor', '0234', 5, 1),\n" +
"('V', NULL, 1, 'Hader', 'Grundlagen der Physik', '0234', 5, 1),\n" +
"('VMG', NULL, 1, 'Hader', 'Grundlagen der Physik', '0234', 5, 1),\n" +
"('VMM', NULL, 1, 'Hader', 'Grundlagen der Physik', '0234', 5, 1),\n" +
"('ET', NULL, 1, 'Elser', 'Praktische Elektronik', 'G2 2.26', 5, 3),\n" +
"('ET', NULL, 1, 'Elser', 'Praktische Elektronik', 'G2 2.26', 5, 4),\n" +
"('ET', NULL, 1, 'Elser', 'Praktische Elektronik', 'G2 2.26', 5, 5),\n" +
"('ET', NULL, 1, 'Hahn-Dambacher', NULL, 'G2 2.26', 5, 3),\n" +
"('ET', NULL, 1, 'Hahn-Dambacher', NULL, 'G2 2.26', 5, 4),\n" +
"('ET', NULL, 1, 'Hahn-Dambacher', NULL, 'G2 2.26', 5, 5),\n" +
"('ET', NULL, 1, 'Kolb', NULL, 'G2 2.26', 5, 3),\n" +
"('ET', NULL, 1, 'Kolb', NULL, 'G2 2.26', 5, 4),\n" +
"('WBA', NULL, 3, '?', 'Raumreservierung', 'G2 0.01, G2 0.21, G2 0.37, G2 1.44, G2 2.41', 6, 5),\n" +
"('WBA', NULL, 3, '?', 'Raumreservierung', 'G2 0.01, G2 1.44, G2 0.37, G2 0.21', 6, 3),\n" +
"('M', NULL, 4, 'Feifel, Wisniewski', 'Konstruktionslehre 1 - Übungen', '0106, 0102', 1, 4),\n" +
"('M', NULL, 4, 'Feifel, Wisniewski', 'Konstruktionslehre 1 - Übungen', '0106, 0102', 1, 5),\n" +
"('WBA', NULL, 3, '?', 'Raumreservierung', 'G2 0.01, G2 0.37', 6, 4),\n" +
"('WBA', NULL, 3, '?', 'Raumreservierung', 'G2 0.01, G2 0.21', 6, 2),\n" +
"('ET', NULL, 1, 'Kolb', NULL, 'G2 2.26', 5, 5),\n" +
"('P', NULL, 4, 'Gärtner', 'CAE-Projekt', '0103', 2, 4),\n" +
"('P', NULL, 3, 'Feuchter', 'Thermodynamik', 'AH 0.01', 1, 2),\n" +
"('P', NULL, 3, 'Feuchter', 'Thermodynamik', 'AH 0.01', 1, 3),\n" +
"('P', NULL, 3, 'Feuchter', 'Thermodynamik', 'AH 0.01', 4, 3),\n" +
"('M', NULL, 3, 'Feuchter', 'Thermodynamik', 'AH 0.01', 1, 2),\n" +
"('M', NULL, 3, 'Feuchter', 'Thermodynamik', 'AH 0.01', 1, 3),\n" +
"('M', NULL, 3, 'Feuchter', 'Thermodynamik', 'AH 0.01', 4, 3),\n" +
"('MW', NULL, 1, 'Plotzitza', 'Festigkeitslehre Übungen', '0102', 1, 3),\n" +
"('MP', NULL, 1, 'Plotzitza', 'Technische Mechanik Übungen', '0131', 3, 1),\n" +
"('M', NULL, 2, 'Maier U.', 'Tutorium Mathematik II', '0102', 1, 1),\n" +
"('P', NULL, 2, 'Maier U.', 'Tutorium Mathematik II', '0102', 1, 1),\n" +
"('IDM', NULL, 1, 'Gretzschel', 'E-Mobilität / Hybrid', '0130', 2, 5),\n" +
"('IDM', NULL, 1, 'Gretzschel', 'E-Mobilität / Hybrid', '0130', 2, 6),\n" +
"('IST', NULL, NULL, 'Gretzschel', 'Elektromobilität', '0130', 2, 5),\n" +
"('IST', NULL, NULL, 'Gretzschel', 'Elektromobilität', '0130', 2, 6),\n" +
"('FR', NULL, 2, 'Weissgerber', 'Layoutsysteme 1 - FrameMaker', 'G1 1.29', 4, 2),\n" +
"('MW', NULL, 1, 'Kirsch', 'Mathematik Tutorium', '0223', 3, 4),\n" +
"('F', NULL, 1, 'Nagengast', 'Tutorium Mathematik 1', 'AH -1.01', 3, 5),\n" +
"('MekA', NULL, 1, 'Nagengast', 'Tutorium Mathematik 1', 'AH -1.01', 3, 5),\n" +
"('FR', NULL, 1, 'Nagengast', 'Tutorium Mathematik 1', 'AH -1.01', 3, 5),\n" +
"('GF', NULL, 1, 'Nagengast', 'Tutorium Mathematik 1', 'AH -1.01', 3, 5),\n" +
"('C', NULL, 1, 'Redlinger', 'Tutorium Mathematik Chemie', '0132', 1, 3),\n" +
"('F', NULL, 2, 'Nagengast', 'Tutorium Mathematik 2', 'G1 0.01', 4, 5),\n" +
"('GE', NULL, 2, 'Nagengast', 'Tutorium Mathematik 2', 'G1 0.01', 4, 5),\n" +
"('K', NULL, 1, 'Waltschläger', 'Physik Tutorium', '0121', 4, 3),\n" +
"('M', NULL, 1, '?', 'Tutorium Experimentalphysik', '0102', 3, 5),\n" +
"('O', NULL, 2, 'Suckow', 'Physik Tutorium', 'G1 0.06', 4, 4),\n" +
"('IN', 'MI', 1, 'Lecon', 'Multimediadesign', 'G2 0.23', 3, 2),\n" +
"('IN', 'MI', 2, 'Lecon', 'Multimediadesign', 'G2 0.23', 3, 2),\n" +
"('IN', 'MI', 4, 'Lecon', 'A / V-Medien', 'G2 1.44', 3, 4),\n" +
"('IN', 'MI', 4, 'Lecon', 'A / V-Medien', 'G2 1.44', 4, 5),\n" +
"('IN', 'MI', 6, 'Lecon', 'Medienformate', 'G2 0.37', 5, 4),\n" +
"('IN', 'MI', 7, 'Lecon', 'Medienformate', 'G2 0.37', 5, 4),\n" +
"('CCS', NULL, 2, 'Lecon', 'Medienformate', 'G2 0.37', 5, 4),\n" +
"('IN', 'MI', 7, 'Lecon', 'Spiele-Programmierung', 'G2 0.21', 2, 1),\n" +
"('IN', 'MI', 7, 'Lecon', 'Spiele-Programmierung', 'G2 0.21', 4, 3),\n" +
"('CCS', NULL, 2, 'Lecon', 'Spiele-Programmierung', 'G2 0.21', 2, 1),\n" +
"('CCS', NULL, 2, 'Lecon', 'Spiele-Programmierung', 'G2 0.21', 4, 3),\n" +
"('W', NULL, 2, 'Depner', 'Einführung in die Informatik (SPO 31)', '0275', 1, 3),\n" +
"('IN', 'IS, SE, MI', 2, 'Lecon', 'Algorithmen und Datenstrukturen 1', 'G2 1.30', 5, 1),\n" +
"('IN', 'IS, WI', 6, 'Höpken', 'Datenschutz', 'G2 0.23', 5, 4),\n" +
"('IN', 'IS, WI', 6, 'Höpken', 'Datenschutz', 'G2 0.23', 5, 5),\n" +
"('IN', 'IS, WI', 6, 'Höpken', 'Datenschutz', 'G2 0.23', 5, 6),\n" +
"('IN', 'IS, WI', 6, 'Höpken', 'Datenschutz', 'G2 2.01', 6, 1),\n" +
"('IN', 'IS, WI', 6, 'Höpken', 'Datenschutz', 'G2 2.01', 6, 2),\n" +
"('IN', 'IS, WI', 6, 'Höpken', 'Datenschutz', 'G2 2.01', 6, 3),\n" +
"('IN', 'IS, WI', 7, 'Höpken', 'Datenschutz', 'G2 0.23', 5, 4),\n" +
"('IN', 'IS, WI', 7, 'Höpken', 'Datenschutz', 'G2 0.23', 5, 5),\n" +
"('IN', 'IS, WI', 7, 'Höpken', 'Datenschutz', 'G2 0.23', 5, 6),\n" +
"('IN', 'IS, WI', 7, 'Höpken', 'Datenschutz', 'G2 2.01', 6, 1),\n" +
"('IN', 'IS, WI', 7, 'Höpken', 'Datenschutz', 'G2 2.01', 6, 2),\n" +
"('IN', 'IS, WI', 7, 'Höpken', 'Datenschutz', 'G2 2.01', 6, 3),\n" +
"('M', 'E', 6, 'Hubel', 'Messdatenverarb. Sensortechnik - Einführung', '0034, 0215', 2, 2),\n" +
"('P', NULL, 3, 'Gärtner, Wagner (P)', 'Rendering Digital und Analog', '0289, 0234', 3, 2),\n" +
"('I', NULL, 1, 'Knobelspies, Morgado', 'TOEIC Vorbereitungskurs', '0213, 0203', 3, 4),\n" +
"('I', NULL, 1, 'Knobelspies, Morgado', 'TOEIC Vorbereitungskurs', '0213, 0203', 3, 5),\n" +
"('I', NULL, 1, 'Knobelspies, Morgado', 'TOEIC Vorbereitungskurs', '0213, 0203', 3, 6),\n" +
"('P', NULL, 4, 'Frank (P), Zorniger, Wagner', 'Labor Regelungstechnik', '0105, 0187', 6, 2),\n" +
"('P', NULL, 4, 'Frank (P), Zorniger, Wagner', 'Labor Regelungstechnik', '0105, 0187', 6, 3),\n" +
"('O', NULL, 6, 'Frasch, Pretorius', 'Optik-Design', 'G1 2.36, G1 0.21', 3, 4),\n" +
"('O', NULL, 6, 'Frasch, Pretorius', 'Optik-Design', 'G1 2.36, G1 0.21', 3, 5),\n" +
"('P', NULL, 3, 'Gärtner, Wagner (P)', 'Rendering Digital und Analog', '0289, 0114', 3, 1),\n" +
"('W', NULL, 2, 'Depner', 'Grundlagen der Informatik (SPO 31)', '0275', 1, 2),\n" +
"('A', NULL, 1, '?', 'Tutorium Mathematik-GO1', 'G4 -1.01', 3, 5),\n" +
"('A', NULL, 1, '?', 'Tutorium Mathematik-GO1', 'G4 -1.01', 3, 6),\n" +
"('A', NULL, 1, '?', 'Tutorium Mathematik-GO1', 'G4 0.02', 4, 1),\n" +
"('A', NULL, 1, '?', 'Tutorium Geometrische Optik 1', 'G4 0.02', 3, 5),\n" +
"('A', NULL, 1, '?', 'Tutorium Geometrische Optik 1', 'G4 -1.01', 3, 6),\n" +
"('A', NULL, 1, '?', 'Tutorium Geometrische Optik 1', 'G4 0.02', 4, 1),\n" +
"('A', NULL, 3, 'Holschbach', 'Praktikum Kontaktlinse 1', 'G4 1.02', 3, 2),\n" +
"('A', NULL, 3, 'Holschbach', 'Praktikum Kontaktlinse 1', 'G4 1.02', 3, 3),\n" +
"('A', NULL, 4, 'Kirschkamp', 'Kontaktlinse 2', 'G4 0.02', 1, 4),\n" +
"('AH', NULL, 4, 'Kirschkamp', 'Kontaktlinse 2', 'G4 0.02', 1, 4),\n" +
"('A', NULL, 4, 'Schiefer', 'Sehbehinderung - Beratung', 'G4 -1.01', 4, 2),\n" +
"('A', NULL, 4, 'Schiefer', 'Sehbehinderung - Beratung', 'G4 -1.01', 4, 3),\n" +
"('AH', NULL, 4, 'Montague', 'Management Skills Optometry', NULL, 6, 1),\n" +
"('AH', NULL, 4, 'Montague', 'Management Skills Optometry', NULL, 6, 2),\n" +
"('AH', NULL, 4, 'Montague', 'Management Skills Optometry', NULL, 6, 3),\n" +
"('A', NULL, 4, 'Montague', 'Management Skills Optometry', NULL, 6, 1),\n" +
"('A', NULL, 4, 'Montague', 'Management Skills Optometry', NULL, 6, 2),\n" +
"('A', NULL, 4, 'Montague', 'Management Skills Optometry', NULL, 6, 3),\n" +
"('GE', NULL, 2, 'Glunk', 'Grundlagen Elektrizität / Magnetismus', 'G1 1.20', 1, 6),\n" +
"('GF', NULL, 3, 'Glunk', 'Grundlagen Elektrizität / Magnetismus', 'G1 1.20', 1, 6),\n" +
"('GME', NULL, NULL, 'Glunk', 'Grundlagen Kern- / Teilchen- / Astrophysik', 'G1 1.20', 1, 4),\n" +
"('GMF', NULL, NULL, 'Glunk', 'Grundlagen Kern- / Teilchen- / Astrophysik', 'G1 1.20', 1, 4),\n" +
"('F', NULL, 6, 'Kazi', 'Advanced Actuators & Sensors', 'G1 1.23', 4, 4),\n" +
"('F', NULL, 6, 'Kazi', 'Advanced Actuators & Sensors', 'G1 1.23', 4, 5),\n" +
"('F', NULL, 7, 'Kazi', 'Advanced Actuators & Sensors', 'G1 1.23', 4, 4),\n" +
"('F', NULL, 7, 'Kazi', 'Advanced Actuators & Sensors', 'G1 1.23', 4, 5),\n" +
"('MekA', NULL, 5, 'Kazi', 'Advanced Actuators & Sensors', 'G1 1.23', 4, 4),\n" +
"('MekA', NULL, 5, 'Kazi', 'Advanced Actuators & Sensors', 'G1 1.23', 4, 5),\n" +
"('FOI', NULL, NULL, 'Kazi', 'Advanced Actuators & Sensors', 'G1 1.23', 4, 4),\n" +
"('FOI', NULL, NULL, 'Kazi', 'Advanced Actuators & Sensors', 'G1 1.23', 4, 5),\n" +
"('A', NULL, 4, 'Michels', 'Praktikum Kontaktlinse 2', 'G4 1.02', 4, 4),\n" +
"('A', NULL, 4, 'Michels', 'Praktikum Kontaktlinse 2', 'G4 1.02', 4, 5),\n" +
"('AH', NULL, 4, 'Michels', 'Praktikum Kontaktlinse 2', 'G4 1.02', 4, 4),\n" +
"('AH', NULL, 4, 'Michels', 'Praktikum Kontaktlinse 2', 'G4 1.02', 4, 5),\n" +
"('F', NULL, 6, 'Glaser', 'Medical Engineering', 'G1 0.21', 2, 2),\n" +
"('F', NULL, 6, 'Glaser', 'Medical Engineering', 'G1 0.22', 5, 5),\n" +
"('FOI', NULL, NULL, 'Glaser', 'Medical Engineering', 'G1 0.21', 2, 2),\n" +
"('FOI', NULL, NULL, 'Glaser', 'Medical Engineering', 'G1 0.22', 5, 5),\n" +
"('WI', NULL, 1, 'Benz', 'Wirtschaftsinformatik 1', 'G2 1.30', 3, 4),\n" +
"('WI', NULL, 1, 'Benz', 'Wirtschaftsinformatik 1', 'G2 1.30', 3, 5),\n" +
"('F', NULL, 6, 'Glaser', 'Leistungselektronik', 'G1 0.21', 4, 1),\n" +
"('F', NULL, 6, 'Glaser', 'Leistungselektronik', 'G1 0.22', 5, 4),\n" +
"('F', NULL, 7, 'Glaser', 'Leistungselektronik', 'G1 0.21', 4, 1),\n" +
"('F', NULL, 7, 'Glaser', 'Leistungselektronik', 'G1 0.22', 5, 4),\n" +
"('GE', NULL, 6, 'Glaser', 'Leistungselektronik', 'G1 0.21', 4, 1),\n" +
"('GE', NULL, 6, 'Glaser', 'Leistungselektronik', 'G1 0.22', 5, 4),\n" +
"('MekA', NULL, 5, 'Glaser', 'Leistungselektronik', 'G1 0.21', 4, 1),\n" +
"('MekA', NULL, 5, 'Glaser', 'Leistungselektronik', 'G1 0.22', 5, 4),\n" +
"('WI', NULL, 1, 'Baumann T.', 'Grundlagen der Informatik', 'G2 0.21', 1, 3),\n" +
"('WI', NULL, 1, 'Baumann T.', 'Grundlagen der Informatik', 'G2 0.21', 1, 4),\n" +
"('F', NULL, 6, 'Glaser', 'Labor Leistungselektronik', 'G1 1.29', 5, 6),\n" +
"('F', NULL, 7, 'Glaser', 'Labor Leistungselektronik', 'G1 1.29', 5, 6),\n" +
"('GE', NULL, 6, 'Glaser', 'Labor Leistungselektronik', 'G1 1.29', 5, 6),\n" +
"('MekA', NULL, 5, 'Glaser', 'Labor Leistungselektronik', 'G1 1.29', 5, 6),\n" +
"('WI', NULL, 1, 'Özel', 'ABWL', 'G1 2.36', 5, 4),\n" +
"('WI', NULL, 1, 'Özel', 'ABWL', 'G1 2.36', 5, 5),\n" +
"('WI', NULL, 1, 'Özel', 'ABWL', 'G1 2.36', 5, 6),\n" +
"('WI', NULL, 1, 'Özel', 'ABWL', 'G1 0.01', 6, 2),\n" +
"('WI', NULL, 1, 'Özel', 'ABWL', 'G1 0.01', 6, 3),\n" +
"('WI', NULL, 1, 'Özel', 'ABWL', 'G1 0.01', 6, 4),\n" +
"('MWI', NULL, 1, 'Antonic', 'Business Intelligence & Controlling', '0201', 5, 1),\n" +
"('MWI', NULL, 1, 'Antonic', 'Business Intelligence & Controlling', '0201', 5, 2),\n" +
"('MWI', NULL, 1, 'Antonic', 'Business Intelligence & Controlling', '0201', 5, 3),\n" +
"('MWI', NULL, 1, 'Antonic', 'Business Intelligence & Controlling', '0102', 6, 2),\n" +
"('MWI', NULL, 1, 'Antonic', 'Business Intelligence & Controlling', '0102', 6, 3),\n" +
"('F', NULL, 6, 'Kazi', 'Tutorial Advanced Actuators & Sensors', 'G1 0.22', 1, 5),\n" +
"('F', NULL, 7, 'Kazi', 'Tutorial Advanced Actuators & Sensors', 'G1 0.22', 1, 5),\n" +
"('MekA', NULL, 5, 'Kazi', 'Tutorial Advanced Actuators & Sensors', 'G1 0.22', 1, 5),\n" +
"('FOI', NULL, NULL, 'Kazi', 'Tutorial Advanced Actuators & Sensors', 'G1 0.22', 1, 5),\n" +
"('F', NULL, 6, 'Glaser', 'Tutorial Medical Engineering', 'G1 1.29', 1, 1),\n" +
"('FOI', NULL, NULL, 'Glaser', 'Tutorial Medical Engineering', 'G1 1.29', 1, 1),\n" +
"('FR', NULL, 1, 'Weissgerber', 'Sprachliche Gestaltung 1', 'G1 0.22', 2, 1),\n" +
"('P', NULL, 6, 'Subek', 'Fertigungstechnik', '0211', 3, 4),\n" +
"('AH', NULL, 4, 'Buschle, Lanzinger', 'Praktikum Opt.Screening', 'G4 1.07, G4 2.02', 3, 3),\n" +
"('AH', NULL, 4, 'Buschle, Lanzinger', 'Praktikum Opt.Screening', 'G4 1.07, G4 2.02', 3, 4),\n" +
"('A', NULL, 4, 'Buschle, Lanzinger', 'Praktikum Opt.Screening', 'G4 1.07, G4 2.02', 3, 4),\n" +
"('P', NULL, 6, 'Subek', 'Fertigungstechnik', '0211', 3, 5),\n" +
"('FR', NULL, 3, 'Weissgerber', 'Sprachliche Gestaltung 2', 'G1 1.01', 2, 5),\n" +
"('P', NULL, 6, 'Schupbach', 'Produktentwickung  /  Konstruktion II', '0115', 4, 5),\n" +
"('P', NULL, 6, 'Schupbach', 'Produktentwickung  /  Konstruktion II', '0115', 4, 6);" 
,"INSERT INTO g02_vorlesungsplan (stg, schwerpunkt, semester, dozent, kurs, raum, tagnr,  zeitblocknr) VALUES\n" +
            "('P', NULL, 6, 'Schupbach', 'Produktentwickung  /  Konstruktion II', '0106', 5, 1),\n" +
"('P', NULL, 6, 'Schupbach', 'Produktentwickung  /  Konstruktion II', '0106', 5, 2),\n" +
"('F', NULL, 1, 'Liebschner', 'Elektrotechnik Grundl. - Gleich- u. Wechselstromt.', 'G2 0.23', 1, 2),\n" +
"('F', NULL, 1, 'Liebschner', 'Elektrotechnik Grundl. - Gleich- u. Wechselstromt.', 'AH -1.01', 2, 4),\n" +
"('F', NULL, 1, 'Liebschner', 'Elektrotechnik Grundl. - Gleich- u. Wechselstromt.', 'G2 0.23', 4, 3),\n" +
"('FR', NULL, 2, 'Liebschner', 'Elektrotechnik Grundl. - Gleich- u. Wechselstromt.', 'G2 0.23', 1, 2),\n" +
"('FR', NULL, 2, 'Liebschner', 'Elektrotechnik Grundl. - Gleich- u. Wechselstromt.', 'AH -1.01', 2, 4),\n" +
"('FR', NULL, 2, 'Liebschner', 'Elektrotechnik Grundl. - Gleich- u. Wechselstromt.', 'G2 0.23', 4, 3),\n" +
"('GF', NULL, 3, 'Liebschner', 'Elektrotechnik Grundl. - Gleich- u. Wechselstromt.', 'G2 0.23', 1, 2),\n" +
"('GF', NULL, 3, 'Liebschner', 'Elektrotechnik Grundl. - Gleich- u. Wechselstromt.', 'AH -1.01', 2, 4),\n" +
"('GF', NULL, 3, 'Liebschner', 'Elektrotechnik Grundl. - Gleich- u. Wechselstromt.', 'G2 0.23', 4, 3),\n" +
"('FR', NULL, 3, 'Liebschner', 'Elektrotechnik Grundl. - Gleich- u. Wechselstromt.', 'G2 0.23', 1, 2),\n" +
"('FR', NULL, 3, 'Liebschner', 'Elektrotechnik Grundl. - Gleich- u. Wechselstromt.', 'AH -1.01', 2, 4),\n" +
"('FR', NULL, 3, 'Liebschner', 'Elektrotechnik Grundl. - Gleich- u. Wechselstromt.', 'G2 0.23', 4, 3),\n" +
"('MekA', NULL, 1, 'Liebschner', 'Elektrotechnik Grundl. - Gleich- u. Wechselstromt.', 'G2 0.23', 1, 2),\n" +
"('MekA', NULL, 1, 'Liebschner', 'Elektrotechnik Grundl. - Gleich- u. Wechselstromt.', 'AH -1.01', 2, 4),\n" +
"('MekA', NULL, 1, 'Liebschner', 'Elektrotechnik Grundl. - Gleich- u. Wechselstromt.', 'G2 0.23', 4, 3),\n" +
"('ET', NULL, 1, 'Liebschner', 'Elektrotechnik Grundl. - Gleich- u. Wechselstromt.', 'G2 0.23', 1, 2),\n" +
"('ET', NULL, 1, 'Liebschner', 'Elektrotechnik Grundl. - Gleich- u. Wechselstromt.', 'AH -1.01', 2, 4),\n" +
"('ET', NULL, 1, 'Liebschner', 'Elektrotechnik Grundl. - Gleich- u. Wechselstromt.', 'G2 0.23', 4, 3),\n" +
"('F', NULL, 1, 'Hörmann', 'Strukturierte Programmierung', 'G1 0.20', 1, 1),\n" +
"('MekA', NULL, 1, 'Hörmann', 'Strukturierte Programmierung', 'G1 0.20', 1, 1),\n" +
"('GF', NULL, 3, 'Hörmann', 'Strukturierte Programmierung', 'G1 0.20', 1, 1),\n" +
"('GMF', NULL, NULL, 'Berger (F)', 'Rapid Product Development', 'G1 0.22', 4, 5),\n" +
"('VI', NULL, 2, 'Subek', 'Fertigungsverfahren', '0273', 1, 1),\n" +
"('VMM', NULL, 3, 'Subek', 'Fertigungsverfahren', '0273', 1, 1),\n" +
"('V', NULL, 4, 'Subek', 'Fertigungsverfahren', '0273', 1, 1),\n" +
"('VMG', NULL, 3, 'Subek', 'Fertigungsverfahren', '0273', 1, 1),\n" +
"('VMM', NULL, 4, 'Subek', 'Fertigungsverfahren', '0273', 1, 1),\n" +
"('VI', NULL, 4, 'Lamek-Creutz', 'Grundlagen der Messtechnik', 'AH 1.01', 1, 2),\n" +
"('VI', NULL, 4, 'Lamek-Creutz', 'Grundlagen der Messtechnik', 'AH 1.01', 1, 3),\n" +
"('VMM', NULL, 4, 'Lamek-Creutz', 'Grundlagen der Messtechnik', 'AH 1.01', 1, 2),\n" +
"('VMM', NULL, 4, 'Lamek-Creutz', 'Grundlagen der Messtechnik', 'AH 1.01', 1, 3),\n" +
"('V', NULL, 3, 'Lamek-Creutz', 'Grundlagen der Messtechnik', 'AH 1.01', 1, 2),\n" +
"('V', NULL, 3, 'Lamek-Creutz', 'Grundlagen der Messtechnik', 'AH 1.01', 1, 3),\n" +
"('VMG', NULL, 3, 'Lamek-Creutz', 'Grundlagen der Messtechnik', 'AH 1.01', 1, 2),\n" +
"('VMG', NULL, 3, 'Lamek-Creutz', 'Grundlagen der Messtechnik', 'AH 1.01', 1, 3),\n" +
"('V', NULL, 4, 'Lamek-Creutz', 'Grundlagen der Messtechnik', 'AH 1.01', 1, 2),\n" +
"('V', NULL, 4, 'Lamek-Creutz', 'Grundlagen der Messtechnik', 'AH 1.01', 1, 3),\n" +
"('VI', NULL, 4, 'Athanasiou', 'Qualitätsmanagement', 'AH 1.02', 5, 5),\n" +
"('V', NULL, 4, 'Athanasiou', 'Qualitätsmanagement', 'AH 1.02', 5, 5),\n" +
"('VMM', NULL, 4, 'Athanasiou', 'Qualitätsmanagement', 'AH 1.02', 5, 5),\n" +
"('VI', NULL, 7, 'Ebenfeld', 'International Finance', '0129', 4, 2),\n" +
"('VI', NULL, 7, 'Ebenfeld', 'International Finance', '0129', 4, 3),\n" +
"('VI', NULL, 7, 'Ebenfeld', 'International Finance', '0130', 4, 4),\n" +
"('VI', NULL, 7, 'Ebenfeld', 'International Finance', '0130', 4, 5),\n" +
"('MP', NULL, 1, 'Plotzitza', 'Festigkeitslehre Übungen', '0211', 1, 2),\n" +
"('GF', NULL, 1, 'Barth A.', 'Fachdidaktik Physik', 'G1 1.20', 2, 5),\n" +
"('GF', NULL, 1, 'Barth A.', 'Seminar zur Fachdidaktik Physik', 'G1 1.20', 3, 6),\n" +
"('MP', NULL, 3, 'Mrzyglod', 'Kraft- und Arbeitsmaschinen', 'AH 1.01', 1, 5),\n" +
"('MP', NULL, 3, 'Mrzyglod', 'Kraft- und Arbeitsmaschinen', 'AH 1.01', 1, 6),\n" +
"('MW', NULL, 3, 'Mrzyglod', 'Kraft- und Arbeitsmaschinen', 'AH 1.01', 1, 5),\n" +
"('MW', NULL, 3, 'Mrzyglod', 'Kraft- und Arbeitsmaschinen', 'AH 1.01', 1, 6),\n" +
"('GE', NULL, 6, 'Glunk', 'Mechanik 2 / Wärmelehre 2', 'G1 1.20', 1, 5),\n" +
"('GF', NULL, 7, 'Glunk', 'Mechanik 2 / Wärmelehre 2', 'G1 1.20', 1, 5),\n" +
"('GME', NULL, NULL, 'Glunk', 'Grundlagen Kern- / Teilchen- / Astrophysik', 'G1 1.20', 1, 3),\n" +
"('GME', NULL, NULL, 'Glunk', 'Grundlagen Kern- / Teilchen- / Astrophysik', 'G1 1.20', 2, 2),\n" +
"('GMF', NULL, NULL, 'Glunk', 'Grundlagen Kern- / Teilchen- / Astrophysik', 'G1 1.20', 1, 3),\n" +
"('GMF', NULL, NULL, 'Glunk', 'Grundlagen Kern- / Teilchen- / Astrophysik', 'G1 1.20', 2, 2),\n" +
"('MP', NULL, 3, 'Schon', 'EDV', '0273', 3, 3),\n" +
"('MW', NULL, 3, 'Schon', 'EDV', '0273', 3, 3),\n" +
"('MW', NULL, 7, 'Schillig', 'Lean Management', '0120', 4, 5),\n" +
"('MW', NULL, 7, 'Schillig', 'Lean Management', '0120', 4, 6),\n" +
"('MP', NULL, 3, 'Schon', 'EDV Übungen', '0034', 1, 3),\n" +
"('MP', NULL, 3, 'Schon', 'EDV Übungen', '0034', 1, 4),\n" +
"('MP', NULL, 7, 'Haag', 'Automatisierungstechnik 2', '0222', 1, 2),\n" +
"('MP', NULL, 7, 'Haag', 'Automatisierungstechnik 2', '0120', 2, 3),\n" +
"('MP', NULL, 3, 'Stock', 'Qualitätsmanagement', 'AH 1.01', 3, 4),\n" +
"('M', 'E, K, R', 6, 'Merkel', 'Finite Elemente Methoden  /  FEM', '0103', 4, 4),\n" +
"('MP', NULL, 3, 'Stock', 'Qualitätsmanagement', 'AH 1.01', 3, 5),\n" +
"('MW', NULL, 3, 'Stock', 'Qualitätsmanagement', 'AH 1.01', 3, 4),\n" +
"('MW', NULL, 3, 'Stock', 'Qualitätsmanagement', 'AH 1.01', 3, 5),\n" +
"('MP', NULL, 7, 'Kalhöfer', 'Zerspanungstechnik und Werkzeugmaschinen', '0103', 1, 4),\n" +
"('MP', NULL, 7, 'Kalhöfer', 'Zerspanungstechnik und Werkzeugmaschinen', '0120', 3, 1),\n" +
"('MP', NULL, 7, 'Ruck', 'Lasertechnik 2', '0223', 3, 5),\n" +
"('MP', NULL, 7, 'Ruck', 'LasertechnikÜbungen', '0289', 3, 6),\n" +
"('MP', NULL, 7, 'Mathy', 'Umformtechnik 2', '0115', 1, 5),\n" +
"('MP', NULL, 7, 'Mathy', 'Umformtechnik 2', '0115', 2, 4),\n" +
"('MW', NULL, 1, 'Plotzitza', 'Technische Mechanik Übungen', '0106', 1, 1),\n" +
"('K', NULL, 1, 'Wenzel', 'Technische Mechanik Tutorium', '0201', 4, 1),\n" +
"('K', NULL, 1, 'Leyrer', 'Einführung in die Kunststofftechnik', 'AH 1.01', 2, 5),\n" +
"('K', NULL, 3, 'Zemanek', 'Thermodynamik und Wärmetransport', '0121', 2, 2),\n" +
"('K', NULL, 3, 'Zemanek', 'Thermodynamik und Wärmetransport', '0121', 4, 2),\n" +
"('MW', NULL, 3, 'Schon', 'EDV Übungen', '0034', 3, 2),\n" +
"('MW', NULL, 3, 'Schon', 'EDV Übungen', '0034', 4, 3),\n" +
"('K', NULL, 3, 'Frick', 'Polymerprüfung', 'AH 1.01', 1, 4),\n" +
"('K', NULL, 3, 'Class', 'Produktentwickung und Konstruktion', '0258', 3, 4),\n" +
"('K', NULL, 3, 'Class', 'Produktentwickung und Konstruktion', '0258', 3, 5),\n" +
"('TME', NULL, NULL, '?', 'Ökonom. / Anlayt. Grundlagen des Managements', '0115', 2, 5),\n" +
"('TME', NULL, NULL, '?', 'Ökonom. / Anlayt. Grundlagen des Managements', '0115', 2, 6),\n" +
"('TMP', NULL, NULL, '?', 'Ökonom. / Anlayt. Grundlagen des Managements', '0115', 2, 5),\n" +
"('TMP', NULL, NULL, '?', 'Ökonom. / Anlayt. Grundlagen des Managements', '0115', 2, 6),\n" +
"('TMP', NULL, NULL, 'Haag', 'Robotik', '0106', 1, 3),\n" +
"('TMP', NULL, NULL, 'Haag', 'Robotik', '0131', 5, 2),\n" +
"('TME', NULL, NULL, 'Haag', 'Robotik', '0106', 1, 3),\n" +
"('TME', NULL, NULL, 'Haag', 'Robotik', '0131', 5, 2),\n" +
"('PEF', NULL, NULL, 'Heine', 'Ingenieurwerkstoffe', 'AH 1.02', 2, 3),\n" +
"('PEF', NULL, NULL, 'Heine', 'Ingenieurwerkstoffe', 'AH 1.02', 2, 4),\n" +
"('TME', NULL, NULL, 'Heine', 'Ingenieurwerkstoffe', 'AH 1.02', 2, 3),\n" +
"('TME', NULL, NULL, 'Heine', 'Ingenieurwerkstoffe', 'AH 1.02', 2, 4),\n" +
"('LBM', NULL, NULL, 'Heine', 'Ingenieurwerkstoffe', 'AH 1.02', 2, 3),\n" +
"('LBM', NULL, NULL, 'Heine', 'Ingenieurwerkstoffe', 'AH 1.02', 2, 4),\n" +
"('TMP', NULL, NULL, 'Heine', 'Ingenieurwerkstoffe', 'AH 1.02', 2, 3),\n" +
"('TMP', NULL, NULL, 'Heine', 'Ingenieurwerkstoffe', 'AH 1.02', 2, 4),\n" +
"('AH', NULL, 4, 'Buschle', 'Praktikum Hörsystemanpassung 1', 'G4 -1.20', 2, 5),\n" +
"('AH', NULL, 4, 'Buschle', 'Praktikum Hörsystemanpassung 1', 'G4 -1.20', 4, 3),\n" +
"('A', NULL, 6, 'Buser', 'W-Hören & Sehen', 'G4 0.02', 2, 2),\n" +
"('A', NULL, 6, 'Buser', 'W-Hören & Sehen', 'G4 0.02', 2, 3),\n" +
"('AH', NULL, 6, 'Buser', 'W-Hören & Sehen', 'G4 0.02', 2, 2),\n" +
"('AH', NULL, 6, 'Buser', 'W-Hören & Sehen', 'G4 0.02', 2, 3),\n" +
"('A', NULL, 7, 'Buser', 'W-Hören & Sehen', 'G4 0.02', 2, 2),\n" +
"('A', NULL, 7, 'Buser', 'W-Hören & Sehen', 'G4 0.02', 2, 3),\n" +
"('AM', NULL, 1, '?', 'Interferometry', NULL, 1, 1),\n" +
"('AM', NULL, 1, '?', 'Interferometry', NULL, 2, 1),\n" +
"('AM', NULL, 1, 'Sauder', 'Augenerkrankungen', NULL, 4, 3),\n" +
"('A', NULL, 2, 'Liebhäusser', 'Werkstatt 1', 'G4 0.09', 4, 2),\n" +
"('A', NULL, 2, 'Liebhäusser', 'Werkstatt 1', 'G4 0.09', 4, 3),\n" +
"('AH', NULL, 2, 'Liebhäusser', 'Werkstatt 1', 'G4 0.09', 4, 2),\n" +
"('AH', NULL, 2, 'Liebhäusser', 'Werkstatt 1', 'G4 0.09', 4, 3),\n" +
"('O', NULL, 4, 'Bauer (O)', 'Controlling', 'G1 0.22', 3, 3),\n" +
"('IDM', NULL, 1, 'Gramlich', 'Corporate Finance', '0106', 2, 4),\n" +
"('IDM', NULL, 1, 'Gramlich', 'Corporate Finance', '0131', 2, 5),\n" +
"('O', NULL, 2, 'Armatowski', 'Projektmanagement', 'G1 0.05', 5, 2),\n" +
"('O', NULL, 2, 'Armatowski', 'Projektmanagement', 'G1 0.05', 5, 3),\n" +
"('O', NULL, 6, 'Heinrich', 'LabView', 'G1 2.36', 2, 2),\n" +
"('O', NULL, 6, 'Heinrich', 'LabView', 'G1 2.36', 3, 3),\n" +
"('O', NULL, 1, 'Varney', 'Elektrotechnik Grundlagen Tutorium', 'G1 0.20', 2, 1),\n" +
"('PH', NULL, NULL, 'Heinrich', 'Matlab / Simulink', 'G1 2.36', 4, 4),\n" +
"('PH', NULL, NULL, 'Heinrich', 'Matlab / Simulink', 'G1 2.36', 4, 5),\n" +
"('O', NULL, 1, 'Oder', 'Mathematik 1 Tutorium', 'G1 1.23', 3, 3),\n" +
"('PH', NULL, NULL, 'Heinrich', 'Photonic Detectors and Devices', 'G1 1.32', 2, 4),\n" +
"('PH', NULL, NULL, 'Heinrich', 'Photonic Detectors and Devices', 'G1 1.32', 2, 5),\n" +
"('O', NULL, 2, 'Kulisch-Huep', 'Mathematik 2 Tutorium', 'G1 0.02', 5, 5),\n" +
"('VMM', NULL, 4, 'Albrecht', 'Dünnschichttechnik', '0125', 3, 3),\n" +
"('VMM', NULL, 4, 'Albrecht', 'Dünnschichttechnik', '0106', 5, 3),\n" +
"('V', NULL, 4, 'Albrecht', 'Dünnschichttechnik', '0125', 3, 3),\n" +
"('V', NULL, 4, 'Albrecht', 'Dünnschichttechnik', '0106', 5, 3),\n" +
"('P', NULL, 4, 'Gretzschel', 'Systemdynamik', '0101', 2, 3),\n" +
"('P', NULL, 4, 'Gretzschel', 'Systemdynamik', '0106', 4, 1),\n" +
"('V', NULL, 6, 'Delarbre', 'Pulvermetallische Werkstoffe', '0119', 6, 2),\n" +
"('V', NULL, 6, 'Delarbre', 'Pulvermetallische Werkstoffe', '0119', 6, 3),\n" +
"('V', NULL, 6, 'Delarbre', 'Pulvermetallische Werkstoffe', '0119', 6, 4),\n" +
"('M', 'E, K, R', 6, 'Gretzschel', 'Maschinendynamik', '0101', 2, 3),\n" +
"('M', 'E, K, R', 6, 'Gretzschel', 'Maschinendynamik', '0106', 4, 1),\n" +
"('IN', 'IS, WI, MI, SE', 4, 'Rössel', 'Softskills', 'G2 0.37', 1, 6),\n" +
"('MP', NULL, 7, 'Kallien', 'Gießereitechnik 2', '0105, 0289', 4, 4),\n" +
"('MP', NULL, 7, 'Kallien', 'Gießereitechnik 2', '0106, 0289', 4, 3),\n" +
"('V', NULL, 6, 'Delarbre', 'Pulvermetallische Werkstoffe', '0119', 6, 5),\n" +
"('VMM', NULL, 6, 'Delarbre', 'Pulvermetallische Werkstoffe', '0119', 6, 2),\n" +
"('VMM', NULL, 6, 'Delarbre', 'Pulvermetallische Werkstoffe', '0119', 6, 3),\n" +
"('VMM', NULL, 6, 'Delarbre', 'Pulvermetallische Werkstoffe', '0119', 6, 4),\n" +
"('VMM', NULL, 6, 'Delarbre', 'Pulvermetallische Werkstoffe', '0119', 6, 5),\n" +
"('PEF', NULL, NULL, 'Merkel', 'FEM  und Übungen', '0273', 2, 2),\n" +
"('TME', NULL, NULL, 'Merkel', 'FEM  und Übungen', '0273', 2, 2),\n" +
"('TMP', NULL, NULL, 'Merkel', 'FEM  und Übungen', '0273', 2, 2),\n" +
"('LBM', NULL, NULL, 'Merkel', 'FEM  und Übungen', '0273', 2, 2),\n" +
"('IST', NULL, NULL, 'Richter', 'Strategy Project', '0233', 3, 4),\n" +
"('IST', NULL, NULL, 'Richter', 'Strategy Project', '0233', 3, 5),\n" +
"('E_VW', NULL, NULL, 'Hofmann (E)', 'Studiengangsbesprechung', 'G2 0.28', 2, 4),\n" +
"('E_VW', NULL, NULL, 'Bartel', 'Studiengangsbesprechung', 'G2 0.28', 2, 4),\n" +
"('E_VW', NULL, NULL, 'Bürkle', 'Studiengangsbesprechung', 'G2 0.28', 2, 4),\n" +
"('E_VW', NULL, NULL, 'Bantel', 'Studiengangsbesprechung', 'G2 0.28', 2, 4),\n" +
"('E_VW', NULL, NULL, 'Horn', 'Studiengangsbesprechung', 'G2 0.28', 2, 4),\n" +
"('E_VW', NULL, NULL, 'Kleppmann', 'Studiengangsbesprechung', 'G2 0.28', 2, 4),\n" +
"('E_VW', NULL, NULL, 'Müller', 'Studiengangsbesprechung', 'G2 0.28', 2, 4),\n" +
"('E_VW', NULL, NULL, 'Schüle', 'Studiengangsbesprechung', 'G2 0.28', 2, 4),\n" +
"('E_VW', NULL, NULL, 'Seelmann', 'Studiengangsbesprechung', 'G2 0.28', 2, 4),\n" +
"('E_VW', NULL, NULL, 'Werner', 'Studiengangsbesprechung', 'G2 0.28', 2, 4),\n" +
"('E_VW', NULL, NULL, 'Hofmann (E)', 'Bachelor Vorträge', 'G2 2.01', 2, 5),\n" +
"('E_VW', NULL, NULL, 'Bantel', 'Bachelor Vorträge', 'G2 2.01', 2, 5),\n" +
"('E_VW', NULL, NULL, 'Bartel', 'Bachelor Vorträge', 'G2 2.01', 2, 5),\n" +
"('E_VW', NULL, NULL, 'Horn', 'Bachelor Vorträge', 'G2 2.01', 2, 5),\n" +
"('E_VW', NULL, NULL, 'Kleppmann', 'Bachelor Vorträge', 'G2 2.01', 2, 5),\n" +
"('E_VW', NULL, NULL, 'Müller', 'Bachelor Vorträge', 'G2 2.01', 2, 5),\n" +
"('E_VW', NULL, NULL, 'Schüle', 'Bachelor Vorträge', 'G2 2.01', 2, 5),\n" +
"('E_VW', NULL, NULL, 'Seelmann', 'Bachelor Vorträge', 'G2 2.01', 2, 5),\n" +
"('E_VW', NULL, NULL, 'Steinhart', 'Bachelor Vorträge', 'G2 2.01', 2, 5),\n" +
"('E_VW', NULL, NULL, 'Werner', 'Bachelor Vorträge', 'G2 2.01', 2, 5),\n" +
"('ET', NULL, 2, 'Kulisch-Huep', 'Mathematik 2 Tutorium', 'G2 0.21', 3, 1),\n" +
"('ET', NULL, 2, 'Kulisch-Huep', 'Mathematik 2 Tutorium', 'G2 0.01', 5, 3),\n" +
"('ET', NULL, 3, 'Horn', 'Nachrichtentechnik', 'G2 1.01', 1, 3),\n" +
"('ET', NULL, 3, 'Horn', 'Nachrichtentechnik', 'G2 2.01', 5, 2),\n" +
"('ET', NULL, 3, 'Horn', 'Nachrichtentechnik', 'G2 2.01', 5, 3),\n" +
"('E', 'MK', 4, 'Seelmann', 'Videotechnik', 'G2 2.39', 2, 1),\n" +
"('E', 'MK', 4, 'Seelmann', 'Videotechnik', 'G2 2.39', 4, 4),\n" +
"('ET', 'IK', 4, 'Seelmann', 'Videotechnik', 'G2 2.39', 2, 1),\n" +
"('ET', 'IK', 4, 'Seelmann', 'Videotechnik', 'G2 2.39', 4, 4),\n" +
"('E', 'EEE', 4, 'Zippel Fabian', 'Energiewirtschaft', 'G2 0.21', 5, 1),\n" +
"('E', 'EEE', 4, 'Zippel Fabian', 'Energiewirtschaft', 'G2 0.21', 5, 2),\n" +
"('ET', 'EE', 4, 'Zippel Fabian', 'Energiewirtschaft', 'G2 0.21', 5, 1),\n" +
"('ET', 'EE', 4, 'Zippel Fabian', 'Energiewirtschaft', 'G2 0.21', 5, 2),\n" +
"('ZDO', NULL, NULL, 'Baur E.', 'Deutsch als Fremdsprache B2 / C1', '0201', 1, 5),\n" +
"('ZDO', NULL, NULL, 'Baur E.', 'Deutsch als Fremdsprache B2 / C1', '0114', 3, 5),\n" +
"('ZSO', NULL, NULL, 'Conde', 'Spanisch A2', '0124', 4, 4),\n" +
"('ZSO', NULL, NULL, 'Conde', 'Spanisch A2', '0124', 4, 5),\n" +
"('ZEO', NULL, NULL, 'Düwel', 'Technical English B2', '0120', 3, 5),\n" +
"('ET', NULL, 4, 'Strauß Harald', 'Systematik der nachhaltigen Entwicklung', 'G2 0.01', 3, 5),\n" +
"('ET', NULL, 4, 'Strauß Harald', 'Systematik der nachhaltigen Entwicklung', 'G2 0.01', 3, 6),\n" +
"('ET', NULL, 6, 'Strauß Harald', 'Systematik der nachhaltigen Entwicklung', 'G2 0.01', 3, 5),\n" +
"('ET', NULL, 6, 'Strauß Harald', 'Systematik der nachhaltigen Entwicklung', 'G2 0.01', 3, 6),\n" +
"('ET', 'EE, IE, IK', 4, 'Strauß Harald', 'Systematik der nachhaltigen Entwicklung', 'G2 0.01', 3, 5),\n" +
"('ET', 'EE, IE, IK', 4, 'Strauß Harald', 'Systematik der nachhaltigen Entwicklung', 'G2 0.01', 3, 6),\n" +
"('E', 'EEE, TI, IFE, MK', 4, 'Strauß Harald', 'Systematik der nachhaltigen Entwicklung', 'G2 0.01', 3, 5),\n" +
"('E', 'EEE, TI, IFE, MK', 4, 'Strauß Harald', 'Systematik der nachhaltigen Entwicklung', 'G2 0.01', 3, 6),\n" +
"('E', 'IFE, MK, TI', 4, 'Horn', 'Übertragungstechnik', 'G2 1.01', 1, 3),\n" +
"('E', 'IFE, MK, TI', 4, 'Horn', 'Übertragungstechnik', 'G2 2.01', 5, 2),\n" +
"('E', 'IFE, MK, TI', 4, 'Horn', 'Übertragungstechnik', 'G2 2.01', 5, 3),\n" +
"('ET', NULL, 7, 'Strauß Harald', 'Systematik der nachhaltigen Entwicklung', 'G2 0.01', 3, 5),\n" +
"('ET', NULL, 7, 'Strauß Harald', 'Systematik der nachhaltigen Entwicklung', 'G2 0.01', 3, 6),\n" +
"('V', NULL, 3, 'Möckel', 'Chemielabor', '0101', 3, 1),\n" +
"('V', NULL, 3, 'Möckel', 'Chemielabor', '0174', 3, 2),\n" +
"('V', NULL, 3, 'Möckel', 'Chemielabor', '0174', 3, 3),\n" +
"('V', NULL, 3, 'Möckel', 'Chemielabor', '0174', 3, 4),\n" +
"('V', NULL, 3, 'Möckel', 'Chemielabor', '0174', 3, 5),\n" +
"('V', NULL, 3, 'Möckel', 'Chemielabor', '0174', 3, 6),\n" +
"('VMG', NULL, 3, 'Möckel', 'Chemielabor', '0101', 3, 1),\n" +
"('VMG', NULL, 3, 'Möckel', 'Chemielabor', '0174', 3, 2),\n" +
"('VMG', NULL, 3, 'Möckel', 'Chemielabor', '0174', 3, 3),\n" +
"('VMG', NULL, 3, 'Möckel', 'Chemielabor', '0174', 3, 4),\n" +
"('VMG', NULL, 3, 'Möckel', 'Chemielabor', '0174', 3, 5),\n" +
"('VMG', NULL, 3, 'Möckel', 'Chemielabor', '0174', 3, 6),\n" +
"('V', NULL, 3, 'Ladwein', 'Elektrochemie - Labor', '0174', 3, 1),\n" +
"('V', NULL, 3, 'Ladwein', 'Elektrochemie - Labor', '0174', 3, 2),\n" +
"('V', NULL, 3, 'Ladwein', 'Elektrochemie - Labor', '0174', 3, 3),\n" +
"('V', NULL, 3, 'Ladwein', 'Elektrochemie - Labor', '0174', 3, 4),\n" +
"('V', NULL, 3, 'Ladwein', 'Elektrochemie - Labor', '0174', 3, 5),\n" +
"('V', NULL, 3, 'Ladwein', 'Elektrochemie - Labor', '0174', 3, 6),\n" +
"('VMG', NULL, 3, 'Ladwein', 'Elektrochemie - Labor', '0174', 3, 1);" 
,"INSERT INTO g02_vorlesungsplan (stg, schwerpunkt, semester, dozent, kurs, raum, tagnr,  zeitblocknr) VALUES\n" +
            "('VMG', NULL, 3, 'Ladwein', 'Elektrochemie - Labor', '0174', 3, 2),\n" +
"('VMG', NULL, 3, 'Ladwein', 'Elektrochemie - Labor', '0174', 3, 3),\n" +
"('VMG', NULL, 3, 'Ladwein', 'Elektrochemie - Labor', '0174', 3, 4),\n" +
"('VMG', NULL, 3, 'Ladwein', 'Elektrochemie - Labor', '0174', 3, 5),\n" +
"('VMG', NULL, 3, 'Ladwein', 'Elektrochemie - Labor', '0174', 3, 6),\n" +
"('V', NULL, 6, 'Ladwein', 'Korrosionslabor', '0174', 3, 4),\n" +
"('V', NULL, 6, 'Ladwein', 'Korrosionslabor', '0174', 3, 5),\n" +
"('V', NULL, 6, 'Ladwein', 'Korrosionslabor', '0174', 3, 6),\n" +
"('C', NULL, 6, 'Neusüß', NULL, '0122', 2, 1),\n" +
"('ABC', NULL, 2, 'Flottmann', 'Brückenkurs Analytische Chemie', '0122', 2, 2),\n" +
"('IDM', NULL, 1, 'Gramlich', 'Fertigungstechnik', '0701', 4, 4),\n" +
"('IDM', NULL, 1, 'Gramlich', 'Fertigungstechnik', '0701', 4, 5),\n" +
"('VI', NULL, 4, 'Weber M.', 'Projektmanagement', 'AH 0.01', 2, 4),\n" +
"('VI', NULL, 4, 'Weber M.', 'Projektmanagement', 'AH 0.01', 2, 5),\n" +
"('VMM', NULL, 4, 'Weber M.', 'Projektmanagement', 'AH 0.01', 2, 4),\n" +
"('VMM', NULL, 4, 'Weber M.', 'Projektmanagement', 'AH 0.01', 2, 5),\n" +
"('V', NULL, 4, 'Weber M.', 'Projektmanagement', 'AH 0.01', 2, 4),\n" +
"('V', NULL, 4, 'Weber M.', 'Projektmanagement', 'AH 0.01', 2, 5),\n" +
"('VMM', NULL, 4, 'Subek', 'Fertigungsverfahren', 'AH 0.01', 2, 4),\n" +
"('VMM', NULL, 4, 'Subek', 'Fertigungsverfahren', 'AH 0.01', 2, 5),\n" +
"('V', NULL, 4, 'Subek', 'Fertigungsverfahren', 'AH 0.01', 2, 4),\n" +
"('V', NULL, 4, 'Subek', 'Fertigungsverfahren', 'AH 0.01', 2, 5),\n" +
"('VI', NULL, 2, 'Subek', 'Fertigungsverfahren', 'AH 0.01', 2, 4),\n" +
"('VI', NULL, 2, 'Subek', 'Fertigungsverfahren', 'AH 0.01', 2, 5),\n" +
"('VMM', NULL, 3, 'Subek', 'Fertigungsverfahren', 'AH 0.01', 2, 4),\n" +
"('VMM', NULL, 3, 'Subek', 'Fertigungsverfahren', 'AH 0.01', 2, 5),\n" +
"('VMG', NULL, 3, 'Subek', 'Fertigungsverfahren', 'AH 0.01', 2, 4),\n" +
"('VMG', NULL, 3, 'Subek', 'Fertigungsverfahren', 'AH 0.01', 2, 5),\n" +
"('IN_VW', NULL, NULL, 'Bantel', 'Dienstbesprechung', 'G2 1.03', 2, 4),\n" +
"('IN_VW', NULL, NULL, 'Dietrich', 'Dienstbesprechung', 'G2 1.03', 2, 4),\n" +
"('IN_VW', NULL, NULL, 'Heinlein', 'Dienstbesprechung', 'G2 1.03', 2, 4),\n" +
"('IN_VW', NULL, NULL, 'Hellmann', 'Dienstbesprechung', 'G2 1.03', 2, 4),\n" +
"('IN_VW', NULL, NULL, 'Klauck', 'Dienstbesprechung', 'G2 1.03', 2, 4),\n" +
"('IN_VW', NULL, NULL, 'Küpper', 'Dienstbesprechung', 'G2 1.03', 2, 4),\n" +
"('IN_VW', NULL, NULL, 'Lecon', 'Dienstbesprechung', 'G2 1.03', 2, 4),\n" +
"('IN_VW', NULL, NULL, 'Oberhauser', 'Dienstbesprechung', 'G2 1.03', 2, 4),\n" +
"('IN_VW', NULL, NULL, 'Thierauf', 'Dienstbesprechung', 'G2 1.03', 2, 4),\n" +
"('IN_VW', NULL, NULL, 'Werthebach', 'Dienstbesprechung', 'G2 1.03', 2, 4),\n" +
"('IN_VW', NULL, NULL, 'Karg', 'Dienstbesprechung', 'G2 1.03', 2, 4),\n" +
"('C', NULL, 6, 'Flottmann, Neusüß', 'Seminar Analytische Chemie', '0122, 0215, 0215', 2, 1),\n" +
"('ET', 'EE, IK, IE', 6, 'Strauß Harald', 'Systematik der nachhaltigen Entwicklung', 'G2 0.01', 3, 5),\n" +
"('ET', 'EE, IK, IE', 6, 'Strauß Harald', 'Systematik der nachhaltigen Entwicklung', 'G2 0.01', 3, 6),\n" +
"('E', 'EE, TI, IFE, MK', 6, 'Strauß Harald', 'Systematik der nachhaltigen Entwicklung', 'G2 0.01', 3, 5),\n" +
"('E', 'EE, TI, IFE, MK', 6, 'Strauß Harald', 'Systematik der nachhaltigen Entwicklung', 'G2 0.01', 3, 6),\n" +
"('ET', 'IE, IK', 7, 'Strauß Harald', 'Systematik der nachhaltigen Entwicklung', 'G2 0.01', 3, 5),\n" +
"('ET', 'IE, IK', 7, 'Strauß Harald', 'Systematik der nachhaltigen Entwicklung', 'G2 0.01', 3, 6),\n" +
"('M', 'E, K, R', 6, 'Kley, Thomisch', 'Konstruktionslehre 2 - Übungen', 'AH -1.01', 1, 5),\n" +
"('M', 'E, K, R', 6, 'Kley, Thomisch', 'Konstruktionslehre 2 - Übungen', 'AH -1.01', 1, 6),\n" +
"('MP', NULL, 1, 'Kalhöfer', 'Produktionsverfahren', 'AH 1.02', 1, 3),\n" +
"('MP', NULL, 1, 'Kalhöfer', 'Produktionsverfahren', '0273', 2, 1),\n" +
"('MW', NULL, 3, 'Kalhöfer', 'Produktionsverfahren', 'AH 1.02', 1, 3),\n" +
"('MW', NULL, 3, 'Kalhöfer', 'Produktionsverfahren', '0273', 2, 1),\n" +
"('MW', NULL, 3, 'Dittmar (LB)', 'Elektrotechnik', '0102', 2, 3),\n" +
"('MW', NULL, 3, 'Dittmar (LB)', 'Elektrotechnik', '0102', 2, 4),\n" +
"('MW', NULL, 7, 'Heilmann', 'Unternehmensführung', '0213', 2, 4),\n" +
"('MW', NULL, 7, 'Heilmann', 'Unternehmensführung', '0213', 2, 5),\n" +
"('ET', NULL, 6, 'Bartel', 'Industrial Control Systems Security', 'G2 0.27', 4, 1),\n" +
"('ET', NULL, 7, 'Bartel', 'Industrial Control Systems Security', 'G2 0.27', 4, 1),\n" +
"('PEF', NULL, NULL, 'Merkel', 'FEM und Übungen', '0289', 1, 1),\n" +
"('TMP', NULL, NULL, 'Merkel', 'FEM und Übungen', '0289', 1, 1),\n" +
"('TME', NULL, NULL, 'Merkel', 'FEM und Übungen', '0289', 1, 1),\n" +
"('GMF', NULL, NULL, 'Berger (F)', 'Rapid Product Development', '0120', 4, 4),\n" +
"('TME', NULL, NULL, 'Berger (F)', 'Generative Verfahren', '0120', 4, 4),\n" +
"('LBM', NULL, NULL, 'Berger (F)', 'Generative Verfahren', '0120', 4, 4),\n" +
"('TMP', NULL, NULL, 'Berger (F)', 'Generative Verfahren', '0120', 4, 4),\n" +
"('PEF', NULL, NULL, 'Berger (F)', 'Digitale Produktentstehung u. Fertigung', '0120', 4, 4),\n" +
"('ET', NULL, 3, 'Schulz (E)', 'Elektrotechnik Tutorium', 'G2 1.44', 4, 1),\n" +
"('E', 'EEE', 4, 'Zippel Fabian', 'Energiewirtschaft', 'G2 0.21', 4, 5),\n" +
"('E', 'EEE', 4, 'Zippel Fabian', 'Energiewirtschaft', 'G2 0.21', 4, 6),\n" +
"('ET', 'EE', 4, 'Zippel Fabian', 'Energiewirtschaft', 'G2 0.21', 4, 5),\n" +
"('ET', 'EE', 4, 'Zippel Fabian', 'Energiewirtschaft', 'G2 0.21', 4, 6),\n" +
"('VMM', NULL, 4, 'Dittmar (LB)', 'Elektrotechnik', '0102', 3, 4),\n" +
"('GF', NULL, 3, 'Hörmann', 'Programmierübungen Gruppe B', 'G1 1.28', 1, 4),\n" +
"('E', 'IFE', 7, 'Steinhart', 'Dynamik elektr. Antriebe', 'G2 1.44', 5, 2),\n" +
"('E', 'IFE', 7, 'Steinhart', 'Dynamik elektr. Antriebe', 'G2 1.44', 5, 3),\n" +
"('K', NULL, 3, 'Walcher', 'Polymerverarbeitung 1', '0701', 4, 3),\n" +
"('B', NULL, 4, 'Wiedenmann', 'Flexblock 1', '0112', 2, 6),\n" +
"('M', NULL, 1, '?', 'Tutorium Mathematik I', '0101', 3, 4),\n" +
"('ET', 'IK', 6, 'Seelmann', 'Informationsth. / Datenkompress.', 'G2 2.39', 2, 2),\n" +
"('ET', 'IK', 6, 'Seelmann', 'Informationsth. / Datenkompress.', 'G2 2.39', 4, 1),\n" +
"('ET', 'EE, IE', 6, 'Bartel', 'Industrial Control Systems Security', 'G2 0.27', 4, 1),\n" +
"('ET', 'EE, IK, IE', 7, 'Bartel', 'Industrial Control Systems Security', 'G2 0.27', 4, 1),\n" +
"('E', 'EE, IFE', 6, 'Steinhart', 'Dynamik elektr. Antriebe', 'G2 1.44', 5, 2),\n" +
"('E', 'EE, IFE', 6, 'Steinhart', 'Dynamik elektr. Antriebe', 'G2 1.44', 5, 3),\n" +
"('E', 'EEE, EIT', 3, 'Schulz (E)', 'Elektrotechnik Tutorium', 'G2 1.44', 4, 1),\n" +
"('ET', 'IE, IE_W', 6, 'Steinhart', 'Dynamik elektr. Antriebe', 'G2 1.44', 5, 2),\n" +
"('ET', 'IE, IE_W', 6, 'Steinhart', 'Dynamik elektr. Antriebe', 'G2 1.44', 5, 3),\n" +
"('E', 'IFE, TI, MK', 4, 'Bartel', 'Industrial Control Systems Security', 'G2 0.27', 4, 1),\n" +
"('IN', 'MI, SE', 6, 'Bantel', 'Compilerbau', 'G2 1.03', 1, 3),\n" +
"('IN', 'MI, SE', 6, 'Bantel', 'Compilerbau', 'G2 1.01', 4, 4),\n" +
"('IN', 'MI, SE', 7, 'Bantel', 'Compilerbau', 'G2 1.03', 1, 3),\n" +
"('IN', 'MI, SE', 7, 'Bantel', 'Compilerbau', 'G2 1.01', 4, 4),\n" +
"('E', 'MK, TI', 6, 'Bartel', 'Industrial Control Systems Security', 'G2 0.27', 4, 1),\n" +
"('E', 'MK, TI', 7, 'Seelmann', 'Informationsth. / Datenkompress.', 'G2 2.39', 2, 2),\n" +
"('E', 'MK, TI', 7, 'Seelmann', 'Informationsth. / Datenkompress.', 'G2 2.39', 4, 1),\n" +
"('P', NULL, 6, 'Bentsche, Wagner, Zorniger', 'Steuerungstechnik u. Labor', '0187', 1, 4),\n" +
"('P', NULL, 6, 'Bentsche, Wagner, Zorniger', 'Steuerungstechnik u. Labor', '0187', 1, 5),\n" +
"('VMM', NULL, 4, 'Bernthaler, Reiter', 'Strukturwerkstofflabor', '0185', 2, 1),\n" +
"('VMM', NULL, 4, 'Bernthaler, Reiter', 'Strukturwerkstofflabor', '0185', 2, 2),\n" +
"('VMM', NULL, 4, 'Bernthaler, Reiter', 'Strukturwerkstofflabor', '0185', 2, 3),\n" +
"('V', NULL, 4, 'Bernthaler, Reiter', 'Strukturwerkstofflabor', '0185', 2, 1),\n" +
"('V', NULL, 4, 'Bernthaler, Reiter', 'Strukturwerkstofflabor', '0185', 2, 2),\n" +
"('V', NULL, 4, 'Bernthaler, Reiter', 'Strukturwerkstofflabor', '0185', 2, 3),\n" +
"('VMM', NULL, 1, 'Oder', 'Mathematik Tutorium', '0120', 4, 1),\n" +
"('V', NULL, 1, 'Oder', 'Mathematik Tutorium', '0120', 4, 1),\n" +
"('VMG', NULL, 1, 'Oder', 'Mathematik Tutorium', '0120', 4, 1),\n" +
"('O', NULL, 2, 'Nuding', 'Mathematik 2 Übungen', 'G1 1.20', 1, 2),\n" +
"('I', NULL, 2, 'Nissen', 'Mikroökonomik', '0201', 3, 1),\n" +
"('I', NULL, 2, 'Nissen', 'Mikroökonomik', '0201', 3, 2),\n" +
"('I', NULL, 2, 'Schlunsky', 'Tutorium : Bilanzierung', 'AH -1.01', 1, 3),\n" +
"('B', NULL, 2, 'Conde', 'Spanisch A1', '0222', 5, 3),\n" +
"('B', NULL, 2, 'Conde', 'Spanisch A1', '0222', 5, 4),\n" +
"('I', NULL, 2, 'Keller B.', 'Tutorium: statistische und empirische Methoden', '0701', 1, 4),\n" +
"('I', NULL, 2, 'Keller B.', 'Tutorium: statistische und empirische Methoden', '0701', 1, 5),\n" +
"('A', NULL, 1, '?', 'Tutorium', NULL, 3, 1),\n" +
"('A', NULL, 1, '?', 'Tutorium', NULL, 3, 2),\n" +
"('A', NULL, 3, 'Buser', 'Sehfunktionen / Lichttechnik Psychoph.', 'G4 -1.01', 1, 1),\n" +
"('A', NULL, 3, 'Buser', 'Sehfunktionen / Lichttechnik Psychoph.', 'G4 -1.01', 2, 1),\n" +
"('A', NULL, 3, '?', 'Objektive / Subjektive Refraktion Übungen', 'G4 1.07', 2, 4),\n" +
"('A', NULL, 3, '?', 'Objektive / Subjektive Refraktion Übungen', 'G4 1.07', 2, 5),\n" +
"('MWI', NULL, 2, 'Hänisch', 'Big Data - Technologien und Anwendungsfälle', '0126', 2, 4),\n" +
"('MWI', NULL, 2, 'Hänisch', 'Big Data - Technologien und Anwendungsfälle', '0201', 2, 5),\n" +
"('MWI', NULL, 2, 'Zeidler', 'Business Analytics', '0201', 1, 2),\n" +
"('MWI', NULL, 2, 'Zeidler', 'Business Analytics', '0201', 1, 3),\n" +
"('MWI', NULL, 2, 'Zeidler', 'Business Analytics', '0201', 1, 4),\n" +
"('MWI', NULL, 2, 'Zeidler', 'Business Analytics', '0124', 5, 4),\n" +
"('MWI', NULL, 2, 'Zeidler', 'Business Analytics', '0124', 5, 5),\n" +
"('MWI', NULL, 2, 'Gentsch', 'Online-Marketing & We-Analytics', '0126', 4, 4),\n" +
"('MWI', NULL, 2, 'Gentsch', 'Online-Marketing & We-Analytics', '0126', 4, 5),\n" +
"('MWI', NULL, 2, 'Richter', 'Unternehmensführung und -steuerung', '0129', 3, 2),\n" +
"('MWI', NULL, 2, 'Richter', 'Unternehmensführung und -steuerung', '0234', 3, 3),\n" +
"('IDM', NULL, 1, 'Richter', 'Unternehmensstrategie  /  Controlling', '0129', 3, 2),\n" +
"('IDM', NULL, 1, 'Richter', 'Unternehmensstrategie  /  Controlling', '0234', 3, 3),\n" +
"('GME', NULL, NULL, 'Zimmermann', 'Physik-Labor zu Grdl. Kern- / Teilchen- / Astrophysik', 'G1 0.06', 3, 5),\n" +
"('GME', NULL, NULL, 'Zimmermann', 'Physik-Labor zu Grdl. Kern- / Teilchen- / Astrophysik', 'G1 0.06', 3, 6),\n" +
"('GMF', NULL, NULL, 'Zimmermann', 'Physik-Labor zu Grdl. Kern- / Teilchen- / Astrophysik', 'G1 0.06', 3, 5),\n" +
"('GMF', NULL, NULL, 'Zimmermann', 'Physik-Labor zu Grdl. Kern- / Teilchen- / Astrophysik', 'G1 0.06', 3, 6),\n" +
"('IDM', NULL, 1, 'Gramlich', 'Produktionsplanung & Systeme', '0131', 1, 1),\n" +
"('IDM', NULL, 1, 'Gramlich', 'Produktionsplanung & Systeme', '0131', 1, 2),\n" +
"('IDM', NULL, 1, 'Demuth', 'Internationale Wirtschaft', '0233', 5, 4),\n" +
"('IDM', NULL, 1, 'Demuth', 'Internationale Wirtschaft', '0233', 5, 5),\n" +
"('IST', NULL, NULL, 'Demuth', 'Internationale Wirtschaft', '0233', 5, 4),\n" +
"('IST', NULL, NULL, 'Demuth', 'Internationale Wirtschaft', '0233', 5, 5),\n" +
"('F', NULL, 4, 'Eichinger', 'Konstruieren mit Kunststoffen', 'G1 0.01', 2, 4),\n" +
"('MekA', NULL, 3, 'Eichinger', 'Konstruieren mit Kunststoffen', 'G1 0.01', 2, 4),\n" +
"('IDM', NULL, 1, 'Ulsamer', 'Technische Produktentwickllung', '0202', 1, 4),\n" +
"('IDM', NULL, 1, 'Ulsamer', 'Technische Produktentwickllung', '0202', 1, 5),\n" +
"('F', NULL, 4, 'Eichinger', 'Konstruieren mit Kunststoffen', 'G1 1.32', 1, 3),\n" +
"('MekA', NULL, 3, 'Eichinger', 'Konstruieren mit Kunststoffen', 'G1 1.32', 1, 3),\n" +
"('FR', NULL, 7, 'Wendland', 'Allgemeine Technikgestaltung', 'G1 1.20', 2, 1),\n" +
"('IDM', NULL, 1, 'Klaas', 'Elektromobilität', '0103', 5, 6),\n" +
"('ET', NULL, 1, 'Elser', 'Praktische Elektronik', 'G2 2.26', 3, 1),\n" +
"('ET', NULL, 1, 'Elser', 'Praktische Elektronik', 'G2 2.26', 3, 2),\n" +
"('ET', NULL, 1, 'Elser', 'Praktische Elektronik', 'G2 2.26', 3, 3),\n" +
"('ET', NULL, 1, 'Hahn-Dambacher', NULL, 'G2 2.26', 3, 1),\n" +
"('ET', NULL, 1, 'Hahn-Dambacher', NULL, 'G2 2.26', 3, 2),\n" +
"('ET', NULL, 1, 'Hahn-Dambacher', NULL, 'G2 2.26', 3, 3),\n" +
"('ET', NULL, 1, 'Kolb', NULL, 'G2 2.26', 3, 1),\n" +
"('ET', NULL, 1, 'Kolb', NULL, 'G2 2.26', 3, 2),\n" +
"('ET', NULL, 1, 'Kolb', NULL, 'G2 2.26', 3, 3),\n" +
"('ET', NULL, 2, 'Lott', 'Werkstoffkunde', 'G2 0.37', 4, 1),\n" +
"('ET', NULL, 2, 'Lott', 'Werkstoffkunde', 'G2 0.37', 4, 2),\n" +
"('ET', 'IE_W', 6, 'Lott', 'Werkstoffkunde', 'G2 0.37', 4, 1),\n" +
"('ET', 'IE_W', 6, 'Lott', 'Werkstoffkunde', 'G2 0.37', 4, 2),\n" +
"('F', NULL, 6, 'Eichinger', 'Industrieprojekt', 'G1 0.22', 5, 2),\n" +
"('F', NULL, 6, 'Eichinger', 'Industrieprojekt', 'G1 0.20', 5, 3),\n" +
"('F', NULL, 7, 'Eichinger', 'Industrieprojekt', 'G1 0.22', 5, 2),\n" +
"('F', NULL, 7, 'Eichinger', 'Industrieprojekt', 'G1 0.20', 5, 3),\n" +
"('GF', NULL, 1, 'PH Dozent', 'PH Schwäbisch Gmünd', NULL, 2, 1),\n" +
"('GF', NULL, 1, 'PH Dozent', 'PH Schwäbisch Gmünd', NULL, 2, 2),\n" +
"('GF', NULL, 1, 'PH Dozent', 'PH Schwäbisch Gmünd', NULL, 2, 3),\n" +
"('GE', NULL, 2, 'PH Dozent', 'PH Schwäbisch Gmünd', NULL, 2, 1),\n" +
"('GE', NULL, 2, 'PH Dozent', 'PH Schwäbisch Gmünd', NULL, 2, 2),\n" +
"('GE', NULL, 2, 'PH Dozent', 'PH Schwäbisch Gmünd', NULL, 2, 3),\n" +
"('GF', NULL, 3, 'Hörmann', 'Programmierübungen Gruppe B', 'G1 1.28', 5, 2),\n" +
"('F', NULL, 6, 'Höfig', 'Dynamik mechatronischer Systeme', 'G1 1.28', 1, 6),\n" +
"('F', NULL, 6, 'Höfig', 'Dynamik mechatronischer Systeme', 'G1 1.28', 5, 1),\n" +
"('F', NULL, 7, 'Höfig', 'Dynamik mechatronischer Systeme', 'G1 1.28', 1, 6),\n" +
"('F', NULL, 7, 'Höfig', 'Dynamik mechatronischer Systeme', 'G1 1.28', 5, 1),\n" +
"('MekA', NULL, 5, 'Höfig', 'Dynamik mechatronischer Systeme', 'G1 1.28', 1, 6),\n" +
"('MekA', NULL, 5, 'Höfig', 'Dynamik mechatronischer Systeme', 'G1 1.28', 5, 1),\n" +
"('ET', 'EE', 6, 'Höfig', 'Regelungstechnik 2', 'G2 1.30', 1, 6),\n" +
"('ET', 'EE', 6, 'Höfig', 'Regelungstechnik 2', 'G2 2.33', 5, 1),\n" +
"('ET', 'IE', 4, 'Höfig', 'Regelungstechnik 2', 'G2 1.30', 1, 6),\n" +
"('ET', 'IE', 4, 'Höfig', 'Regelungstechnik 2', 'G2 2.33', 5, 1),\n" +
"('GF', NULL, 7, 'PH Dozent', 'PH Schwäbisch Gmünd', NULL, 2, 1),\n" +
"('GF', NULL, 7, 'PH Dozent', 'PH Schwäbisch Gmünd', NULL, 2, 2),\n" +
"('GF', NULL, 7, 'PH Dozent', 'PH Schwäbisch Gmünd', NULL, 2, 3),\n" +
"('GF', NULL, 7, 'PH Dozent', 'PH Schwäbisch Gmünd', NULL, 2, 4),\n" +
"('GE', NULL, 6, 'PH Dozent', 'PH Schwäbisch Gmünd', NULL, 2, 1),\n" +
"('GE', NULL, 6, 'PH Dozent', 'PH Schwäbisch Gmünd', NULL, 2, 2),\n" +
"('GE', NULL, 6, 'PH Dozent', 'PH Schwäbisch Gmünd', NULL, 2, 3),\n" +
"('GE', NULL, 6, 'PH Dozent', 'PH Schwäbisch Gmünd', NULL, 2, 4),\n" +
"('F', NULL, 1, 'Hörmann', 'Programmierübungen Gruppe A', 'G1 1.28', 4, 5),\n" +
"('MekA', NULL, 1, 'Hörmann', 'Programmierübungen Gruppe A', 'G1 1.28', 4, 5),\n" +
"('GME', NULL, NULL, 'PH Dozent', 'PH Schwäbisch Gmünd', NULL, 2, 4),\n" +
"('GME', NULL, NULL, 'PH Dozent', 'PH Schwäbisch Gmünd', NULL, 2, 5),\n" +
"('GME', NULL, NULL, 'PH Dozent', 'PH Schwäbisch Gmünd', NULL, 2, 6),\n" +
"('GME', NULL, NULL, 'PH Dozent', 'PH Schwäbisch Gmünd', NULL, 3, 1),\n" +
"('GME', NULL, NULL, 'PH Dozent', 'PH Schwäbisch Gmünd', NULL, 3, 2),\n" +
"('GME', NULL, NULL, 'PH Dozent', 'PH Schwäbisch Gmünd', NULL, 3, 3),\n" +
"('GME', NULL, NULL, 'PH Dozent', 'PH Schwäbisch Gmünd', NULL, 4, 1),\n" +
"('GME', NULL, NULL, 'PH Dozent', 'PH Schwäbisch Gmünd', NULL, 4, 2);" 
,"INSERT INTO g02_vorlesungsplan (stg, schwerpunkt, semester, dozent, kurs, raum, tagnr,  zeitblocknr) VALUES\n" +
            "('GMF', NULL, NULL, 'PH Dozent', 'PH Schwäbisch Gmünd', NULL, 2, 4),\n" +
"('GMF', NULL, NULL, 'PH Dozent', 'PH Schwäbisch Gmünd', NULL, 2, 5),\n" +
"('GMF', NULL, NULL, 'PH Dozent', 'PH Schwäbisch Gmünd', NULL, 2, 6),\n" +
"('GMF', NULL, NULL, 'PH Dozent', 'PH Schwäbisch Gmünd', NULL, 3, 1),\n" +
"('GMF', NULL, NULL, 'PH Dozent', 'PH Schwäbisch Gmünd', NULL, 3, 2),\n" +
"('GMF', NULL, NULL, 'PH Dozent', 'PH Schwäbisch Gmünd', NULL, 3, 3),\n" +
"('GMF', NULL, NULL, 'PH Dozent', 'PH Schwäbisch Gmünd', NULL, 4, 1),\n" +
"('GMF', NULL, NULL, 'PH Dozent', 'PH Schwäbisch Gmünd', NULL, 4, 2),\n" +
"('W', NULL, 1, 'Demuth', 'Einführung Allg. VWL / BWL', '0105', 5, 2),\n" +
"('W', NULL, 1, 'Demuth', 'Einführung Allg. VWL / BWL', '0105', 5, 3),\n" +
"('A', NULL, 6, '?', 'W-Sprachkurse Chinesisch', NULL, 1, 6),\n" +
"('A', NULL, 6, '?', 'W-Sprachkurse Chinesisch', NULL, 1, 7),\n" +
"('A', NULL, 7, '?', 'W-Sprachkurse Chinesisch', NULL, 1, 6),\n" +
"('A', NULL, 7, '?', 'W-Sprachkurse Chinesisch', NULL, 1, 7),\n" +
"('A', NULL, 6, 'Baumbach', 'W-Projekt Einstärken', NULL, 3, 1),\n" +
"('A', NULL, 7, 'Baumbach', 'W-Projekt Einstärken', NULL, 3, 1),\n" +
"('A', NULL, 6, 'Nolting', 'W-Informatik-Vertiefung', 'G4 -1.01', 3, 2),\n" +
"('A', NULL, 6, 'Nolting', 'W-Informatik-Vertiefung', 'G4 -1.01', 3, 3),\n" +
"('A', NULL, 6, '?', 'W-Marketing+Controlling (O)', NULL, 3, 2),\n" +
"('A', NULL, 6, '?', 'W-Marketing+Controlling (O)', NULL, 3, 3),\n" +
"('A', NULL, 6, 'Holschbach', 'Praktikum Kontaktlinse 3', 'G4 1.02', 2, 6),\n" +
"('A', NULL, 6, 'Holschbach', 'Praktikum Kontaktlinse 3', 'G4 1.02', 2, 7),\n" +
"('AH', NULL, 6, 'Holschbach', 'Praktikum Kontaktlinse 3', 'G4 1.02', 2, 6),\n" +
"('AH', NULL, 6, 'Holschbach', 'Praktikum Kontaktlinse 3', 'G4 1.02', 2, 7),\n" +
"('A', NULL, 6, '?', 'W-Sprachkurse Englisch B2 Grammar', NULL, 3, 6),\n" +
"('A', NULL, 6, '?', 'W-Sprachkurse Englisch B2 Grammar', NULL, 3, 7),\n" +
"('A', NULL, 7, '?', 'W-Sprachkurse Englisch B2 Grammar', NULL, 3, 6),\n" +
"('A', NULL, 7, '?', 'W-Sprachkurse Englisch B2 Grammar', NULL, 3, 7),\n" +
"('A', NULL, 6, 'Holschbach', 'Kontaktlinse 3', 'G4 -1.01', 3, 4),\n" +
"('AH', NULL, 6, 'Holschbach', 'Kontaktlinse 3', 'G4 -1.01', 3, 4),\n" +
"('A', NULL, 6, '?', 'W-Sprachkurse Technisches Englisch', NULL, 3, 5),\n" +
"('A', NULL, 7, '?', 'W-Sprachkurse Technisches Englisch', NULL, 3, 5),\n" +
"('A', NULL, 6, '?', 'W-Sprachkurse Spanisch A1', NULL, 3, 5),\n" +
"('A', NULL, 7, '?', 'W-Sprachkurse Spanisch A1', NULL, 3, 5),\n" +
"('A', NULL, 7, '?', 'W-Sprachkurse Spanisch A2', NULL, 4, 4),\n" +
"('A', NULL, 7, '?', 'W-Sprachkurse Spanisch A2', NULL, 4, 5),\n" +
"('AM', NULL, 1, 'Limberger', 'W-Projekt-Hören&Sehen', NULL, 1, 4),\n" +
"('AM', NULL, 1, 'Schiefer', 'W-Neuro-Ophthalmologie', NULL, 2, 3),\n" +
"('AM', NULL, 1, 'Holschbach', 'Augenerkrankungen', NULL, 4, 2),\n" +
"('FR', NULL, 3, 'Bartkowiak', '3D-CAD', '0034', 3, 5),\n" +
"('FR', NULL, 3, 'Bartkowiak', '3D-CAD', '0034', 3, 6),\n" +
"('E', 'EE, IFE', 6, 'Höfig', 'Regelungstechnik 2', 'G2 1.30', 1, 6),\n" +
"('E', 'EE, IFE', 6, 'Höfig', 'Regelungstechnik 2', 'G2 2.33', 5, 1),\n" +
"('E', 'EE, IFE', 7, 'Höfig', 'Regelungstechnik 2', 'G2 1.30', 1, 6),\n" +
"('E', 'EE, IFE', 7, 'Höfig', 'Regelungstechnik 2', 'G2 2.33', 5, 1),\n" +
"('G', 'F', 1, 'Bauer M., Wendland, Liebschner, Baur J., Berger (F), Eichinger, Glaser, Glunk, Höfig, Hörmann, Holzwarth, Kazi, Richter C., Schmidt Ho, Schmitt, Weissgerber', 'Studiengangbesprechung F', 'G1 1.06', 2, 3),\n" +
"('P', NULL, 6, 'Feuchter, Wagner (P)', 'CFD', '0234, 0034', 2, 5),\n" +
"('P', NULL, 7, 'Feuchter, Wagner (P)', 'CFD', '0234, 0034', 2, 4),\n" +
"('P', NULL, 7, 'Feuchter, Wagner (P)', 'CFD', '0234, 0034', 2, 5),\n" +
"('AM', NULL, 1, 'Breuninger, Schiefer', 'W-Spezielle Physiologie', 'null, null', 3, 4),\n" +
"('P', NULL, 6, 'Feuchter, Wagner (P)', 'CFD', '0234, 0034', 2, 4),\n" +
"('FR', NULL, 3, 'Ankenbrand', 'Digitalfotografie', 'G1 -1.19b', 3, 2),\n" +
"('FR', NULL, 3, 'Ankenbrand', 'Digitalfotografie', 'G1 -1.19b', 4, 4),\n" +
"('FR', NULL, 3, 'Ankenbrand', 'Digitalfotografie', 'G1 -1.19b', 4, 5),\n" +
"('FR', NULL, 6, 'Wendland', 'Informationsmanagement und kooperative Systeme', 'G1 0.21', 3, 3),\n" +
"('MWI', NULL, 1, 'Gentsch', 'Market Research', '0130', 3, 4),\n" +
"('MWI', NULL, 1, 'Gentsch', 'Market Research', '0130', 3, 5),\n" +
"('F', NULL, 4, 'Kazi', 'Dummy Sensorik XXL', 'G1 1.26', 3, 4),\n" +
"('F', NULL, 4, 'Kazi', 'Dummy Sensorik XXL', 'G1 1.26', 3, 5),\n" +
"('IoT', NULL, 1, 'Schüle', 'Programmieren 1', 'GS1', 3, 2),\n" +
"('IoT', NULL, 1, 'Schüle', 'Programmieren 1', 'GS1', 3, 3),\n" +
"('IoT', NULL, 1, 'Walter', 'Physik', 'GS1', 3, 1),\n" +
"('IoT', NULL, 1, 'Walter', 'Physik', 'GS1', 4, 5),\n" +
"('IoT', NULL, 1, 'Reißer', 'Mathematik 1', 'GS1', 2, 4),\n" +
"('IoT', NULL, 1, 'Reißer', 'Mathematik 1', 'GS1', 2, 5),\n" +
"('IoT', NULL, 1, 'Reißer', 'Mathematik 1', 'GS1', 4, 4),\n" +
"('IoT', NULL, 1, 'Müller', 'Internetprotokolle 1', 'GS1', 1, 4),\n" +
"('IoT', NULL, 1, 'Müller', 'Internetprotokolle 1', 'GS1', 1, 5),\n" +
"('IoT', NULL, 1, 'Bartel', 'Lern- und Arbeitstechniken', 'GS1', 2, 1),\n" +
"('IoT', NULL, 1, 'Bartel', 'Lern- und Arbeitstechniken', 'GS1', 2, 2),\n" +
"('IoT', NULL, 1, 'Kril', 'Design Grundlagen', 'GS1', 4, 1),\n" +
"('IoT', NULL, 1, 'Kril', 'Design Grundlagen', 'GS1', 4, 2),\n" +
"('W', NULL, 2, 'Beck Y.', 'Zulassungstestat', '0121', 4, 1),\n" +
"('WI', NULL, 3, 'Grünewald', 'Wissenschaftliches Arbeiten', '0101', 1, 4),\n" +
"('WI', NULL, 3, 'Grünewald', 'Wissenschaftliches Arbeiten', '0101', 1, 5),\n" +
"('WI', NULL, 3, 'Rössle', 'ERP-Systeme', 'G2 2.01', 2, 3),\n" +
"('WI', NULL, 3, 'Rössle', 'ERP-Systeme', 'G2 2.01', 2, 4),\n" +
"('PH', NULL, NULL, 'Krapp', 'Project /  Soft Skills', 'G1 2.27', 5, 2),\n" +
"('PH', NULL, NULL, 'Krapp', 'Project /  Soft Skills', 'G1 2.27', 5, 3),\n" +
"('PH', NULL, NULL, 'Heinrich', 'Project /  Soft Skills', 'G1 2.27', 5, 2),\n" +
"('PH', NULL, NULL, 'Heinrich', 'Project /  Soft Skills', 'G1 2.27', 5, 3),\n" +
"('WI', NULL, 3, 'Fernandes', 'Grundlagen Informationssysteme', 'G2 2.41', 5, 2),\n" +
"('WI', NULL, 3, 'Fernandes', 'Informationssysteme (Praxis)', 'G2 2.41', 5, 1),\n" +
"('WI', NULL, 3, 'Baumann T.', 'Grundlagen der Informationssystementwicklung', 'G2 1.30', 2, 6),\n" +
"('WI', NULL, 3, 'Baumann T.', 'Informationssystementwicklung (Praxis)', 'G2 1.28', 2, 5),\n" +
"('O', NULL, 1, 'Kulisch-Huep', 'Mathematik 1', 'G1 0.22', 2, 3),\n" +
"('O', NULL, 1, 'Kulisch-Huep', 'Mathematik 1', 'G1 0.22', 2, 4),\n" +
"('O', NULL, 1, 'Kulisch-Huep', 'Mathematik 1 Übungen', 'G1 0.20', 4, 3),\n" +
"('IDM', NULL, 1, 'Grimm-Meyerdierks', 'Geschäftsmodelle', '0231', 1, 3),\n" +
"('IDM', NULL, 1, 'Grimm-Meyerdierks', 'Geschäftsmodelle', 'AH 1.01', 3, 1),\n" +
"('IST', NULL, NULL, 'Grimm-Meyerdierks', 'Geschäftsmodelle', '0231', 1, 3),\n" +
"('IST', NULL, NULL, 'Grimm-Meyerdierks', 'Geschäftsmodelle', 'AH 1.01', 3, 1),\n" +
"('PTC', NULL, 1, 'Leyrer', 'Scientific Project', '0130', 4, 1),\n" +
"('M', 'K', 6, 'Ruf', 'Fahrdynamik', '0211', 5, 3),\n" +
"('P', NULL, 6, 'Ruf', 'Fahrdynamik', '0211', 5, 3),\n" +
"('M', 'R', 6, 'Kropp', 'Nachhaltige Entwicklung', '0130', 2, 1),\n" +
"('O', NULL, 1, 'Hellmuth', 'Physik 1', 'G1 1.23', 2, 2),\n" +
"('O', NULL, 1, 'Hellmuth', 'Physik 1', 'G1 0.01', 3, 2),\n" +
"('ET', NULL, 1, 'Hofmann (E), Seelmann, Müller, Steinhart', 'Ausgewählte Themen der Elektrotechnik', 'G2 0.01', 2, 5),\n" +
"('ET', NULL, 1, 'Hofmann (E), Seelmann, Müller, Steinhart', 'Ausgewählte Themen der Elektrotechnik', 'G2 0.01', 2, 6),\n" +
"('ET', NULL, 2, 'Hofmann (E), Seelmann, Müller, Steinhart', 'Ausgewählte Themen der Elektrotechnik', 'G2 0.01', 2, 5),\n" +
"('ET', NULL, 2, 'Hofmann (E), Seelmann, Müller, Steinhart', 'Ausgewählte Themen der Elektrotechnik', 'G2 0.01', 2, 6),\n" +
"('ET', NULL, 3, 'Hofmann (E), Seelmann, Müller, Steinhart', 'Ausgewählte Themen der Elektrotechnik', 'G2 0.01', 2, 5),\n" +
"('ET', NULL, 3, 'Hofmann (E), Seelmann, Müller, Steinhart', 'Ausgewählte Themen der Elektrotechnik', 'G2 0.01', 2, 6),\n" +
"('O', NULL, 4, 'Moisel, Wagner (O)', 'Einführung in die Lichttechnik', 'G1 1.23', 5, 4),\n" +
"('O', NULL, 4, 'Moisel, Wagner (O)', 'Einführung in die Lichttechnik', 'G1 1.23', 5, 5),\n" +
"('ET', NULL, 3, 'Stelzenmüller', 'Regelungstechnik 1 Matlab Übung', 'G2 1.30', 3, 3),\n" +
"('ET', 'IE_W', 6, 'Stelzenmüller', 'Regelungstechnik 1 Matlab Übung', 'G2 1.30', 3, 3),\n" +
"('ET', 'EE', 4, 'Hofmann (E)', 'Elektrische Netze', 'G2 2.01', 3, 4),\n" +
"('ET', 'EE', 4, 'Hofmann (E)', 'Elektrische Netze', 'G2 1.01', 4, 3),\n" +
"('E', 'EE', 6, 'Hofmann (E)', 'Elektrische Netze', 'G2 2.01', 3, 4),\n" +
"('E', 'EE', 6, 'Hofmann (E)', 'Elektrische Netze', 'G2 1.01', 4, 3),\n" +
"('ET', NULL, 4, 'Horn', 'Digitale Signalverarbeitung', 'G2 1.01', 1, 1),\n" +
"('ET', NULL, 6, 'Schüle', 'Embedded Systems 1', 'G2 2.26', 4, 3),\n" +
"('ET', NULL, 6, 'Schüle', 'Embedded Systems 1', 'G2 2.26', 4, 4),\n" +
"('ET', 'EE', 7, 'Hörger', 'Energiesysteme 2', 'G2 1.01', 5, 2),\n" +
"('ET', 'EE', 7, 'Hörger', 'Energiesysteme 2', 'G2 1.01', 5, 3),\n" +
"('E', 'EE', 7, 'Hörger', 'Energiesysteme 2', 'G2 1.01', 5, 2),\n" +
"('E', 'EE', 7, 'Hörger', 'Energiesysteme 2', 'G2 1.01', 5, 3),\n" +
"('ET', 'EE', 7, 'Kolb', 'Energietechnik Labor', 'G2 2.30', 2, 1),\n" +
"('ET', 'EE', 7, 'Kolb', 'Energietechnik Labor', 'G2 2.30', 2, 2),\n" +
"('ET', 'EE', 7, 'Kolb', 'Energietechnik Labor', 'G2 2.30', 2, 3),\n" +
"('ET', 'EE', 7, 'Kolb', 'Energietechnik Labor', 'G2 2.30', 2, 4),\n" +
"('ET', NULL, 4, 'Knobelspies', 'Technical English B2', 'G2 2.41', 4, 2),\n" +
"('ET', NULL, 6, 'Knobelspies', 'Technical English B2', 'G2 2.41', 4, 2),\n" +
"('ET', NULL, 7, 'Knobelspies', 'Technical English B2', 'G2 2.41', 4, 2),\n" +
"('M', 'R', 6, 'Rittmann', 'Thermische Verfahrenstechnik', '0115', 1, 1),\n" +
"('M', 'K', 7, 'Rittmann', 'Fahrzeugmotor', '0211', 4, 4),\n" +
"('M', 'K', 7, 'Frick', 'Fahrzeugwerkstoffe', '0212', 2, 1),\n" +
"('M', 'K', 7, 'Merkel', 'Fahrzeugkonstruktion', '0201', 2, 3),\n" +
"('VI', NULL, 6, 'Boy', 'Kosten- u. Leistungsrechnung', '0233', 5, 2),\n" +
"('VI', NULL, 6, 'Boy', 'Kosten- u. Leistungsrechnung', '0233', 5, 3),\n" +
"('VI', NULL, 7, 'Görne', 'Comprehensive Design Sales', '0222', 3, 3),\n" +
"('VI', NULL, 7, 'Subek', 'Innovationsmanagement', '0120', 3, 6),\n" +
"('VI', NULL, 7, 'Athanasiou', 'Produktmanagement', '0120', 5, 6),\n" +
"('TME', NULL, NULL, 'Subek', 'Produkt- u. Innovationsmanagement', '0102', 4, 5),\n" +
"('TME', NULL, NULL, 'Subek', 'Produkt- u. Innovationsmanagement', '0102', 4, 6),\n" +
"('TMP', NULL, NULL, 'Subek', 'Produkt- u. Innovationsmanagement', '0102', 4, 5),\n" +
"('TMP', NULL, NULL, 'Subek', 'Produkt- u. Innovationsmanagement', '0102', 4, 6),\n" +
"('PEF', NULL, NULL, 'Subek', 'Produkt- u. Innovationsmanagement', '0102', 4, 5),\n" +
"('PEF', NULL, NULL, 'Subek', 'Produkt- u. Innovationsmanagement', '0102', 4, 6),\n" +
"('IDM', NULL, 1, 'Subek', 'Produktentwickung', '0102', 4, 5),\n" +
"('IDM', NULL, 1, 'Subek', 'Produktentwickung', '0102', 4, 6),\n" +
"('IST', NULL, NULL, 'Subek', 'Produktentwickung', '0102', 4, 5),\n" +
"('IST', NULL, NULL, 'Subek', 'Produktentwickung', '0102', 4, 6),\n" +
"('TME', NULL, NULL, 'Subek', 'Produktionsmanagement', '0106', 1, 2),\n" +
"('TME', NULL, NULL, 'Subek', 'Produktionsmanagement', '0105', 5, 1),\n" +
"('TMP', NULL, NULL, 'Subek', 'Produktionsmanagement', '0106', 1, 2),\n" +
"('TMP', NULL, NULL, 'Subek', 'Produktionsmanagement', '0105', 5, 1),\n" +
"('PEF', NULL, NULL, 'Subek', 'Produktionsmanagement', '0106', 1, 2),\n" +
"('PEF', NULL, NULL, 'Subek', 'Produktionsmanagement', '0105', 5, 1),\n" +
"('TME', NULL, NULL, 'Hauf', 'Physikalische Modellbildung', '0225', 3, 4),\n" +
"('TME', NULL, NULL, 'Hauf', 'Physikalische Modellbildung', '0225', 3, 5),\n" +
"('PEF', NULL, NULL, 'Hauf', 'Physikalische Modellbildung', '0225', 3, 4),\n" +
"('PEF', NULL, NULL, 'Hauf', 'Physikalische Modellbildung', '0225', 3, 5),\n" +
"('TME', NULL, NULL, 'Hauf', 'Fahrdynamik', '0122', 5, 3),\n" +
"('TME', NULL, NULL, 'Hauf', 'Fahrdynamik', '0122', 5, 4),\n" +
"('TMP', NULL, NULL, 'Hauf', 'Fahrdynamik', '0122', 5, 3),\n" +
"('TMP', NULL, NULL, 'Hauf', 'Fahrdynamik', '0122', 5, 4),\n" +
"('PEF', NULL, NULL, 'Hauf', 'Fahrdynamik', '0122', 5, 3),\n" +
"('PEF', NULL, NULL, 'Hauf', 'Fahrdynamik', '0122', 5, 4),\n" +
"('P', NULL, 1, 'Günter', 'Statik', 'AH 1.02', 2, 1),\n" +
"('P', NULL, 1, 'Günter', 'Statik', '0202', 4, 1),\n" +
"('P', NULL, 1, 'Günter', 'Statik', '0202', 4, 2),\n" +
"('ET', 'EE, IK, IE', 6, 'Knobelspies', 'Technical English B2', 'G2 2.41', 4, 2),\n" +
"('ET', 'EE, IE, IK', 7, 'Knobelspies', 'Technical English B2', 'G2 2.41', 4, 2),\n" +
"('E', 'EEE, IFE, MK', 4, 'Stelzenmüller', 'Regelungstechnik 1 Matlab Übung', 'G2 1.30', 3, 3),\n" +
"('ET', 'IE, IK', 4, 'Horn', 'Digitale Signalverarbeitung', 'G2 1.01', 1, 1),\n" +
"('ET', 'IE, IK, IE_W', 6, 'Schüle', 'Embedded Systems 1', 'G2 2.26', 4, 3),\n" +
"('ET', 'IE, IK, IE_W', 6, 'Schüle', 'Embedded Systems 1', 'G2 2.26', 4, 4),\n" +
"('ET', 'IE, IK', 7, 'Schüle', 'Embedded Systems 2', 'G2 2.26', 1, 2),\n" +
"('ET', 'IE, IK', 7, 'Schüle', 'Embedded Systems 2', 'G2 2.26', 1, 3),\n" +
"('ET', 'IE, IK', 4, 'Knobelspies', 'Technical English B2', 'G2 2.41', 4, 2),\n" +
"('P', NULL, 6, 'Pietzsch', 'Freeform Surface design', '0101, 0290', 4, 1),\n" +
"('P', NULL, 6, 'Gärtner', 'Simulation im Design', '0106, 0290', 5, 2),\n" +
"('P', NULL, 6, 'Gärtner', 'Simulation im Design', '0106, 0290', 5, 1),\n" +
"('P', NULL, 6, 'Pietzsch', 'Freeform Surface design', '0115, 0290', 4, 4),\n" +
"('P', NULL, 7, 'Pietzsch', 'Ecodesign', '0115', 1, 2),\n" +
"('P', NULL, 7, '?', 'Simulationsprojekt', '0290', 2, 3),\n" +
"('SG', NULL, NULL, 'Kropp', 'Studium Generale - Ringvorlesung Nachhaltige Entwicklung', 'AH 0.01', 3, 5),\n" +
"('VMM', NULL, 1, 'Steinhauser', 'Grundlagen Technisches Zeichnen', 'AH 1.01', 5, 5),\n" +
"('VMM', NULL, 1, 'Steinhauser', 'Grundlagen Technisches Zeichnen', 'AH 1.01', 5, 6),\n" +
"('VI', NULL, 1, 'Steinhauser', 'Technisches Zeichnen', 'AH 1.01', 5, 5),\n" +
"('VI', NULL, 1, 'Steinhauser', 'Technisches Zeichnen', 'AH 1.01', 5, 6),\n" +
"('VMM', NULL, 1, '?', 'CAD', '0289', 5, 5),\n" +
"('VMM', NULL, 1, '?', 'CAD', '0289', 5, 6),\n" +
"('VI', NULL, 1, '?', 'CAD', '0289', 5, 5),\n" +
"('VI', NULL, 1, '?', 'CAD', '0289', 5, 6),\n" +
"('LBM', NULL, NULL, 'Pietzsch', 'Indutrial Design / Technisches Design', '0102', 5, 3),\n" +
"('LBM', NULL, NULL, 'Pietzsch', 'Indutrial Design / Technisches Design', '0102', 5, 4),\n" +
"('V', NULL, 6, 'Goll', 'Werkstoffe Nutzenergiewandlung', '0120', 3, 3),\n" +
"('V', NULL, 7, 'Goll', 'Werkstoffe Nutzenergiewandlung', '0120', 3, 3),\n" +
"('VMM', NULL, 6, 'Goll', 'Werkstoffe Nutzenergiewandlung', '0120', 3, 3),\n" +
"('VMM', NULL, 7, 'Goll', 'Werkstoffe Nutzenergiewandlung', '0120', 3, 3),\n" +
"('VMG', NULL, 7, 'Goll', 'Werkstoffe Nutzenergiewandlung', '0120', 3, 3),\n" +
"('V', NULL, 6, 'Gerstner', 'Leichtbauwerkstoffe', '0114', 1, 6),\n" +
"('VMM', NULL, 6, 'Gerstner', 'Leichtbauwerkstoffe', '0114', 1, 6),\n" +
"('V', NULL, 6, 'Goll', 'Werkstoffe Primärenergiewandlung', '0120', 3, 2),\n" +
"('V', NULL, 7, 'Goll', 'Werkstoffe Primärenergiewandlung', '0120', 3, 2),\n" +
"('VMM', NULL, 6, 'Goll', 'Werkstoffe Primärenergiewandlung', '0120', 3, 2),\n" +
"('VMM', NULL, 7, 'Goll', 'Werkstoffe Primärenergiewandlung', '0120', 3, 2),\n" +
"('VMG', NULL, 7, 'Goll', 'Werkstoffe Primärenergiewandlung', '0120', 3, 2),\n" +
"('MP', NULL, 7, 'Schillig', 'Lean Management', '0212', 3, 2),\n" +
"('MP', NULL, 7, 'Schillig', 'Lean Management', '0212', 3, 3),\n" +
"('V', NULL, 6, 'Möckel', 'Analytische Methoden', '0101', 2, 4),\n" +
"('V', NULL, 7, 'Möckel', 'Analytische Methoden', '0101', 2, 4),\n" +
"('M', NULL, 4, 'Wagner', 'Steuern und Regeln 1', 'AH 1.01', 4, 2),\n" +
"('M', NULL, 4, 'Wagner', 'Steuern und Regeln 1', 'AH 1.01', 4, 3),\n" +
"('P', NULL, 4, 'Wagner', 'Steuern und Regeln 1', 'AH 1.01', 4, 2),\n" +
"('P', NULL, 4, 'Wagner', 'Steuern und Regeln 1', 'AH 1.01', 4, 3),\n" +
"('VMM', NULL, 6, 'Wagner', 'Steuern und Regeln 1', 'AH 1.01', 4, 2),\n" +
"('VMM', NULL, 6, 'Wagner', 'Steuern und Regeln 1', 'AH 1.01', 4, 3),\n" +
"('VMM', NULL, 7, 'Wegmann', 'Vertiefung Festigkeitslehre', '0102', 3, 1),\n" +
"('VMG', NULL, 7, 'Wegmann', 'Vertiefung Festigkeitslehre', '0102', 3, 1),\n" +
"('VMG', NULL, 7, 'Diverse', 'KIT', '0119', 4, 1),\n" +
"('VMG', NULL, 7, 'Diverse', 'KIT', '0119', 4, 2),\n" +
"('VMG', NULL, 7, 'Diverse', 'KIT', '0119', 4, 3),\n" +
"('VMG', NULL, 7, 'Diverse', 'KIT', '0119', 4, 4),\n" +
"('VMG', NULL, 7, 'Diverse', 'KIT', '0119', 4, 5),\n" +
"('VMG', NULL, 7, 'Diverse', 'KIT', '0119', 4, 6),\n" +
"('VMG', NULL, 7, 'Diverse', 'KIT', '0119', 5, 1),\n" +
"('VMG', NULL, 7, 'Diverse', 'KIT', '0119', 5, 2),\n" +
"('VMG', NULL, 7, 'Diverse', 'KIT', '0119', 5, 3),\n" +
"('VMG', NULL, 7, 'Diverse', 'KIT', '0119', 5, 4),\n" +
"('VMG', NULL, 7, 'Diverse', 'KIT', '0119', 5, 5),\n" +
"('VMG', NULL, 7, 'Diverse', 'KIT', '0119', 5, 6),\n" +
"('V', NULL, 7, 'Sörgel', 'Spezielle Verfahren der Galvanotechnik', '0129', 4, 4),\n" +
"('MP', NULL, 7, 'Bausch Ch.', 'Internationaler Produktionsanlagenbau', '0114', 4, 5),\n" +
"('MP', NULL, 7, 'Bausch Ch.', 'Internationaler Produktionsanlagenbau', '0114', 4, 6),\n" +
"('ET', NULL, 2, 'Bartel', 'Digitaltechnik', 'G2 2.33', 1, 5),\n" +
"('ET', NULL, 2, 'Bartel', 'Digitaltechnik', 'G2 2.33', 4, 4),\n" +
"('ET', NULL, 1, 'Bartel', 'Digitaltechnik', 'G2 2.33', 1, 5),\n" +
"('ET', NULL, 1, 'Bartel', 'Digitaltechnik', 'G2 2.33', 4, 4),\n" +
"('LBM', NULL, NULL, 'Merkel', 'FEM und Übungen', '0290', 4, 3),\n" +
"('MW', NULL, 1, 'Plotzitza', 'Festigkeitslehre', '0275', 1, 4),\n" +
"('MW', NULL, 1, 'Plotzitza', 'Festigkeitslehre', '0275', 1, 5),\n" +
"('V', NULL, 4, 'Bingel', 'Galvanotechnische Verfahren', '0234', 5, 2),\n" +
"('V', NULL, 3, 'Bingel', 'Galvanotechnik 1', '0234', 5, 3),\n" +
"('VMG', NULL, 3, 'Bingel', 'Galvanotechnik 1', '0234', 5, 3),\n" +
"('ET', NULL, 2, 'Lamek-Creutz', 'Elektrotechnik 2', 'G2 2.41', 1, 4),\n" +
"('WI', NULL, 3, 'Burdack', 'Tutorium: ERP-Systeme', 'G2 1.30', 4, 4),\n" +
"('WI', NULL, 3, 'Burdack', 'Tutorium: ERP-Systeme', 'G2 1.30', 4, 5),\n" +
"('F', NULL, 6, 'Höfig', 'Ringvorlesung Industrie 4.0', 'G1 0.20', 1, 6),\n" +
"('F', NULL, 7, 'Höfig', 'Ringvorlesung Industrie 4.0', 'G1 0.20', 1, 6),\n" +
"('MekA', NULL, 5, 'Höfig', 'Ringvorlesung Industrie 4.0', 'G1 0.20', 1, 6),\n" +
"('O', NULL, 2, 'Zipfl', 'Grundlagen elektronischer Schaltungen', 'G1 0.22', 2, 2),\n" +
"('O', NULL, 2, 'Zipfl', 'Elektronische Bauelemente', 'G1 1.20', 4, 5),\n" +
"('O', NULL, 3, 'Zipfl', 'Schaltungstechnik der Optoelektronik', 'G1 0.01', 4, 4),\n" +
"('O', NULL, 4, 'Zipfl', 'Systemtheorie und Simulation', 'G1 1.01', 2, 1),\n" +
"('O', NULL, 4, 'Zipfl', 'Systemtheorie und Simulation', 'G1 0.22', 4, 1),\n" +
"('O', NULL, 6, 'Zipfl', 'Systemtheorie und Simulation', 'G1 1.01', 2, 1),\n" +
"('O', NULL, 6, 'Zipfl', 'Systemtheorie und Simulation', 'G1 0.22', 4, 1),\n" +
"('PH', NULL, NULL, 'Zipfl', 'Analog Signal Processing', 'G1 1.01', 3, 1),\n" +
"('IDM', NULL, 1, 'Ulsamer', 'Messen Steuern und Regeln', '0202', 1, 6),\n" +
"('IDM', NULL, 1, 'Ulsamer', 'Messen Steuern und Regeln', '0202', 1, 7),\n" +
"('IST', NULL, NULL, 'Ulsamer', 'Messen Steuern und Regeln', '0202', 1, 6),\n" +
"('IST', NULL, NULL, 'Ulsamer', 'Messen Steuern und Regeln', '0202', 1, 7),\n" +
"('O', NULL, 1, 'Hahn, Zipfl', 'Elektrische Messtechnik', 'G1 0.34', 5, 2),\n" +
"('O', NULL, 1, 'Hahn, Zipfl', 'Elektrische Messtechnik', 'G1 0.34', 5, 3),\n" +
"('PH', NULL, NULL, 'Zipfl', 'Analog Signal Processing labratory', 'G1 0.01', 3, 4),\n" +
"('PH', NULL, NULL, 'Zipfl', 'Analog Signal Processing labratory', 'G1 0.01', 3, 5),\n" +
"('WI', NULL, NULL, '?', 'Probevorlesung WI', '0614', 2, 3),\n" +
"('WI', NULL, NULL, '?', 'Probevorlesung WI', '0614', 2, 4),\n" +
"('WI', NULL, NULL, '?', 'Probevorlesung WI', '0614', 2, 5),\n" +
"('WI', NULL, NULL, '?', 'Probevorlesung WI', '0614', 2, 6),\n" +
"('WI', NULL, NULL, '?', 'Probevorlesung WI', '0614', 2, 7),\n" +
"('WI', NULL, NULL, '?', 'Probevorlesung WI', '0614', 2, NULL),\n" +
"('WI', NULL, NULL, '?', 'Probevorlesung WI', '0614', 3, 1),\n" +
"('WI', NULL, NULL, '?', 'Probevorlesung WI', '0614', 3, 2),\n" +
"('WI', NULL, NULL, '?', 'Probevorlesung WI', '0614', 3, 3),\n" +
"('WI', NULL, NULL, '?', 'Probevorlesung WI', '0614', 3, 4),\n" +
"('WI', NULL, NULL, '?', 'Probevorlesung WI', '0614', 4, 1),\n" +
"('WI', NULL, NULL, '?', 'Probevorlesung WI', '0614', 4, 2),\n" +
"('WI', NULL, NULL, '?', 'Probevorlesung WI', '0614', 4, 3),\n" +
"('WI', NULL, NULL, '?', 'Probevorlesung WI', '0614', 4, 4),\n" +
"('WI', NULL, NULL, '?', 'Probevorlesung WI', '0614', 4, 5),\n" +
"('WI', NULL, NULL, '?', 'Probevorlesung WI', '0614', 4, 6),\n" +
"('WI', NULL, NULL, '?', 'Probevorlesung WI', '0614', 4, 7),\n" +
"('WI', NULL, NULL, '?', 'Probevorlesung WI', '0614', 4, NULL),\n" +
"('IN', 'IS', 6, 'Hellmann', 'Netzwerksicherheit', 'G2 1.37', 4, 4),\n" +
"('IN', 'IS', 6, 'Hellmann', 'Netzwerksicherheit', 'G2 1.37', 4, 5),\n" +
"('IN', 'WI', 6, 'Gold', 'Business Intelligence', 'G2 0.23', 6, 1),\n" +
"('IN', 'WI', 6, 'Gold', 'Business Intelligence', 'G2 0.23', 6, 2),\n" +
"('IN', 'WI', 6, 'Gold', 'Business Intelligence', 'G2 0.23', 6, 3),\n" +
"('IN', 'WI', 7, 'Gold', 'Business Intelligence', 'G2 0.23', 6, 1),\n" +
"('IN', 'WI', 7, 'Gold', 'Business Intelligence', 'G2 0.23', 6, 2),\n" +
"('IN', 'WI', 7, 'Gold', 'Business Intelligence', 'G2 0.23', 6, 3),\n" +
"('IN', 'WI', 6, 'Barth Sascha', 'SAP BI', 'G2 2.41~G2 1.28', 4, 4),\n" +
"('IN', 'WI', 6, 'Barth Sascha', 'SAP BI', 'G2 2.41~G2 1.28', 4, 5),\n" +
"('IN', 'WI', 7, 'Barth Sascha', 'SAP BI', 'G2 2.41~G2 1.28', 4, 4),\n" +
"('IN', 'WI', 7, 'Barth Sascha', 'SAP BI', 'G2 2.41~G2 1.28', 4, 5),\n" +
"('AM', NULL, 1, 'Schiefer', 'Refraktion u. Screening', NULL, 3, 7),\n" +
"('AM', NULL, 1, '?', 'Innovation und Qualitätsm.', NULL, 1, 5),\n" +
"('AM', NULL, 1, '?', 'Innovation und Qualitätsm.', NULL, 1, 6),\n" +
"('AM', NULL, 1, '?', 'Matlab / Simulink', NULL, 4, 4),\n" +
"('AM', NULL, 1, '?', 'Matlab / Simulink', NULL, 4, 5),\n" +
"('ZDO', NULL, NULL, 'Nowak', 'Deutsch als Fremdsprache A1.2', '0124', 1, 5),\n" +
"('ZDO', NULL, NULL, 'Nowak', 'Deutsch als Fremdsprache A1.2', '0124', 1, 6),\n" +
"('ZEO', NULL, NULL, 'Ruf Viola', 'General English B2', '0212', 3, 5),\n" +
"('I', NULL, 1, 'Egetenmeier', 'Tutorium Wirtschaftsmathematik', '0133', 1, 4),\n" +
"('CCS', NULL, 2, 'Kleppmann', 'Design of Experiments', 'G2 1.30', 2, 3),\n" +
"('CCS', NULL, 2, 'Rupp', 'Modellierung und Simulation technischer Systeme', 'G2 1.35', 2, 4),\n" +
"('CCS', NULL, 2, 'Rupp', 'Modellierung und Simulation technischer Systeme', 'G2 2.33', 2, 5),\n" +
"('CCS', NULL, 2, 'Rupp', 'Übung - Modellierung und Simulation techn Syst.', 'G2 2.33', 2, 6),\n" +
"('CCS', NULL, 2, 'Diverse', 'Projektarbeit', NULL, 3, 5),\n" +
"('CCS', NULL, 2, 'Diverse', 'Kolloquium', NULL, 3, 4),\n" +
"('MP', NULL, 3, 'Dittmar (LB)', 'Elektrotechnik', '0233', 3, 1),\n" +
"('MP', NULL, 3, 'Dittmar (LB)', 'Elektrotechnik', '0233', 3, 2),\n" +
"('MW', NULL, 1, 'Plotzitza', 'Technische Mechanik', '0101', 4, 4),\n" +
"('MW', NULL, 1, 'Plotzitza', 'Technische Mechanik', '0101', 4, 5),\n" +
"('AM', NULL, 1, '?', 'Biochemie / Biotechnologie', NULL, 3, 5),\n" +
"('AM', NULL, 1, '?', 'Biochemie / Biotechnologie', NULL, 5, 3),\n" +
"('AM', NULL, 1, '?', 'Biochemie / Biotechnologie', NULL, 5, 4),\n" +
"('VW-Mit', NULL, NULL, 'Diverse', 'Verwaltung - Mitarbeiterschulung', '0267', 3, 2),\n" +
"('P', NULL, 6, 'Weber (P)', 'Licht und Sicht', '0111', 1, 2),\n" +
"('P', NULL, 6, 'Weber (P)', NULL, '0290', 1, 2),\n" +
"('PEF', NULL, NULL, 'Merkel', 'FEM und Übungen', '0289', 3, 3),\n" +
"('TMP', NULL, NULL, 'Merkel', 'FEM und Übungen', '0289', 3, 3),\n" +
"('TME', NULL, NULL, 'Merkel', 'FEM und Übungen', '0289', 3, 3),\n" +
"('LBM', NULL, NULL, 'Merkel', 'FEM und Übungen', '0289', 3, 3),\n" +
"('PEF', NULL, NULL, 'Merkel', 'FEM und Übungen', '0701', 3, 3),\n" +
"('TMP', NULL, NULL, 'Merkel', 'FEM und Übungen', '0701', 3, 3),\n" +
"('TME', NULL, NULL, 'Merkel', 'FEM und Übungen', '0701', 3, 3),\n" +
"('LBM', NULL, NULL, 'Merkel', 'FEM und Übungen', '0701', 3, 3),\n" +
"('B', NULL, 3, 'Leiterholt', 'Betriebsorganisaion', '0105', 3, 4),\n" +
"('B', NULL, 3, 'Leiterholt', 'Betriebsorganisaion', '0103', 4, 6),\n" +
"('B', NULL, 3, 'Leiterholt', 'Betriebsorganisaion', '0103', 4, 7),\n" +
"('ZEO', NULL, NULL, 'Kreuzer', 'Englisch B1', '0124', 3, 5),\n" +
"('IN', 'IN, MI, IS, SE', 1, 'Rössel', 'Schlüsselqualifikationen', 'G2 0.23~G2 0.21~G2 1.30', 2, 4),\n" +
"('IN', 'IN, MI, IS, SE', 1, 'Rössel', 'Schlüsselqualifikationen', 'G2 0.23~G2 0.21~G2 1.30', 2, 5),\n" +
"('IN', 'MI, SE', 4, 'Küpper', 'Mensch-Computer-Interaktion', 'G2 1.44', 1, 4),\n" +
"('IN', 'MI, SE', 4, 'Küpper', 'Mensch-Computer-Interaktion', 'G2 0.21', 3, 3),\n" +
"('IN', 'MI, SE', 6, 'Klauck', 'Bildverarbeitung', 'G2 1.01', 2, 2),\n" +
"('IN', 'MI, SE', 6, 'Klauck', 'Bildverarbeitung', 'G2 1.01', 2, 3),\n" +
"('IN', 'MI, SE', 7, 'Klauck', 'Bildverarbeitung', 'G2 1.01', 2, 2),\n" +
"('IN', 'MI, SE', 7, 'Klauck', 'Bildverarbeitung', 'G2 1.01', 2, 3),\n" +
"('ET', 'EE, IE', 4, 'Steinhart', 'Elektrische Antriebe', 'G2 0.01', 1, 2),\n" +
"('I', 'F, M', 6, 'Güida', 'Makroökonomie', '0222', 3, 2),\n" +
"('A', NULL, 4, 'Buschle, Lanzinger', 'Praktikum Opt.Screening', 'G4 1.07, G4 2.02', 3, 3);"};
	
}
/*
drop table g02_fachbereiche;
drop table g02_raeume;
drop table g02_gebaeude;
drop table g02_user;
drop table g02_vlteilnahme;
drop table g02_vorlesungsplan;
*/