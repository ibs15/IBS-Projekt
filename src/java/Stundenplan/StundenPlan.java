/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Stundenplan;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.websocket.Session;

/**
 *
 * @author john
 */
public class StundenPlan extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    //String userName = "Please report Error";
    User user;

    //int semester;
    //String studiengang;
    //String fachrichtung;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String debug = "";
        // try {

        //out.println("<--! " + Workr + " s-->");
        out.println("<!DOCTYPE html>");
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Stundenplan</title>"
                + "<meta charset=\"utf-8\">"
                + "<link rel=\"stylesheet\" type=\"text/css\" href=\"IBS_Stundenplan.css\">");
        out.println("</head>");
        out.println("<body>");

        out.println("<h1>" + user.getName() + ", Willkommen zum Stundenplan</h1>\n"
                + "\n"
                + "<ul class=topbar >\n");
        out.print("<li><form action=\"undoServlet\" method=\"get\">\n"
                + "<input  type=\"submit\" value=\"undo\" "
                + (user.canUndo() ? "disabled" : "")
                + " /></form></li>");

        out.print("<li><form action=\"LogoutServlet\" method=\"post\">\n"
                + "<input  type=\"submit\" value=\"Logout\"/></form></li>");

        out.print("<li><form action=\"UploadServlet\" method=\"post\">\n"
                + "<input  type=\"submit\" value=\"upload XML\"/></form></li>");
         out.print("<li><form action=\"PreRegister\" method=\"post\">\n"
                + "<input  type=\"submit\" value=\"register\"/></form></li>");

        out.print("<li><form action=\"DownloadServlet\" method=\"post\">\n"
                + "<input  type=\"submit\" value=\"download XML\"/></form></li>");

        out.print("<li><form action=\"undoServlet\" method=\"get\">\n"
                + "<input  type=\"submit\" value=\"redo\" "
                + (user.canRedo() ? "disabled" : "")
                + "/></form></li>");

        out.println("</ul><ul class=\"sidebar\"> ");

        out.print(this.getSidebar());
        out.println("</ul><ul class=\"main\" >");
        out.print(user.getTable().toHtml());

        out.println("</ul></body>");
        out.println("</html>");
        //out.print(debug);
        out.close();
        SQLConnector.free();
       
    }

    public String getSidebar() {
        String a = "";

        if (user.getSemester() != 0 && user.getStudiengang() != null) {// || user.getStudiengang().equals("")) {

            a += ("<form action=\"StundenPlan\" method=\"post\" >\n");
            a += ("<select name=\"fachrichtung\">  Fachrichtung\n");
            try {
                SQLConnector sql = new SQLConnector();
                String base = "select distinct schwerpunkt from g02_vorlesungsplan where stg=? and semester=?";
                PreparedStatement prep = sql.getPreparedStatement(base);
                prep.setString(1, user.getStudiengang());
                prep.setInt(2, user.getSemester());
                ResultSet res = sql.performSQL(prep);
                ArrayList<String> dist = new ArrayList<String>();
                dist.add(" ");
                try {
                    while (!res.isLast()) {
                        res.next();
                        String[] data = res.getString("schwerpunkt").split(",");
                        for (String val : data) {
                            if (!dist.contains(val.trim())) {
                                dist.add(val.trim());
                            }
                        }
                    }

                    for (String val : dist) {
                        a += ("<option name=\"fachrichtung\"  value=\"" + val.trim() + "\" >" + val + "</option>\n");
                    }

                } catch (Exception e) {
                    a += e.getLocalizedMessage();
                }

                try {
                    res.close();
                } catch (Exception e) {
                }
            } catch (Exception e) {
                a += "##";
                a += e.getMessage();

            }

            a += ("</select>\n");
            a += ("<input id=\"submit\" type=\"submit\" value=\"jup\">\n");
            a += ("</form>\n");
            a += "<form action=\"StundenPlan\" method=\"post\" ><input type=\"hidden\" name=\"semester\" value=\"0\"> <input  type=\"submit\" value=\"auswahl zurücksetzen\"></form>";
            a += generateList();

//            a+= generateList();
        } else {

            a += "<form action=\"StundenPlan\" method=\"post\" >\n";
            a += ("<select name=\"semester\"> Fachsemester:\n");
            for (int i = 1; i <= 7; i++) {
                a += ("<option name=\"semester\" value=\"" + i + "\">" + i + "</option>\n");
            }
            a += ("</select>\n");

            a += ("<select name=\"studiengang\"> studiengang:\n");
            try {
                SQLConnector sql = new SQLConnector();
                String base = "select distinct stg from g02_vorlesungsplan order by(stg) ";
                PreparedStatement prep = sql.getPreparedStatement(base);
                ResultSet res = sql.performSQL(prep);
                try {
                    while (!res.isLast()) {
                        res.next();
                        a += ("<option name=\"studiengang\" value=\"" + res.getString("stg") + "\">" + res.getString("stg") + "</option>\n");

                    }

                    res.close();
                } catch (Exception e) {
                    a += e.getLocalizedMessage();
                }

                try {
                    res.close();
                } catch (Exception e) {
                }
            } catch (Exception e) {
                a += "##";
                a += e.getMessage();

            }
            a += ("</select><input id=\"submit\" type=\"submit\" value=\"jup\"> </form>\n");

        }
        a += "";
        SQLConnector.free();
        return a;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {


        doPost(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Cookie[] cookies = request.getCookies();
        user = LoginManager.login(cookies);
        if (user == null||user.getName()==null) {
            System.err.println("No such User");
            response.sendRedirect("/Stundeplan");
            response.getWriter().close();
            return;
        }
        String get = request.getParameter("semester");
        int semester = 0;
        if (get != null) {
            try {
                semester = Integer.parseInt(get);
            } catch (Exception e) {
                System.err.println("error CookieCre" + get + e.getMessage());
            }
            Cookie loginCookie = new Cookie("semester", get);
            loginCookie.setMaxAge(30 * 60);
            response.addCookie(loginCookie);
            user.setSemester(semester);
        }
        get = request.getParameter("studiengang");

        if (get != null) {
            String studiengang2;
            Cookie loginCookie = new Cookie("studiengang", get);
            loginCookie.setMaxAge(30 * 60);
            response.addCookie(loginCookie);
            studiengang2 = get;
            user.setStudiengang(studiengang2);
        }
        get = request.getParameter("fachrichtung");
        if (get != null) {
            String fachrichtung;
            Cookie loginCookie = new Cookie("fachrichtung", get);
            loginCookie.setMaxAge(30 * 60);
            response.addCookie(loginCookie);
            fachrichtung = get;
            user.setFachrichtung(fachrichtung);
        }

        String[] toDelete = request.getParameterValues("table");
        try {
            if (toDelete != null) {
                for (String s : toDelete) {
                    try {
                        int val = Integer.parseInt(s);
                        System.err.println("Stundenplan-rm" + s);
                        user.removeKurstermin(val);
                    } catch (Exception e) {
                    }
                }
            }
        } catch (Exception e) {
        };

        String[] toInsert = request.getParameterValues("list");
        try {
            if (toInsert != null) {
                for (String s : toInsert) {
                    try {
                        int val = Integer.parseInt(s);
                        System.err.println("Stundenplan-add" + s);
                        user.addKurstermin(val);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        user.save();
        
        /*try {
        //    Thread.sleep(100);
        } catch (InterruptedException ex) {
            Logger.getLogger(StundenPlan.class.getName()).log(Level.SEVERE, null, ex);
        }*/
        user.getUid();
       // user = LoginManager.login(cookies);
        processRequest(request, response);
    }

    private String generateList() {
        String a = "";
        int count = 0;
        a += "<form action=\"StundenPlan\" method=\"post\" >\n";
        try {
            String base = "";
            SQLConnector sql = new SQLConnector();
            PreparedStatement prep;
            if (user.getFachrichtung() == null) {
                base = "select dozent, kurs, tagnr, zeitblocknr, vid from g02_vorlesungsplan where semester=? and stg= ?";
                prep = sql.getPreparedStatement(base);
            } else {
                base = "select dozent, kurs, tagnr, zeitblocknr, vid from g02_vorlesungsplan where semester=? and stg=? and schwerpunkt like ? ";
                prep = sql.getPreparedStatement(base);
                prep.setString(3, "%" + user.getFachrichtung() + "%");
            }

            prep.setInt(1, user.getSemester());
            prep.setString(2, user.getStudiengang());

            ResultSet res = sql.performSQL(prep);

            try {
                while (res.next()) {
                    count++;

                    a += "<input type=\"checkbox\"  name=\"list\" value=\"" + res.getString("vid") + "\">" + res.getString("kurs") + "<br/>\n";

                }

            } catch (Exception e) {
                a += e.getLocalizedMessage();
            }

            try {
                res.close();
            } catch (Exception e) {
            }
        } catch (Exception e) {
            a += "##";
            a += e.getMessage();

        }
        if (count == 0) {
            a = "Es konnten keine fächer für diese Auswahl gefunden Werden";
            return a;
        }
        a += "<input  type=\"submit\" value=\"hinzufügen\"></form>";

        return a;
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Zeigt den Stundenplan an";
    }

}
