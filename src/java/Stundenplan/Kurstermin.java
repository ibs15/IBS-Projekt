/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Stundenplan;

/**
 *
 * @author john
 */
class Kurstermin {

    String stg;
    Schwerpunkter schwerpunkt;
    int semester;
    String dozent, kurs, raum;
    int tagnr, zeitblocknr;
    int vid;

    public String getStg() {
        return stg;
    }

    public Schwerpunkter getSchwerpunkt() {
        return schwerpunkt;
    }

    public int getSemester() {
        return semester;
    }

    public String getDozent() {
        return dozent;
    }

    public String getKurs() {
        return kurs;
    }

    public String getRaum() {
        return raum;
    }

    public int getTagnr() {
        return tagnr;
    }

    public int getZeitblocknr() {
        return zeitblocknr;
    }

    public String getListElement() {
        String Returnvalue = "<td>";

        Returnvalue += "</td>";
        return Returnvalue;
    }

    public String getTableElement() {
        String a = " <td data-toggle=\"tooltip\" title=\""+ dozent+"\n"+stg+"\" > "//<div class=\"prof\">" + dozent + "</div>\n 
                +"<div align=\"vorlesung\">" + kurs + "</div>\n"+" <input type=\"checkbox\" name=\"table\" value=\""+vid+ "\" /> "
                +schwerpunkt.getHtml()+"<div class=\"raum\">"+raum+"</div>";
                
            a += "</td>";
        
        return a;
    }

    public String getXML() {
        return "<kurstermin>\n"
                +"\t<Studiengang>"+this.stg+"</Studiengang>\n"
                +"\t<Schwerpunkt>"+this.schwerpunkt+"</Schwerpunkt>\n"
                +"\t<Semester>"+this.semester+"</Semester>\n"
                +"\t<Dozent>"+this.dozent+"</Dozent>\n"
                +"\t<Kurs>"+this.kurs+"</Kurs>\n"
                +"\t<Raum>"+this.raum+"</Raum>\n"
                +"\t<TagNR>"+this.tagnr+"</TagNR>\n"
                +"\t<ZeitBlockNR>"+this.zeitblocknr+"</ZeitBlockNR>\n"
                +"\t<VID>"+this.vid+"</VID>\n"
                +"</kurstermin>\n\n";
    }

    public Kurstermin( String stg, Schwerpunkter schwerpunkt, int semester, String dozent, String kurs, String raum, int tagnr, int zeitblocknr, int vid) {
        this.stg = stg;
        this.schwerpunkt = schwerpunkt;
        this.semester = semester;
        this.dozent = dozent;
        this.kurs = kurs;
        this.raum = raum;
        this.tagnr = tagnr;
        this.zeitblocknr = zeitblocknr;
        this.vid=vid;
    }

}
