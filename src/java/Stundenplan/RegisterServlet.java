
package Stundenplan;


import java.io.IOException;
import java.io.PrintWriter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;



import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LoginServlet
// */
@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {

    String debug = "";
    private static final long serialVersionUID = 1L;
    private boolean succes;
    private String Error="";
   protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String debug = "";
        // try {

        //out.println("<--! " + Workr + " s-->");
        out.println("<!DOCTYPE html>");
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Stundenplan</title>"
                + "<meta charset=\"utf-8\">"
                + "<meta http-equiv=\"refresh\" content=\"3; url="+((Error.equals(""))?"/Stundeplan/StundenPlan":"/Stundeplan/PreRegister")+"\" />"
                + "<link rel=\"stylesheet\" type=\"text/css\" href=\"IBS_Stundenplan_nosidebar.css\">");
        out.println("</head>");
        out.println("<body>");
if(Error.equals("")){
    out.println("<h1> Registerd Successfull</h1>");
    out.println("<a href=\"/Stundeplan/StundenPlan\" >weiter</a>");
}
else{
    out.println("<h1>Regitern hat leider nicht geklappt</h1>");
    out.println("<h2>"+Error+"</h2>");
      out.println("<a href=\"/Stundeplan/PreRegister\" >zurück</a>");
}
   }
    @Override
    protected void doPost(HttpServletRequest request,

            HttpServletResponse response) throws ServletException, IOException {
        System.out.println("register");
        SQLConnector a=new SQLConnector();
        //a.databaseCheck();
         System.out.println("----------------------------------");
         Cookie[] cookies=request.getCookies();
         User myUser=LoginManager.login(cookies);
        // get request parameters for userID and password
        String user = request.getParameter("user");
        String pwd = request.getParameter("pwd");
        String pwdT = request.getParameter("pwdT");
         System.out.println("-----Setup");
        if(myUser==null||myUser.getName()==""||myUser.getUid()<1){
             System.out.println("cancle");
             response.sendRedirect("/Stundeplan/PreRegister");
        }
        if(checkName(user)){
            System.out.println("nameOK");
            if(pwd.equals(pwdT) ){
                            System.out.println("PWOk");

            byte[] salt=Passwords.getNextSalt();
            byte[] hash=Passwords.hash(pwd.toCharArray(), salt);
             String base = "update g02_user set name = ? , salz =?, passwordhash =? where uid=? ";
             SQLConnector sql=new SQLConnector();
             PreparedStatement prep = sql.getPreparedStatement(base);
             try{
             prep.setInt(4, myUser.getUid());
              prep.setString(1, user);
            //   char [] transator="0123456789abcd".toCharArray();
            //  String salts="",hashs="";
//               for(int i=0;i<salt.length;i++){
//                   System.err.println(".");
//                  salts+=transator[(int)salt[i]];
//                  hashs+=transator[(int)hash[i]];
//              }
              prep.setString(2, javax.xml.bind.DatatypeConverter.printHexBinary(salt));
              prep.setString(3, javax.xml.bind.DatatypeConverter.printHexBinary(hash));
                
             
              ResultSet res = sql.performSQL(prep);
              res.close();

             }
             catch(Exception e){
                 e.printStackTrace();
             }
              System.out.println("Done");
        }
        else{
            Error+="Die Passwörter sind verschieden passwort und testpasswort bitte gleich eingeben";
        }
        }
        else{
            Error+="Der Benutzername wird schon Verwendet";
        }
        processRequest(request, response);
        
       

    }
    
    private boolean checkName(String Username){
        
    	SQLConnector sq = new SQLConnector();
        Error="";
        try{
            
        PreparedStatement ps = sq.getPreparedStatement("SELECT COUNT(*) from g02_user where name=?");
        ps.setString(1, Username);            
        ResultSet rs = sq.performSQL(ps);
        if(rs.next()&&rs.getInt(1)==0){
            if(!Username.contains("Benutzer"))
                rs.close();
            return true;
        }
        }
        catch(Exception e){
        	System.out.println(e.getMessage());
        }            
    	return false;
    }
    

}
