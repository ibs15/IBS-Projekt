/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Stundenplan;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author john
 */
public class DownloadServlet extends HttpServlet {

    private String userName;
    private String SID;
    

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    User author;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/xml;charset=UTF-8");
  //      byte[] res={125,125,125};
        byte[] res = author.getTable().toXML().getBytes(Charset.defaultCharset());;
        response.setContentLength(res.length);

        response.setHeader("Content-Disposition", "attachment; filename=\"StundenplanG02.xml\"");
        OutputStream outStream = response.getOutputStream();
        outStream.write(res);
        outStream.close();

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Cookie[] cookies = request.getCookies();
        author=LoginManager.login(cookies);
        
       User Myuser=LoginManager.login(cookies);
      
        if(Myuser == null) {
              try {
                  response.sendRedirect("/Stundeplan/LogoutServlet");
              } catch (IOException ex) {
                  Logger.getLogger(CommonsFileUploadServlet.class.getName()).log(Level.SEVERE, null, ex);
              }
           
        }

        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
          Cookie[] cookies = request.getCookies();
       User Myuser=LoginManager.login(cookies);
      
        if(Myuser == null) {
              try {
                  response.sendRedirect("/Stundeplan/LogoutServlet");
              } catch (IOException ex) {
                  Logger.getLogger(CommonsFileUploadServlet.class.getName()).log(Level.SEVERE, null, ex);
              }
           
        }

        author = LoginManager.login(cookies);
        processRequest(request, response);

        //sql logout foo
//if(userName == null) response.sendRedirect("login.html");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
